USE MASTER
--PROCESO DE CREACION DE LA BD
CREATE DATABASE ANODISA
ON
  PRIMARY (
        NAME     =  ANODISA_PRIMARIO_DATOS_01, 
        FILENAME = 'C:\DATABASE\ANODISA\DATOS\ANODISA_PRIMARIO_01.MDF',
            SIZE = 20MB)
  LOG ON 
      (
        NAME     = ANODISA_PRIMARIO_LOG_01, 
        FILENAME = 'C:\DATABASE\ANODISA\LOG\ANODISA_LOG_01.MDF',
            SIZE = 30MB)
GO

ALTER DATABASE ANODISA
  ADD FILEGROUP DATOS
GO 

ALTER DATABASE ANODISA
  ADD  FILE (NAME= ANODISA_DATOS_01,FILENAME = 'C:\DATABASE\ANODISA\DATOS\ANODISA_DATOS_01.MDF',
                SIZE = 20MB  ) TO FILEGROUP DATOS
GO

ALTER DATABASE ANODISA
 ADD FILEGROUP INDICES

GO    

        
ALTER DATABASE ANODISA
ADD FILE (NAME= ANODISA_INDEX_01,FILENAME = 'C:\DATABASE\ANODISA\INDICES\ANODISA_IDX_01.MDF',
            SIZE = 50MB) TO FILEGROUP INDICES
GO


/*USUERIO  ANODISA Y PASSWORD AUTOREPUESTOS*/

/*PERMITE BORRAR UN USUARIO DE TODO EL SQL*/
EXEC sp_droplogin  ANODISA
 
 
 /*creo la cuenta del usuario*/
exec sp_addlogin  @loginame =  'ANODISA' 
      ,  @passwd =  'S9u8r7e6c5@@'  
      ,  @defdb =  'ANODISA'  
      
exec sp_password  @new = 'S9u8r7e6c5@@'  
     ,  @loginame =  'ANODISA' 
      
 
use ANODISA
exec sp_grantdbaccess @loginame = 'ANODISA'
    ,@name_in_db = 'ANODISA'
    

exec sp_addrolemember  @rolename =  'db_owner', 
     @membername =  'ANODISA' 


/*PERMITE DESHABILITAR LA POLITICA DE CONTRASEÑAS DE SQL SERVER 2008*/
/****************************************/
ALTER LOGIN ANODISA WITH CHECK_POLICY = OFF;
/***************************************/  



