﻿using Business;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebSite.App_Start;
using WebSite.Models;

namespace WebSite.Controllers
{
    /// <summary>
    /// Nombre de la clase: OrdenesAnodizadoController
    /// </summary>
    public class OrdenesAnodizadoController : Controller
    {
        /// <summary>
        /// Se declara un objeto Api
        /// </summary>
        private Api api;

        /// <summary>
        /// Url del API
        /// </summary>
        private string ApiUrl;

        /// <summary>
        /// Url con el controlador
        /// </summary>
        private string url;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public OrdenesAnodizadoController()
        {
            api = new Api();
            ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
            url = ApiUrl + "OrdenesAnodizado";
            CargarListados();
        }

        /// <summary>
        /// Consulta el listado
        /// </summary>
        /// <returns>Retorna una vista con el listado</returns>
        [SessionCheck]
        public ActionResult Listado()
        {
            try
            {
                var json = api.Get(url);
                var listado = JsonConvert.DeserializeObject<List<OrdenAnodizado>>(json);

                return View(listado);
            }
            catch(Exception ex)
            {
                ModelState.AddModelError(string.Empty, "Server error." + ex.Message);
                return View();
            }
        }

        [SessionCheck]
        [HttpPost]
        public ActionResult Listado(DateTime Desde, DateTime Hasta)
        {
            //var empresaId = Convert.ToInt32(System.Web.HttpContext.Current.Session["EmpresaID"]);

            var desdeFormateada = Desde.Month + "/" + Desde.Day + "/" + Desde.Year + " 00:00:00";
            var hastaFormateada = Hasta.Month + "/" + Hasta.Day + "/" + Hasta.Year + " 23:59:59";

            var urlConsulta =
                url + "/Buscar/1?Desde=" + desdeFormateada + "&Hasta=" + hastaFormateada;
    

            var json = api.Get(urlConsulta);
            var listado = JsonConvert.DeserializeObject<List<OrdenAnodizado>>(json);

            return View(listado);
        }


        /// <summary>
        /// Consulta el listado
        /// </summary>
        /// <returns>Retorna una vista con el listado</returns>
        [SessionCheck]
        public ActionResult ListadoReproceso()
        {
            try
            {
                var json = api.Get(url + "/ListadoReproceso");
                var listado = JsonConvert.DeserializeObject<List<OrdenAnodizado>>(json);

                return View(listado);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, "Server error." + ex.Message);
                return View();
            }
        }


        /// <summary>
        /// Consulta un registro mediante su id
        /// </summary>
        /// <param name="id">Recibe el id</param>
        /// <returns>Retorna la vista de detalle</returns>
        [SessionCheck]
        public ActionResult Detalles(int id)
        {
            try
            {
                var json = api.Get(id, url + "/");
                var registro = JsonConvert.DeserializeObject<OrdenAnodizado>(json);

                return View(registro);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }

        /// <summary>
        /// Retorna una vista para crear un nuevo registro
        /// </summary>
        /// <returns>Retorna la vista</returns>
        [SessionCheck]
        public ActionResult Crear()
        {
            var orden = new OrdenAnodizado();

            orden.RecibidoActivo = false;
            orden.RaqueoActivo = false;
            orden.ProcesoActivo = false;
            orden.SalidaActivo = false;
            orden.EmpaqueActivo = false;
            orden.DespachoActivo = false;
            orden.NumeroLote = 1;
            orden.CantidadTotalLotes = 1;
            orden.CantidadTotal = 1;

            ViewBag.FichasTecnicas = new List<SelectListItem>();

            return View(orden);
        }

        /// <summary>
        /// Sirve para crear un nuevo registro
        /// </summary>
        /// <param name="ordenAnodizado">Recibe el objeto</param>
        /// <returns>Retorna la vista/returns>
        [SessionCheck]
        [HttpPost]
        public ActionResult Crear(OrdenAnodizado ordenAnodizado)
        {
            try
            {
                ordenAnodizado.RecibidoActivo = true;

                var jsonObj = JsonConvert.SerializeObject(ordenAnodizado);
                var result = api.Post(jsonObj, url);

                if (result)
                {
                    return RedirectToAction("Listado");
                }

                ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                return View();

            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }

        /// <summary>
        /// Metodo que sirve para consultar el registro a editar
        /// </summary>
        /// <param name="id">Recibe el id del registro</param>
        /// <returns>retorna la vista</returns>
        [SessionCheck]
        public ActionResult Editar(int id)
        {
            try
            {
                var json = api.Get(id, url + "/");
                var registro = JsonConvert.DeserializeObject<OrdenAnodizado>(json);

                var jsonFicha = api.Get(ApiUrl + "FichasTecnicas/ListadoDelCliente/" + registro.ClienteCode);
                var fichasListado = JsonConvert.DeserializeObject<List<FichaTecnica>>(jsonFicha);
                ViewBag.FichasTecnicas = fichasListado.Select(x => new SelectListItem { Value = x.Code + "", Text = x.Codigo });

                return View(registro);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }

        /// <summary>
        /// Metodo que sirve para editar el registro
        /// </summary>
        /// <param name="ordenAnodizado">Recibe el objeto</param>
        /// <returns>Retorna una vista</returns>
        [SessionCheck]
        [HttpPost]
        public ActionResult Editar(OrdenAnodizado ordenAnodizado)
        {
            try
            {
                var jsonObj = JsonConvert.SerializeObject(ordenAnodizado);
                var result = api.Put(jsonObj, url + "/");

                if (result)
                {
                    return RedirectToAction("Listado");
                }

                ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                return View();
            }
            catch (Exception)
            {
                return View();
            }
        }

        /// <summary>
        /// Metodo que sirve para consultar el registro a borrar
        /// </summary>
        /// <param name="id">Recibe el id</param>
        /// <returns>Retorna la vista</returns>
        [SessionCheck]
        public ActionResult Borrar(int? id)
        {
            try
            {
                var json = api.Get(id, url + "/");
                var registro = JsonConvert.DeserializeObject<OrdenAnodizado>(json);
                return View(registro);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }

        /// <summary>
        /// Metodo que sirve para borrar un registro
        /// </summary>
        /// <param name="id">Recibe el id</param>
        /// <returns>Retorna una vista</returns>
        [SessionCheck]
        [HttpPost]
        public ActionResult Borrar(int id)
        {
            try
            {
                var result = api.Delete(id, url + "/");

                if (result)
                {
                    return RedirectToAction("Listado");
                }

                ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                return View();
            }
            catch (Exception)
            {
                return View();
            }
        }

        /// <summary>
        /// Metodo para cargar los listados
        /// </summary>
        public void CargarListados()
        {
            var jsonClientes = api.Get(ApiUrl + "Clientes");
            var clientesListado = JsonConvert.DeserializeObject<List<Cliente>>(jsonClientes);
            ViewBag.Clientes = clientesListado.Select(x => new SelectListItem { Value = x.Code + "", Text = x.Nombre });

            var jsonEstados = api.Get(ApiUrl + "EstatusOrdenAnodizado");
            var estadosListado = JsonConvert.DeserializeObject<List<EstatusOrdenAnodizado>>(jsonEstados);
            ViewBag.Estados = estadosListado.Select(x => new SelectListItem { Value = x.Code + "", Text = x.Nombre });

            var jsonAccionesSalida = api.Get(ApiUrl + "AccionesSalida");
            var accionesSalidaListado = JsonConvert.DeserializeObject<List<AccionSalida>>(jsonAccionesSalida);
            ViewBag.AccionesSalida = accionesSalidaListado.Select(x => new SelectListItem { Value = x.Code + "", Text = x.Nombre });

           //listados de tipos de tratamientos
            var jsonTiposTratamiento = api.Get(ApiUrl + "TiposTratamiento");
            var tiposTratamientoListado = JsonConvert.DeserializeObject<List<TipoTratamiento>>(jsonTiposTratamiento);
            ViewBag.TiposTratamiento = tiposTratamientoListado.Select(x => new SelectListItem { Value = x.Code + "", Text = x.Nombre });

            //listado de tipos de materiales
            var jsonTiposMaterial = api.Get(ApiUrl + "TiposMaterial");
            var tiposMaterialesListado = JsonConvert.DeserializeObject<List<TipoMaterial>>(jsonTiposMaterial);
            ViewBag.TiposMaterial = tiposMaterialesListado.Select(x => new SelectListItem { Value = x.Code + "", Text = x.Nombre });

            //listado de tipos de prioridades
            var jsonTiposPrioridad = api.Get(ApiUrl + "TiposPrioridad");
            var tiposPrioridadListado = JsonConvert.DeserializeObject<List<TipoPrioridad>>(jsonTiposPrioridad);
            ViewBag.TiposPrioridad = tiposPrioridadListado.Select(x => new SelectListItem { Value = x.Code + "", Text = x.Nombre });
        }

        /// <summary>
        /// Metodo que sirve para guardar el encabezado de la orden de produccion
        /// </summary>
        /// <param name="clienteCode">codigo del cliente</param>
        /// <param name="cantidadTotal">cantidad total de partes</param>
        /// <param name="cantidadTotalLotes">cantidad total de lotes</param>
        /// <param name="fechaVencimiento">fecha de vencimiento</param>
        /// <param name="numeroLote">numero de lote</param>
        /// <param name="ordenTrabajo">orden de trabajo</param>
        /// <param name="revisionPlano">revision de plano</param>
        /// <param name="tipoTratamiento">tipo de tratamiento</param>
        /// <returns>retorna la respuesta</returns>
        public string CrearNueva
        (
                string clienteCode,
                string fichaCode,
                string cantidadTotal,
                string cantidadTotalLotes,
                string fechaVencimiento,
                string numeroLote,
                string ordenTrabajo,
                string tipoTratamiento,
                string revisionPlano,
                string tipoPrioridadCode
        )
        {
            var ordenAnodizado = new OrdenAnodizado();
            ordenAnodizado.RecibidoActivo = true;

            ordenAnodizado.ClienteCode = Convert.ToInt32(clienteCode);
            ordenAnodizado.FichaCode = Convert.ToInt32(fichaCode);
            ordenAnodizado.CantidadTotal = Convert.ToInt32(cantidadTotal);
            ordenAnodizado.CantidadTotalLotes = Convert.ToInt32(cantidadTotalLotes);
            ordenAnodizado.FechaVencimiento = Convert.ToDateTime(fechaVencimiento);
            ordenAnodizado.NumeroLote = Convert.ToInt32(numeroLote);
            ordenAnodizado.OrdenTrabajo = ordenTrabajo;
            ordenAnodizado.RevisionPlano = revisionPlano;
            ordenAnodizado.TipoTratamiento = Convert.ToInt32(tipoTratamiento);
            ordenAnodizado.TipoPrioridadCode = Convert.ToInt32(tipoPrioridadCode);

            var usuario = System.Web.HttpContext.Current.Session["Usuario.NombreUsuario"].ToString();
            ordenAnodizado.CreadoPor = usuario;
            ordenAnodizado.FechaCreacion = DateTime.Now;

            var jsonObj = JsonConvert.SerializeObject(ordenAnodizado);
            var secuencial = api.PostReturnInfo(jsonObj, url);

            return secuencial;
        }

        /// <summary>
        /// Metodo que sirve para guardar el encabezado de la orden de produccion
        /// </summary>
        /// <param name="clienteCode">codigo del cliente</param>
        /// <param name="cantidadTotal">cantidad total de partes</param>
        /// <param name="cantidadTotalLotes">cantidad total de lotes</param>
        /// <param name="fechaVencimiento">fecha de vencimiento</param>
        /// <param name="numeroLote">numero de lote</param>
        /// <param name="ordenTrabajo">orden de trabajo</param>
        /// <param name="revisionPlano">revision de plano</param>
        /// <param name="tipoTratamiento">tipo de tratamiento</param>
        /// <returns>retorna la respuesta</returns>
        public string GuardarEncabezado
        (
                string code,
                string codigo,
                string fichaCode,
                string clienteCode,
                string cantidadTotal,
                string cantidadTotalLotes,
                string fechaVencimiento,
                string numeroLote,
                string ordenTrabajo,
                string revisionPlano,
                string tipoTratamiento,
                string tipoPrioridadCode
        )
        {
            var ordenAnodizado = new OrdenAnodizado();
            ordenAnodizado.RecibidoActivo = true;

            ordenAnodizado.Code = Convert.ToInt32(code);
            ordenAnodizado.Codigo = codigo;
            ordenAnodizado.FichaCode = Convert.ToInt32(fichaCode);
            ordenAnodizado.ClienteCode = Convert.ToInt32(clienteCode);
            ordenAnodizado.CantidadTotal = Convert.ToInt32(cantidadTotal);
            ordenAnodizado.CantidadTotalLotes = Convert.ToInt32(cantidadTotalLotes);
            ordenAnodizado.FechaVencimiento = Convert.ToDateTime(fechaVencimiento);
            ordenAnodizado.NumeroLote = Convert.ToInt32(numeroLote);
            ordenAnodizado.OrdenTrabajo = ordenTrabajo;
            ordenAnodizado.RevisionPlano = revisionPlano;
            ordenAnodizado.TipoTratamiento = Convert.ToInt32(tipoTratamiento);
            ordenAnodizado.TipoPrioridadCode = Convert.ToInt32(tipoPrioridadCode);

            var usuario = System.Web.HttpContext.Current.Session["Usuario.NombreUsuario"].ToString();
            ordenAnodizado.ActualizadoPor = usuario;
            ordenAnodizado.FechaActualizacion = DateTime.Now;

            ordenAnodizado.RecibidoActivo = true;

            var jsonObj = JsonConvert.SerializeObject(ordenAnodizado);
            var respuesta = api.Post(jsonObj, url + "/GuardarEncabezado");

            if (respuesta)
            {
                return "Orden actualizada correctamente";
            }
            else
            {
                return "Se presento un error al guardar";
            }
        }

        /// <summary>
        /// Metodo que sirve para guardar la informacion de la inspeccion de recibo
        /// </summary>
        /// <param name="recibidoCodigo">guarda el codigo de recibo</param>
        /// <param name="recibidoCantidad">cantidad recibo</param>
        /// <param name="recibidoMedidasCriticas">recibo medidas criticas</param>
        /// <param name="recibidoResponsable">responsable</param>
        /// <param name="recibidoFecha">fecha</param>
        /// <returns>retorna la respuesta</returns>
        public string GuardarInspeccionRecibo
        (
             string code,
             string recibidoCodigo,
             string recibidoCantidad,
             string recibidoMedidasCriticas,
             string recibidoResponsable,
             string recibidoFecha
        )
        {
            var ordenAnodizado = new OrdenAnodizado();

            ordenAnodizado.RaqueoActivo = true;

            ordenAnodizado.Code = Convert.ToInt32(code);
            ordenAnodizado.RecibidoCodigo = recibidoCodigo;
            ordenAnodizado.RecibidoCantidad = Convert.ToInt32(recibidoCantidad);
            ordenAnodizado.RecibidoMedidasCriticas = recibidoMedidasCriticas;
            ordenAnodizado.RecibidoResponsable = recibidoResponsable;
            ordenAnodizado.RecibidoFecha = Convert.ToDateTime(recibidoFecha);

            var usuario = System.Web.HttpContext.Current.Session["Usuario.NombreUsuario"].ToString();
            ordenAnodizado.ActualizadoPor = usuario;
            ordenAnodizado.FechaActualizacion = DateTime.Now;

            ordenAnodizado.RaqueoActivo = true;

            var jsonObj = JsonConvert.SerializeObject(ordenAnodizado);
            var result = api.Post(jsonObj, url + "/GuardarInspeccionRecibo");

            if (result)
            {
                return "Datos guardados correctamente";
            }
            return "Se presento un problema al guardar los datos";
        }

        /// <summary>
        /// Metodo que sirve para guardar la informacion del raqueo
        /// </summary>
        /// <param name="raqueoRecomendacion">recomendacion</param>
        /// <param name="raqueoCantidad">cantidad</param>
        /// <param name="raqueoResponsable">responsable</param>
        /// <param name="RaqueoFecha">fecha</param>
        /// <returns>Retorna la respuesta</returns>
        public string GuardarRaqueo
        (
            string code,
            string raqueoRecomendacion,
            string raqueoCantidad,
            string raqueoResponsable,
            string raqueoFecha
        )
        {
            var ordenAnodizado = new OrdenAnodizado();

            ordenAnodizado.ProcesoActivo = true;

            ordenAnodizado.Code = Convert.ToInt32(code);
            ordenAnodizado.RaqueoRecomendacion = raqueoRecomendacion;
            ordenAnodizado.RaqueoCantidad = Convert.ToInt32(raqueoCantidad);
            ordenAnodizado.RaqueoResponsable = raqueoResponsable;
            ordenAnodizado.RaqueoFecha = Convert.ToDateTime(raqueoFecha);

            var usuario = System.Web.HttpContext.Current.Session["Usuario.NombreUsuario"].ToString();
            ordenAnodizado.ActualizadoPor = usuario;
            ordenAnodizado.FechaActualizacion = DateTime.Now;

            ordenAnodizado.ProcesoActivo = true;

            var jsonObj = JsonConvert.SerializeObject(ordenAnodizado);
            var result = api.Post(jsonObj, url + "/GuardarRaqueo");

            if (result)
            {
                return "Datos guardados correctamente";
            }
            return "Se presento un problema al guardar los datos";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="procesoDesengraseMinutos">desangre minutos</param>
        /// <param name="procesoDesengraseTanque">dasngre tanque</param>
        /// <param name="procesoDesengraseHoraInicio">desangre inicio</param>
        /// <param name="procesoDesengraseHoraHoraFinal">desangre final</param>
        /// <param name="procesoEtching">tiempo etching</param>
        /// <param name="procesoEtchingTanque">etching tanque</param>
        /// <param name="procesoEtchingHoraInicial">etching inicio</param>
        /// <param name="procesoEtchingHoraFinal">etching fin</param>
        /// <param name="procesoAmperaje">tiempo amperaje</param>
        /// <param name="procesoAmperajeTanque">amperaje tanque</param>
        /// <param name="procesoAmperajeHoraInicio">amperaje inicio</param>
        /// <param name="procesoAmperajeHoraFinal">amperaje final</param>
        /// <param name="procesoAnodizadoMinutos">anodizado miniutos</param>
        /// <param name="procesoAnodizadoTanque">anodizado tanque</param>
        /// <param name="procesoAnodizadoHoraInicio">anodizado inicio</param>
        /// <param name="procesoAnodizadoHoraFinal">anodizado fin</param>
        /// <param name="procesoTintaMinutos">anodizado fin</param>
        /// <param name="procesoTintaTanque">tinta tanque</param>
        /// <param name="procesoTintaHoraInicio">tinta inicio</param>
        /// <param name="procesoTintaHoraFinal">tinta fin</param>
        /// <param name="procesoSelloMinutos">sello minutos</param>
        /// <param name="procesoSelloTanque">sello tanque</param>
        /// <param name="procesoSelloHoraInicial">sello inicio</param>
        /// <param name="procesoSelloHoraFinal">sello fin</param>
        /// <param name="procesoOtros">otros</param>
        /// <param name="procesoOtrosMinutos">otros minutos</param>
        /// <param name="procesoOtrosTanque">otros tanque</param>
        /// <param name="procesoOtrosHoraInicial">otros hora inicial</param>
        /// <param name="procesoOtrosHoraFinal">otros hora final</param>
        /// <param name="procesoCiudadosEspeciales">cuidados especiales</param>
        /// <param name="procesoResponsable">responsable</param>
        /// <returns>retorna la respuesta</returns>
        public string GuardarProceso
        (
            string code,
            string procesoDesengraseMinutos,
            string procesoDesengraseTanque,
            string procesoDesengraseHoraInicio,
            string procesoDesengraseHoraHoraFinal,
            string procesoEtching,
            string procesoEtchingTanque,
            string procesoEtchingHoraInicial,
            string procesoEtchingHoraFinal,
            string procesoAmperaje,
            string procesoAmperajeTanque,
            string procesoAmperajeHoraInicio,
            string procesoAmperajeHoraFinal,
            string procesoAnodizadoMinutos,
            string procesoAnodizadoTanque,
            string procesoAnodizadoHoraInicio,
            string procesoAnodizadoHoraFinal,
            string procesoTintaMinutos,
            string procesoTintaTanque,
            string procesoTintaHoraInicio,
            string procesoTintaHoraFinal,
            string procesoSelloMinutos,
            string procesoSelloTanque,
            string procesoSelloHoraInicial,
            string procesoSelloHoraFinal,
            string procesoOtros,
            string procesoOtrosMinutos,
            string procesoOtrosTanque,
            string procesoOtrosHoraInicial,
            string procesoOtrosHoraFinal,
            string procesoCiudadosEspeciales,
            string procesoResponsable
        )
        {
            var ordenAnodizado = new OrdenAnodizado();
            ordenAnodizado.Code = Convert.ToInt32(code);

            ordenAnodizado.SalidaActivo = true;

            ordenAnodizado.ProcesoDesengraseMinutos = Formatos.GetDecimal(procesoDesengraseMinutos);
            ordenAnodizado.ProcesoDesengraseTanque = Formatos.GetInt32(procesoDesengraseTanque);
            ordenAnodizado.ProcesoDesengraseHoraInicio = Formatos.GetTime(procesoDesengraseHoraInicio);
            ordenAnodizado.ProcesoDesengraseHoraFinal = Formatos.GetTime(procesoDesengraseHoraHoraFinal);

            ordenAnodizado.ProcesoEtching = Formatos.GetDecimal(procesoEtching);
            ordenAnodizado.ProcesoEtchingTanque = Formatos.GetInt32(procesoEtchingTanque);
            ordenAnodizado.ProcesoEtchingHoraInicial = Formatos.GetTime(procesoEtchingHoraInicial);
            ordenAnodizado.ProcesoEtchingHoraFinal = Formatos.GetTime(procesoEtchingHoraFinal);

            ordenAnodizado.ProcesoAmperaje = Formatos.GetDecimal(procesoAmperaje);
            ordenAnodizado.ProcesoAmperajeTanque = Formatos.GetInt32(procesoAmperajeTanque);
            ordenAnodizado.ProcesoAmperajeHoraInicio = Formatos.GetTime(procesoAmperajeHoraInicio);
            ordenAnodizado.ProcesoAmperajeHoraFinal = Formatos.GetTime(procesoAmperajeHoraFinal);

            ordenAnodizado.ProcesoAnodizadoMinutos = Formatos.GetDecimal(procesoAnodizadoMinutos);
            ordenAnodizado.ProcesoAnodizadoTanque = Formatos.GetInt32(procesoAnodizadoTanque);
            ordenAnodizado.ProcesoAnodizadoHoraInicio = Formatos.GetTime(procesoAnodizadoHoraInicio);
            ordenAnodizado.ProcesoAnodizadoHoraFinal = Formatos.GetTime(procesoAnodizadoHoraFinal);

            ordenAnodizado.ProcesoTintaMinutos = Formatos.GetDecimal(procesoTintaMinutos);
            ordenAnodizado.ProcesoTintaTanque = Formatos.GetInt32(procesoTintaTanque);
            ordenAnodizado.ProcesoTintaHoraInicio = Formatos.GetTime(procesoTintaHoraInicio);
            ordenAnodizado.ProcesoTintaHoraFinal = Formatos.GetTime(procesoTintaHoraFinal);

            ordenAnodizado.ProcesoSelloMinutos = Formatos.GetDecimal(procesoSelloMinutos);
            ordenAnodizado.ProcesoSelloTanque = Formatos.GetInt32(procesoSelloTanque);
            ordenAnodizado.ProcesoSelloHoraInicial = Formatos.GetTime(procesoSelloHoraInicial);
            ordenAnodizado.ProcesoSelloHoraFinal = Formatos.GetTime(procesoSelloHoraFinal);

            ordenAnodizado.ProcesoOtros = procesoOtros;
            ordenAnodizado.ProcesoOtrosMinutos = Formatos.GetDecimal(procesoOtrosMinutos);
            ordenAnodizado.ProcesoOtrosTanque = Formatos.GetInt32(procesoOtrosTanque);
            ordenAnodizado.ProcesoOtrosHoraInicial = Formatos.GetTime(procesoOtrosHoraInicial);
            ordenAnodizado.ProcesoOtrosHoraFinal = Formatos.GetTime(procesoOtrosHoraFinal);

            ordenAnodizado.ProcesoCiudadosEspeciales = procesoCiudadosEspeciales;
            ordenAnodizado.ProcesoResponsable = procesoResponsable;


            var usuario = System.Web.HttpContext.Current.Session["Usuario.NombreUsuario"].ToString();
            ordenAnodizado.ActualizadoPor = usuario;
            ordenAnodizado.FechaActualizacion = DateTime.Now;

            ordenAnodizado.SalidaActivo = true;

            var jsonObj = JsonConvert.SerializeObject(ordenAnodizado);
            var result = api.Post(jsonObj, url + "/GuardarProceso");

            if (result)
            {
                return "Datos guardados correctamente";
            }
            return "Se presento un problema al guardar los datos";
        }



        /// <summary>
        /// Metodo que sirve para guardar los datos de la salida
        /// </summary>
        /// <param name="salidaCantiAprobada">cantidad aprobadas</param>
        /// <param name="salidaCantiRechazada">cantidad rechazadas</param>
        /// <param name="salidaCausas">causas</param>
        /// <param name="salidaAccion">accion</param>
        /// <param name="salidaAprobadas">aprobadas</param>
        /// <param name="salidaFecha">fecha</param>
        /// <param name="salidaResponsable">responsable</param>
        /// <returns>Retorna la respuesta</returns>
        public string GuardarSalida
        (
            string code,
            string salidaCriteriosAceptacion,
            string salidaCantiAprobada,
            string salidaCantiRechazada,
            string salidaCausas,
            string salidaAccion,
            string salidaAprobadas,
            string salidaFecha,
            string salidaResponsable
        )
        {
            var ordenAnodizado = new OrdenAnodizado();
            ordenAnodizado.Code = Convert.ToInt32(code);

            ordenAnodizado.EmpaqueActivo = true;

            ordenAnodizado.SalidaCriteriosAceptacion = salidaCriteriosAceptacion;

            ordenAnodizado.SalidaCantiRechazada = Convert.ToInt32(salidaCantiRechazada);
            ordenAnodizado.SalidaCantiAprobada = Convert.ToInt32(salidaCantiAprobada);
            ordenAnodizado.SalidaCantiRechazada = Convert.ToInt32(salidaCantiRechazada);
            ordenAnodizado.SalidaCausas = salidaCausas;

            if (!String.IsNullOrEmpty(salidaAccion))
            {
                ordenAnodizado.SalidaAccion = Convert.ToInt32(salidaAccion);
            }
           
            ordenAnodizado.SalidaAprobadas = Convert.ToBoolean(salidaAprobadas);
            ordenAnodizado.SalidaFecha = Convert.ToDateTime(salidaFecha);
            ordenAnodizado.SalidaResponsable = salidaResponsable;


            var usuario = System.Web.HttpContext.Current.Session["Usuario.NombreUsuario"].ToString();
            ordenAnodizado.ActualizadoPor = usuario;
            ordenAnodizado.FechaActualizacion = DateTime.Now;

            ordenAnodizado.EmpaqueActivo = true;

            var jsonObj = JsonConvert.SerializeObject(ordenAnodizado);
            var result = api.Post(jsonObj, url + "/GuardarSalida");

            if (result)
            {
                return "Datos guardados correctamente";
            }
            return "Se presento un problema al guardar los datos";
        }

        /// <summary>
        /// Metodo que sirve para guardar el empaque
        /// </summary>
        /// <param name="empaqueRecomendacion">recomendacion</param>
        /// <param name="empaqueCantidad">cantidad</param>
        /// <param name="empaqueFecha">fecha</param>
        /// <param name="empaqueResponsable">responsable</param>
        /// <returns>Retorna la respuesta</returns>
        public string GuardarEmpaque
        (
            string code,
            string empaqueRecomendacion,
            string empaqueCantidad,
            string empaqueFecha,
            string empaqueResponsable
        )
        {
            var ordenAnodizado = new OrdenAnodizado();

            ordenAnodizado.DespachoActivo = true;

            ordenAnodizado.Code = Convert.ToInt32(code);
            ordenAnodizado.EmpaqueRecomendacion = empaqueRecomendacion;
            ordenAnodizado.EmpaqueCantidad = Convert.ToInt32(empaqueCantidad);
            ordenAnodizado.EmpaqueFecha = Convert.ToDateTime(empaqueFecha);
            ordenAnodizado.EmpaqueResponsable = empaqueResponsable;

            var usuario = System.Web.HttpContext.Current.Session["Usuario.NombreUsuario"].ToString();
            ordenAnodizado.ActualizadoPor = usuario;
            ordenAnodizado.FechaActualizacion = DateTime.Now;

            ordenAnodizado.DespachoActivo = true;

            var jsonObj = JsonConvert.SerializeObject(ordenAnodizado);
            var result = api.Post(jsonObj, url + "/GuardarEmpaque");

            if (result)
            {
                return "Datos guardados correctamente";
            }
            return "Se presento un problema al guardar los datos";
        }

        /// <summary>
        /// Metodo que sirve para guardar la informacion del despacho
        /// </summary>
        /// <param name="despachoCantidad">cantidad</param>
        /// <param name="despachoFactura">factura</param>
        /// <param name="despachoFecha">fecha</param>
        /// <param name="despachoResponsable">responsable</param>
        /// <returns>Retorna la respuesta</returns>
        public string GuardarDespacho
        (
            string code,
            string despachoCantidad,
            string despachoFactura,
            string despachoCertificado,
            string despachoFecha,
            string despachoResponsable
        )
        {
            var ordenAnodizado = new OrdenAnodizado();
            ordenAnodizado.Code = Convert.ToInt32(code);
            ordenAnodizado.DespachoCantidad = Convert.ToInt32(despachoCantidad);
            ordenAnodizado.DespachoFactura = despachoFactura;
            ordenAnodizado.DespachoCertificado = despachoCertificado;
            ordenAnodizado.DespachoFecha = Formatos.GetDateTime(despachoFecha);
            ordenAnodizado.DespachoResponsable = despachoResponsable;

            var jsonObj = JsonConvert.SerializeObject(ordenAnodizado);
            var result = api.Post(jsonObj, url + "/GuardarDespacho");

            if (result)
            {
                return "Datos guardados correctamente";
            }
            return "Se presento un problema al guardar los datos";
        }
    }
}