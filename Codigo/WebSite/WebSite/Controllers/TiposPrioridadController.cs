﻿using Business;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebSite.App_Start;
using WebSite.Models;

namespace WebSite.Controllers
{
    /// <summary>
    /// Nombre de la clase: TiposPrioridadController
    /// </summary>
    public class TiposPrioridadController : Controller
    {
        /// <summary>
        /// Se declara un objeto Api
        /// </summary>
        private Api api;

        /// <summary>
        /// Url del API
        /// </summary>
        private string ApiUrl;

        /// <summary>
        /// Url con el controlador
        /// </summary>
        private string url;

        /// <summary>
        /// Constructor
        /// </summary>
        public TiposPrioridadController()
        {
            api = new Api();
            ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
            url = ApiUrl + "TiposPrioridad";
        }

        /// <summary>
        /// Consulta el listado
        /// </summary>
        /// <returns>Retorna una vista con el listado</returns>
        [SessionCheck]
        public ActionResult Listado()
        {
            try
            {
                var json = api.Get(url);
                var listado = JsonConvert.DeserializeObject<List<TipoPrioridad>>(json);

                return View(listado);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }


        /// <summary>
        /// Consulta un registro mediante su id
        /// </summary>
        /// <param name="id">Recibe el id</param>
        /// <returns>Retorna la vista de detalle</returns>
        [SessionCheck]
        public ActionResult Detalles(int id)
        {
            try
            {
                var json = api.Get(id, url + "/");
                var registro = JsonConvert.DeserializeObject<TipoPrioridad>(json);

                return View(registro);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }

        /// <summary>
        /// Retorna una vista para crear un nuevo registro
        /// </summary>
        /// <returns>Retorna la vista</returns>
        [SessionCheck]
        public ActionResult Crear()
        {
            return View(new TipoPrioridad());
        }

        /// <summary>
        /// Sirve para crear un nuevo registro
        /// </summary>
        /// <param name="tipoPrioridad">Recibe el objeto</param>
        /// <returns>Retorna la vista/returns>
        [SessionCheck]
        [HttpPost]
        public ActionResult Crear(TipoPrioridad tipoPrioridad)
        {
            try
            {
                var usuario = System.Web.HttpContext.Current.Session["Usuario.NombreUsuario"].ToString();
                tipoPrioridad.CreateUser = usuario;
                tipoPrioridad.CreateDate = DateTime.Now;

                var jsonObj = JsonConvert.SerializeObject(tipoPrioridad);
                var result = api.Post(jsonObj, url);

                if (result)
                {
                    return RedirectToAction("Listado");
                }

                ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                return View();

            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }

        /// <summary>
        /// Metodo que sirve para consultar el registro a editar
        /// </summary>
        /// <param name="id">Recibe el id del registro</param>
        /// <returns>retorna la vista</returns>
        [SessionCheck]
        public ActionResult Editar(int id)
        {
            try
            {
                var json = api.Get(id, url + "/");
                var registro = JsonConvert.DeserializeObject<TipoPrioridad>(json);
                return View(registro);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }

        /// <summary>
        /// Metodo que sirve para editar el registro
        /// </summary>
        /// <param name="tipoPrioridad">Recibe el objeto</param>
        /// <returns>Retorna una vista</returns>
        [SessionCheck]
        [HttpPost]
        public ActionResult Editar(TipoPrioridad tipoPrioridad)
        {
            try
            {
                var usuario = System.Web.HttpContext.Current.Session["Usuario.NombreUsuario"].ToString();
                tipoPrioridad.UpdateUser = usuario;
                tipoPrioridad.UpdateDate = DateTime.Now;

                var jsonObj = JsonConvert.SerializeObject(tipoPrioridad);
                var result = api.Put(jsonObj, url + "/");

                if (result)
                {
                    return RedirectToAction("Listado");
                }

                ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                return View();
            }
            catch (Exception)
            {
                return View();
            }
        }

        /// <summary>
        /// Metodo que sirve para consultar el registro a borrar
        /// </summary>
        /// <param name="id">Recibe el id</param>
        /// <returns>Retorna la vista</returns>
        [SessionCheck]
        public ActionResult Borrar(int? id)
        {
            try
            {
                var json = api.Get(id, url + "/");
                var registro = JsonConvert.DeserializeObject<TipoPrioridad>(json);
                return View(registro);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }

        /// <summary>
        /// Metodo que sirve para borrar un registro
        /// </summary>
        /// <param name="id">Recibe el id</param>
        /// <returns>Retorna una vista</returns>
        [SessionCheck]
        [HttpPost]
        public ActionResult Borrar(int id)
        {
            try
            {
                var result = api.Delete(id, url + "/");

                if (result)
                {
                    return RedirectToAction("Listado");
                }

                ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                return View();
            }
            catch (Exception)
            {
                return View();
            }
        }
    }
}