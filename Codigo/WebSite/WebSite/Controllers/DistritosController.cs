﻿using Business;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebSite.App_Start;
using WebSite.Models;

namespace WebSite.Controllers
{
    /// <summary>
    /// Nombre de la clase: DistritosController
    /// </summary>
    public class DistritosController : Controller
    {
        private Api api;
        private string ApiUrl;
        private string url;

        /// <summary>
        /// Constructor
        /// </summary>
        public DistritosController()
        {
            api = new Api();
            ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
            url = ApiUrl + "Distritos";
            CargarListados();
        }

        /// <summary>
        /// Consulta el listado
        /// </summary>
        /// <returns>Retorna una vista con el listado</returns>
        [SessionCheck]
        public ActionResult Listado()
        {
            try
            {
                var json = api.Get(url);
                var listado = JsonConvert.DeserializeObject<List<Distrito>>(json);

                return View(listado);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }


        /// <summary>
        /// Consulta un registro mediante su id
        /// </summary>
        /// <param name="id">Recibe el id</param>
        /// <returns>Retorna la vista de detalle</returns>
        [SessionCheck]
        public ActionResult Detalles(int provinciaCode, int cantonCode, int distritoCode)
        {
            try
            {

                var ulrconsulta = url + "Buscar?provinciaCode=" + provinciaCode + "&cantonCode=" + cantonCode + "&distrito=" + distritoCode;
                var json = api.Get(ulrconsulta);
                var registro = JsonConvert.DeserializeObject<Distrito>(json);

                return View(registro);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }

        /// <summary>
        /// Retorna una vista para crear un nuevo registro
        /// </summary>
        /// <returns>Retorna la vista</returns>
        [SessionCheck]
        public ActionResult Crear()
        {
            return View(new Distrito());
        }

        /// <summary>
        /// Sirve para crear un nuevo registro
        /// </summary>
        /// <param name="distrito">Recibe el objeto</param>
        /// <returns>Retorna la vista/returns>
        [SessionCheck]
        [HttpPost]
        public ActionResult Crear(Distrito distrito)
        {
            try
            {
                var jsonObj = JsonConvert.SerializeObject(distrito);
                var result = api.Post(jsonObj, url);

                if (result)
                {
                    return RedirectToAction("Listado");
                }

                ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                return View();

            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }

        /// <summary>
        /// Metodo que sirve para consultar el registro a editar
        /// </summary>
        /// <param name="id">Recibe el id del registro</param>
        /// <returns>retorna la vista</returns>
        [SessionCheck]
        public ActionResult Editar(int provinciaCode, int cantonCode, int distritoCode)
        {
            try
            {
                var ulrconsulta = url + "Buscar?provinciaCode=" + provinciaCode + "&cantonCode=" + cantonCode + "&distrito=" + distritoCode;
                var json = api.Get(ulrconsulta);
                var registro = JsonConvert.DeserializeObject<Distrito>(json);
                return View(registro);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }

        /// <summary>
        /// Metodo que sirve para editar el registro
        /// </summary>
        /// <param name="distrito">Recibe el objeto</param>
        /// <returns>Retorna una vista</returns>
        [SessionCheck]
        [HttpPost]
        public ActionResult Editar(Distrito distrito)
        {
            try
            {
                var jsonObj = JsonConvert.SerializeObject(distrito);
                var result = api.Put(jsonObj, url + "/");

                if (result)
                {
                    return RedirectToAction("Listado");
                }

                ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                return View();
            }
            catch (Exception)
            {
                return View();
            }
        }

        /// <summary>
        /// Metodo que sirve para consultar el registro a borrar
        /// </summary>
        /// <param name="id">Recibe el id</param>
        /// <returns>Retorna la vista</returns>
        [SessionCheck]
        public ActionResult Borrar(int? provinciaCode, int? cantonCode, int? distritoCode)
        {
            try
            {
                var ulrconsulta = url + "Buscar?provinciaCode=" + provinciaCode + "&cantonCode=" + cantonCode + "&distrito=" + distritoCode;
                var json = api.Get(ulrconsulta);
                var registro = JsonConvert.DeserializeObject<Distrito>(json);
                return View(registro);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }

        /// <summary>
        /// Metodo que sirve para borrar un registro
        /// </summary>
        /// <param name="id">Recibe el id</param>
        /// <returns>Retorna una vista</returns>
        [SessionCheck]
        [HttpPost]
        public ActionResult Borrar(int provinciaCode, int cantonCode, int distritoCode)
        {
            try
            {
                //var result = api.Delete(id, url + "/");

                if (true)
                {
                    return RedirectToAction("Listado");
                }

                ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                return View();
            }
            catch (Exception)
            {
                return View();
            }
        }

        public void CargarListados()
        {
            var jsonProvincias = api.Get(ApiUrl + "Provincias");
            var provincias = JsonConvert.DeserializeObject<List<Provincia>>(jsonProvincias);
            ViewBag.Provincias = provincias.Select(x => new SelectListItem { Value = x.Code + "", Text = x.Nombre });

            var jsonCantones = api.Get(ApiUrl + "Cantones");
            var cantones = JsonConvert.DeserializeObject<List<Canton>>(jsonCantones);
            ViewBag.Cantones = provincias.Select(x => new SelectListItem { Value = x.Code + "", Text = x.Nombre });
        }
    }
}