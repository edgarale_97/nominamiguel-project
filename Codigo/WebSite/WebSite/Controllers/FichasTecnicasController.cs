﻿using Business;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebSite.App_Start;
using WebSite.Models;

namespace WebSite.Controllers
{
    /// <summary>
    /// Nombre de la clase: FichasTecnicasController
    /// </summary>
    public class FichasTecnicasController : Controller
    {
        /// <summary>
        /// Se declara un objeto Api
        /// </summary>
        private Api api;

        /// <summary>
        /// Url del API
        /// </summary>
        private string ApiUrl;

        /// <summary>
        /// Url con el controlador
        /// </summary>
        private string url;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public FichasTecnicasController()
        {
            api = new Api();
            ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
            url = ApiUrl + "FichasTecnicas";
            CargarListados();
        }

        /// <summary>
        /// Consulta el listado
        /// </summary>
        /// <returns>Retorna una vista con el listado</returns>
        [SessionCheck]
        public ActionResult Listado()
        {
            try
            {
                var json = api.Get(url);
                var listado = JsonConvert.DeserializeObject<List<FichaTecnica>>(json);

                return View(listado);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }

        /// <summary>
        /// Metodo que sirve para obtener el listado de fichas de un cliente en formato json
        /// </summary>
        /// <param name="fichaCode">Recibe el codigo del cliente</param>
        /// <returns>Retorna el json</returns>
        public string ListadoJSON(int ClienteCode)
        {
            var ulrListadoFiltrado = url + "/ListadoDelCliente/" + ClienteCode;
            var listadoJson = api.Get(ulrListadoFiltrado);

            return listadoJson;
        }

        /// <summary>
        /// Consulta un registro mediante su id
        /// </summary>
        /// <param name="id">Recibe el id</param>
        /// <returns>Retorna la vista de detalle</returns>
        [SessionCheck]
        public ActionResult Detalles(int id)
        {
            try
            {
                var json = api.Get(id, url + "/");
                var registro = JsonConvert.DeserializeObject<FichaTecnica>(json);

                return View(registro);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }

        /// <summary>
        /// Metodo que sirve para retornar el detalle en formato json
        /// </summary>
        /// <param name="id">Recibe el id de la ficha</param>
        /// <returns>Retorna el json</returns>
        public string DetalleJson(int id)
        {
            var json = api.Get(id, url + "/");

            return json;
        }

        /// <summary>
        /// Retorna una vista para crear un nuevo registro
        /// </summary>
        /// <returns>Retorna la vista</returns>
        [SessionCheck]
        public ActionResult Crear()
        {
            return View(new FichaTecnica());
        }

        /// <summary>
        /// Metodo que sirve para crear una ficha tecnica
        /// </summary>
        /// <param name="fichaTecnica">Objeto FichaTecnica</param>
        /// <param name="fileFotoRecibo1">Foto Recibo 1</param>
        /// <param name="fileFotoRecibo2">Foto Recibo 2</param>
        /// <param name="fileFotoRecibo3">Foto Recibo 3</param>
        /// <param name="fileFotoRaqueo1">Foto Raqueo 1</param>
        /// <param name="fileFotoRaqueo2">Foto Raqueo 2</param>
        /// <param name="fileFotoRaqueo3">Foto Raqueo 3</param>
        /// <param name="fileFotoEmpaque1">Foto Empaque 1</param>
        /// <param name="fileFotoEmpaque2">Foto Empaque 2</param>
        /// <param name="fileFotoEmpaque3">Foto Empaque 3</param>
        /// <param name="fileFotoSalida1">Foto Salida 1</param>
        /// <param name="fileFotoSalida2">Foto Salida 2</param>
        /// <param name="fileFotoSalida3">Foto Salida 3</param>
        /// <returns>Retorna la vista correspondiente al resultado</returns>
        [HttpPost]
        public ActionResult Crear(
            FichaTecnica fichaTecnica,
            HttpPostedFileBase fileFotoRecibo1,
            HttpPostedFileBase fileFotoRecibo2,
            HttpPostedFileBase fileFotoRecibo3,
            HttpPostedFileBase fileFotoRaqueo1,
            HttpPostedFileBase fileFotoRaqueo2,
            HttpPostedFileBase fileFotoRaqueo3,
            HttpPostedFileBase fileFotoEmpaque1,
            HttpPostedFileBase fileFotoEmpaque2,
            HttpPostedFileBase fileFotoEmpaque3,
            HttpPostedFileBase fileFotoSalida1,
            HttpPostedFileBase fileFotoSalida2,
            HttpPostedFileBase fileFotoSalida3
            )
        {
            try
            {
                //se declara un nuevo objeto FileManager
                var fileManager = new FileManager();
                var usuario = System.Web.HttpContext.Current.Session["Usuario.NombreUsuario"].ToString();

                //valida que las fotos de recibido no vengan vacias
                #region FOTOS RECIBO
                var fotosRecibo = new List<FotoRecibo>();

                //se valida que la primer foto de recibo no venga nula
                if (fileFotoRecibo1 != null)
                {
                    var logotipoBytes = fileManager.FileToByteArray(fileFotoRecibo1);
                    var logotipoBase64 = Convert.ToBase64String(logotipoBytes);

                    var fotoRecibo = new FotoRecibo();
                    fotoRecibo.ArchivoBase64 = logotipoBase64;
                    fotoRecibo.ArchivoNombre = fileFotoRecibo1.FileName;
                    fotoRecibo.ArchivoType = fileFotoRecibo1.ContentType;
                    fotoRecibo.CreateDate = DateTime.Now;
                    fotoRecibo.CreateUser = usuario;

                    fotosRecibo.Add(fotoRecibo);
                }

                //se valida que la segunda foto de recibo no venga nula
                if (fileFotoRecibo2 != null)
                {
                    var logotipoBytes = fileManager.FileToByteArray(fileFotoRecibo2);
                    var logotipoBase64 = Convert.ToBase64String(logotipoBytes);

                    var fotoRecibo = new FotoRecibo();
                    fotoRecibo.ArchivoBase64 = logotipoBase64;
                    fotoRecibo.ArchivoNombre = fileFotoRecibo2.FileName;
                    fotoRecibo.ArchivoType = fileFotoRecibo2.ContentType;
                    fotoRecibo.CreateDate = DateTime.Now;
                    fotoRecibo.CreateUser = usuario;

                    fotosRecibo.Add(fotoRecibo);
                }

                //se valida que la tercer foto de recibo no venga nula
                if (fileFotoRecibo3 != null)
                {
                    var logotipoBytes = fileManager.FileToByteArray(fileFotoRecibo3);
                    var logotipoBase64 = Convert.ToBase64String(logotipoBytes);

                    var fotoRecibo = new FotoRecibo();
                    fotoRecibo.ArchivoBase64 = logotipoBase64;
                    fotoRecibo.ArchivoNombre = fileFotoRecibo2.FileName;
                    fotoRecibo.ArchivoType = fileFotoRecibo2.ContentType;
                    fotoRecibo.CreateDate = DateTime.Now;
                    fotoRecibo.CreateUser = usuario;

                    fotosRecibo.Add(fotoRecibo);
                }

                if(fotosRecibo.Count() > 0)
                {
                    fichaTecnica.FotosRecibo = fotosRecibo;
                }

                #endregion

                //valida que las fotos de raqueo no vengan vacias
                #region FOTOS RAQUEO
                var fotosRaqueo = new List<FotoRaqueo>();

                //se valida que la primer foto de raqueo no venga nula
                if (fileFotoRaqueo1 != null)
                {
                    var logotipoBytes = fileManager.FileToByteArray(fileFotoRaqueo1);
                    var logotipoBase64 = Convert.ToBase64String(logotipoBytes);

                    var fotoRaqueo = new FotoRaqueo();
                    fotoRaqueo.ArchivoBase64 = logotipoBase64;
                    fotoRaqueo.ArchivoNombre = fileFotoRaqueo1.FileName;
                    fotoRaqueo.ArchivoType = fileFotoRaqueo1.ContentType;
                    fotoRaqueo.CreateDate = DateTime.Now;
                    fotoRaqueo.CreateUser = usuario;
            

                    fotosRaqueo.Add(fotoRaqueo);
                }

                //se valida que la segunda foto de raqueo no venga nula
                if (fileFotoRaqueo2 != null)
                {
                    var logotipoBytes = fileManager.FileToByteArray(fileFotoRaqueo2);
                    var logotipoBase64 = Convert.ToBase64String(logotipoBytes);

                    var fotoRaqueo = new FotoRaqueo();
                    fotoRaqueo.ArchivoBase64 = logotipoBase64;
                    fotoRaqueo.ArchivoNombre = fileFotoRaqueo2.FileName;
                    fotoRaqueo.ArchivoType = fileFotoRaqueo2.ContentType;
                    fotoRaqueo.CreateDate = DateTime.Now;
                    fotoRaqueo.CreateUser = usuario;

                    fotosRaqueo.Add(fotoRaqueo);
                }

                //se valida que la tercer foto de raqueo no venga nula
                if (fileFotoRaqueo3 != null)
                {
                    var logotipoBytes = fileManager.FileToByteArray(fileFotoRaqueo3);
                    var logotipoBase64 = Convert.ToBase64String(logotipoBytes);

                    var fotoRaqueo = new FotoRaqueo();
                    fotoRaqueo.ArchivoBase64 = logotipoBase64;
                    fotoRaqueo.ArchivoNombre = fileFotoRaqueo3.FileName;
                    fotoRaqueo.ArchivoType = fileFotoRaqueo3.ContentType;
                    fotoRaqueo.CreateDate = DateTime.Now;
                    fotoRaqueo.CreateUser = usuario;

                    fotosRaqueo.Add(fotoRaqueo);
                }

                if(fotosRaqueo.Count() > 0)
                {
                    fichaTecnica.FotosRaqueo = fotosRaqueo;
                }

                #endregion

                //valida que las fotos de empaque no vengan vacias
                #region FOTOS EMPAQUE
                //se crea un listado de fotos de empaque
                var fotosEmpaque = new List<FotoEmpaque>();

                //se valida que la primer foto de empaque no venga nula
                if (fileFotoEmpaque1 != null)
                {
                    var logotipoBytes = fileManager.FileToByteArray(fileFotoEmpaque1);
                    var logotipoBase64 = Convert.ToBase64String(logotipoBytes);

                    var fotoEmpaque = new FotoEmpaque();
                    fotoEmpaque.ArchivoBase64 = logotipoBase64;
                    fotoEmpaque.ArchivoNombre = fileFotoEmpaque1.FileName;
                    fotoEmpaque.ArchivoType = fileFotoEmpaque1.ContentType;
                    fotoEmpaque.CreateDate = DateTime.Now;
                    fotoEmpaque.CreateUser = usuario;

                    fotosEmpaque.Add(fotoEmpaque);
                }

                //se valida que la segunda foto de empaque no venga nula
                if (fileFotoEmpaque2 != null)
                {
                    var logotipoBytes = fileManager.FileToByteArray(fileFotoEmpaque2);
                    var logotipoBase64 = Convert.ToBase64String(logotipoBytes);

                    var fotoEmpaque = new FotoEmpaque();
                    fotoEmpaque.ArchivoBase64 = logotipoBase64;
                    fotoEmpaque.ArchivoNombre = fileFotoEmpaque2.FileName;
                    fotoEmpaque.ArchivoType = fileFotoEmpaque2.ContentType;
                    fotoEmpaque.CreateDate = DateTime.Now;
                    fotoEmpaque.CreateUser = usuario;

                    fotosEmpaque.Add(fotoEmpaque);
                }

                //se valida que la tercer foto de empaque no venga nula
                if (fileFotoEmpaque3 != null)
                {
                    var logotipoBytes = fileManager.FileToByteArray(fileFotoEmpaque3);
                    var logotipoBase64 = Convert.ToBase64String(logotipoBytes);

                    var fotoEmpaque = new FotoEmpaque();
                    fotoEmpaque.ArchivoBase64 = logotipoBase64;
                    fotoEmpaque.ArchivoNombre = fileFotoEmpaque3.FileName;
                    fotoEmpaque.ArchivoType = fileFotoEmpaque3.ContentType;
                    fotoEmpaque.CreateDate = DateTime.Now;
                    fotoEmpaque.CreateUser = usuario;

                    fotosEmpaque.Add(fotoEmpaque);
                }
            
                //se valida que hayan fotos y se agregan a la ficha tecnica
                if (fotosEmpaque.Count() > 0)
                {
                    fichaTecnica.FotosEmpaque = fotosEmpaque;
                }

                #endregion

                //valida que las fotos de salida no vengan vacias
                #region FOTOS SALIDA

                //se declara un nuevo objeto List<FotoSalida>
                var fotosSalida = new List<FotoSalida>();

                //se valida que la primer foto de salida no venga nula
                if (fileFotoSalida1 != null)
                {
                    var logotipoBytes = fileManager.FileToByteArray(fileFotoSalida1);
                    var logotipoBase64 = Convert.ToBase64String(logotipoBytes);

                    var fotoSalida = new FotoSalida();
                    fotoSalida.ArchivoBase64 = logotipoBase64;
                    fotoSalida.ArchivoNombre = fileFotoSalida1.FileName;
                    fotoSalida.ArchivoType = fileFotoSalida1.ContentType;
                    fotoSalida.CreateDate = DateTime.Now;
                    fotoSalida.CreateUser = usuario;

                    fotosSalida.Add(fotoSalida);
                }

                //se valida que la segunda foto de salida no venga nula
                if (fileFotoSalida2 != null)
                {
                    var logotipoBytes = fileManager.FileToByteArray(fileFotoSalida2);
                    var logotipoBase64 = Convert.ToBase64String(logotipoBytes);

                    var fotoSalida = new FotoSalida();
                    fotoSalida.ArchivoBase64 = logotipoBase64;
                    fotoSalida.ArchivoNombre = fileFotoSalida2.FileName;
                    fotoSalida.ArchivoType = fileFotoSalida2.ContentType;
                    fotoSalida.CreateDate = DateTime.Now;
                    fotoSalida.CreateUser = usuario;

                    fotosSalida.Add(fotoSalida);
                }

                //se valida que la tercer foto de salida no venga nula
                if (fileFotoSalida3 != null)
                {
                    var logotipoBytes = fileManager.FileToByteArray(fileFotoSalida3);
                    var logotipoBase64 = Convert.ToBase64String(logotipoBytes);

                    var fotoSalida = new FotoSalida();
                    fotoSalida.ArchivoBase64 = logotipoBase64;
                    fotoSalida.ArchivoNombre = fileFotoSalida3.FileName;
                    fotoSalida.ArchivoType = fileFotoSalida3.ContentType;
                    fotoSalida.CreateDate = DateTime.Now;
                    fotoSalida.CreateUser = usuario;

                    fotosSalida.Add(fotoSalida);
                }

                if(fotosSalida.Count() > 0)
                {
                    fichaTecnica.FotosSalida = fotosSalida;
                }

                #endregion

                //se menten los datos de auditoria
                fichaTecnica.CreateUser = usuario;
                fichaTecnica.CreateDate = DateTime.Now;

                //se crea el json y se manda al api
                var jsonObj = JsonConvert.SerializeObject(fichaTecnica);
                var result = api.Post(jsonObj, url);

                if (result)
                {
                    return RedirectToAction("Listado");
                }

                ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                return View();

            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }

        /// <summary>
        /// Metodo que sirve para consultar el registro a editar
        /// </summary>
        /// <param name="id">Recibe el id del registro</param>
        /// <returns>retorna la vista</returns>
        [SessionCheck]
        public ActionResult Editar(int id)
        {
            try
            {
                var json = api.Get(id, url + "/");
                var registro = JsonConvert.DeserializeObject<FichaTecnica>(json);

                //se cargan las fotos de recibo
                #region Fotos Recibo
                var fotosRecibo = registro.FotosRecibo;
                if (fotosRecibo.Count != 0)
                {
                    var reciboArray = fotosRecibo.ToArray();

                    ViewBag.Recibo1 = reciboArray[0].ArchivoBase64;

                    if (fotosRecibo.Count > 1)
                    {
                        ViewBag.Recibo2 = reciboArray[1].ArchivoBase64;
                    }

                    if (fotosRecibo.Count > 2)
                    {
                        ViewBag.Recibo3 = reciboArray[2].ArchivoBase64;
                    }
                }
                #endregion

                //se cargan las fotos de raqueo
                #region Fotos Raqueo
                var fotosRaqueo = registro.FotosRaqueo;
                if (fotosRaqueo.Count != 0)
                {
                    var raqueoArray = fotosRaqueo.ToArray();

                    ViewBag.Raqueo1 = raqueoArray[0].ArchivoBase64;

                    if (fotosRaqueo.Count > 1)
                    {
                        ViewBag.Raqueo2 = raqueoArray[1].ArchivoBase64;
                    }

                    if (fotosRaqueo.Count > 2)
                    {
                        ViewBag.Raqueo3 = raqueoArray[2].ArchivoBase64;
                    }
                }
                #endregion

                //se cargan las fotos de empaque
                #region Fotos Empaque
                var fotosEmpaque = registro.FotosEmpaque;
                if (fotosEmpaque.Count != 0)
                {
                    var empaqueArray = fotosEmpaque.ToArray();

                    ViewBag.Empaque1 = empaqueArray[0].ArchivoBase64;

                    if (fotosEmpaque.Count > 1)
                    {
                        ViewBag.Empaque2 = empaqueArray[1].ArchivoBase64;
                    }

                    if (fotosEmpaque.Count > 2)
                    {
                        ViewBag.Empaque3 = empaqueArray[2].ArchivoBase64;
                    }
                }
                #endregion

                //se cargan las fotos de salida
                #region Fotos Salida
                var fotosSalida = registro.FotosSalida;
                if (fotosSalida.Count != 0)
                {
                    var salidaArray = fotosSalida.ToArray();

                    ViewBag.Salida1 = salidaArray[0].ArchivoBase64;

                    if (fotosSalida.Count > 1)
                    {
                        ViewBag.Salida2 = salidaArray[1].ArchivoBase64;
                    }

                    if (fotosSalida.Count > 2)
                    {
                        ViewBag.Salida3 = salidaArray[2].ArchivoBase64;
                    }
                }
                #endregion

                return View(registro);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }

        /// <summary>
        /// Metodo que sirve para editar una ficha tecnica
        /// </summary>
        /// <param name="fichaTecnica">Objeto FichaTecnica</param>
        /// <param name="fileFotoRecibo1">Foto Recibo 1</param>
        /// <param name="fileFotoRecibo2">Foto Recibo 2</param>
        /// <param name="fileFotoRecibo3">Foto Recibo 3</param>
        /// <param name="fileFotoRaqueo1">Foto Raqueo 1</param>
        /// <param name="fileFotoRaqueo2">Foto Raqueo 2</param>
        /// <param name="fileFotoRaqueo3">Foto Raqueo 3</param>
        /// <param name="fileFotoEmpaque1">Foto Empaque 1</param>
        /// <param name="fileFotoEmpaque2">Foto Empaque 2</param>
        /// <param name="fileFotoEmpaque3">Foto Empaque 3</param>
        /// <param name="fileFotoSalida1">Foto Salida 1</param>
        /// <param name="fileFotoSalida2">Foto Salida 2</param>
        /// <param name="fileFotoSalida3">Foto Salida 3</param>
        /// <returns>Retorna la vista correspondiente al resultado</returns>
        [SessionCheck]
        [HttpPost]
        public ActionResult Editar(
            FichaTecnica fichaTecnica,
            HttpPostedFileBase fileFotoRecibo1,
            HttpPostedFileBase fileFotoRecibo2,
            HttpPostedFileBase fileFotoRecibo3,
            HttpPostedFileBase fileFotoRaqueo1,
            HttpPostedFileBase fileFotoRaqueo2,
            HttpPostedFileBase fileFotoRaqueo3,
            HttpPostedFileBase fileFotoEmpaque1,
            HttpPostedFileBase fileFotoEmpaque2,
            HttpPostedFileBase fileFotoEmpaque3,
            HttpPostedFileBase fileFotoSalida1,
            HttpPostedFileBase fileFotoSalida2,
            HttpPostedFileBase fileFotoSalida3
            )
        {
            try
            {
                var fileManager = new FileManager();
                var usuario = System.Web.HttpContext.Current.Session["Usuario.NombreUsuario"].ToString();

                var json = api.Get(fichaTecnica.Code, url + "/");
                var registro = JsonConvert.DeserializeObject<FichaTecnica>(json);


                //valida que las fotos de recibido no vengan vacias
                #region FOTOS RECIBO
                //var fotosRecibo = new List<FotoRecibo>();
                var fotosRecibo = registro.FotosRecibo;

                //se valida que la primer foto de recibo no venga nula
                if (fileFotoRecibo1 != null)
                {
                    var logotipoBytes = fileManager.FileToByteArray(fileFotoRecibo1);
                    var logotipoBase64 = Convert.ToBase64String(logotipoBytes);

                    var fotoRecibo = new FotoRecibo();
                    fotoRecibo.ArchivoBase64 = logotipoBase64;
                    fotoRecibo.ArchivoNombre = fileFotoRecibo1.FileName;
                    fotoRecibo.ArchivoType = fileFotoRecibo1.ContentType;
                    fotoRecibo.CreateUser = usuario;
                    fotoRecibo.CreateDate = DateTime.Now;

                    fotosRecibo.Add(fotoRecibo);
                }

                //se valida que la segunda foto de recibo no venga nula
                if (fileFotoRecibo2 != null)
                {
                    var logotipoBytes = fileManager.FileToByteArray(fileFotoRecibo2);
                    var logotipoBase64 = Convert.ToBase64String(logotipoBytes);

                    var fotoRecibo = new FotoRecibo();
                    fotoRecibo.ArchivoBase64 = logotipoBase64;
                    fotoRecibo.ArchivoNombre = fileFotoRecibo2.FileName;
                    fotoRecibo.ArchivoType = fileFotoRecibo2.ContentType;
                    fotoRecibo.CreateUser = usuario;
                    fotoRecibo.CreateDate = DateTime.Now;

                    fotosRecibo.Add(fotoRecibo);
                }

                //se valida que la tercer foto de recibo no venga nula
                if (fileFotoRecibo3 != null)
                {
                    var logotipoBytes = fileManager.FileToByteArray(fileFotoRecibo3);
                    var logotipoBase64 = Convert.ToBase64String(logotipoBytes);

                    var fotoRecibo = new FotoRecibo();
                    fotoRecibo.ArchivoBase64 = logotipoBase64;
                    fotoRecibo.ArchivoNombre = fileFotoRecibo3.FileName;
                    fotoRecibo.ArchivoType = fileFotoRecibo3.ContentType;
                    fotoRecibo.CreateUser = usuario;
                    fotoRecibo.CreateDate = DateTime.Now;

                    fotosRecibo.Add(fotoRecibo);
                }

                if (fotosRecibo.Count() > 0)
                {
                    fichaTecnica.FotosRecibo = fotosRecibo;
                }

                #endregion

                //valida que las fotos de raqueo no vengan vacias
                #region FOTOS RAQUEO
                //var fotosRaqueo = new List<FotoRaqueo>();
                var fotosRaqueo = registro.FotosRaqueo;

                var fotoRecibo2 = ViewBag.Recibo2;

                //se valida que la primer foto de raqueo no venga nula
                if (fileFotoRaqueo1 != null)
                {
                    var logotipoBytes = fileManager.FileToByteArray(fileFotoRaqueo1);
                    var logotipoBase64 = Convert.ToBase64String(logotipoBytes);

                    var fotoRaqueo = new FotoRaqueo();
                    fotoRaqueo.ArchivoBase64 = logotipoBase64;
                    fotoRaqueo.ArchivoNombre = fileFotoRaqueo1.FileName;
                    fotoRaqueo.ArchivoType = fileFotoRaqueo1.ContentType;
                    fotoRaqueo.CreateUser = usuario;
                    fotoRaqueo.CreateDate = DateTime.Now;

                    fotosRaqueo.Add(fotoRaqueo);
                }

                //se valida que la segunda foto de raqueo no venga nula
                if (fileFotoRaqueo2 != null)
                {
                    var logotipoBytes = fileManager.FileToByteArray(fileFotoRaqueo2);
                    var logotipoBase64 = Convert.ToBase64String(logotipoBytes);

                    var fotoRaqueo = new FotoRaqueo();
                    fotoRaqueo.ArchivoBase64 = logotipoBase64;
                    fotoRaqueo.ArchivoNombre = fileFotoRaqueo2.FileName;
                    fotoRaqueo.ArchivoType = fileFotoRaqueo2.ContentType;
                    fotoRaqueo.CreateUser = usuario;
                    fotoRaqueo.CreateDate = DateTime.Now;

                    fotosRaqueo.Add(fotoRaqueo);
                }

                //se valida que la tercer foto de raqueo no venga nula
                if (fileFotoRaqueo3 != null)
                {
                    var logotipoBytes = fileManager.FileToByteArray(fileFotoRaqueo3);
                    var logotipoBase64 = Convert.ToBase64String(logotipoBytes);

                    var fotoRaqueo = new FotoRaqueo();
                    fotoRaqueo.ArchivoBase64 = logotipoBase64;
                    fotoRaqueo.ArchivoNombre = fileFotoRaqueo3.FileName;
                    fotoRaqueo.ArchivoType = fileFotoRaqueo3.ContentType;
                    fotoRaqueo.CreateUser = usuario;
                    fotoRaqueo.CreateDate = DateTime.Now;

                    fotosRaqueo.Add(fotoRaqueo);
                }

                if (fotosRaqueo.Count() > 0)
                {
                    fichaTecnica.FotosRaqueo = fotosRaqueo;
                }

                #endregion

                //valida que las fotos de empaque no vengan vacias
                #region FOTOS EMPAQUE
                //se crea un listado de fotos de empaque
                //var fotosEmpaque = new List<FotoEmpaque>();
                var fotosEmpaque = registro.FotosEmpaque;

                //se valida que la primer foto de empaque no venga nula
                if (fileFotoEmpaque1 != null)
                {
                    var logotipoBytes = fileManager.FileToByteArray(fileFotoEmpaque1);
                    var logotipoBase64 = Convert.ToBase64String(logotipoBytes);

                    var fotoEmpaque = new FotoEmpaque();
                    fotoEmpaque.ArchivoBase64 = logotipoBase64;
                    fotoEmpaque.ArchivoNombre = fileFotoEmpaque1.FileName;
                    fotoEmpaque.ArchivoType = fileFotoEmpaque1.ContentType;
                    fotoEmpaque.CreateUser = usuario;
                    fotoEmpaque.CreateDate = DateTime.Now;

                    fotosEmpaque.Add(fotoEmpaque);
                }

                //se valida que la segunda foto de empaque no venga nula
                if (fileFotoEmpaque2 != null)
                {
                    var logotipoBytes = fileManager.FileToByteArray(fileFotoEmpaque2);
                    var logotipoBase64 = Convert.ToBase64String(logotipoBytes);

                    var fotoEmpaque = new FotoEmpaque();
                    fotoEmpaque.ArchivoBase64 = logotipoBase64;
                    fotoEmpaque.ArchivoNombre = fileFotoEmpaque2.FileName;
                    fotoEmpaque.ArchivoType = fileFotoEmpaque2.ContentType;
                    fotoEmpaque.CreateUser = usuario;
                    fotoEmpaque.CreateDate = DateTime.Now;

                    fotosEmpaque.Add(fotoEmpaque);
                }

                //se valida que la tercer foto de empaque no venga nula
                if (fileFotoEmpaque3 != null)
                {
                    var logotipoBytes = fileManager.FileToByteArray(fileFotoEmpaque3);
                    var logotipoBase64 = Convert.ToBase64String(logotipoBytes);

                    var fotoEmpaque = new FotoEmpaque();
                    fotoEmpaque.ArchivoBase64 = logotipoBase64;
                    fotoEmpaque.ArchivoNombre = fileFotoEmpaque3.FileName;
                    fotoEmpaque.ArchivoType = fileFotoEmpaque3.ContentType;
                    fotoEmpaque.CreateUser = usuario;
                    fotoEmpaque.CreateDate = DateTime.Now;

                    fotosEmpaque.Add(fotoEmpaque);
                }

                //se valida que hayan fotos y se agregan a la ficha tecnica
                if (fotosEmpaque.Count() > 0)
                {
                    fichaTecnica.FotosEmpaque = fotosEmpaque;
                }

                #endregion

                //valida que las fotos de salida no vengan vacias
                #region FOTOS SALIDA

                //se declara un nuevo objeto List<FotoSalida>
                //var fotosSalida = new List<FotoSalida>();
                var fotosSalida = registro.FotosSalida;

                //se valida que la primer foto de salida no venga nula
                if (fileFotoSalida1 != null)
                {
                    var logotipoBytes = fileManager.FileToByteArray(fileFotoSalida1);
                    var logotipoBase64 = Convert.ToBase64String(logotipoBytes);

                    var fotoSalida = new FotoSalida();
                    fotoSalida.ArchivoBase64 = logotipoBase64;
                    fotoSalida.ArchivoNombre = fileFotoSalida1.FileName;
                    fotoSalida.ArchivoType = fileFotoSalida1.ContentType;
                    fotoSalida.CreateUser = usuario;
                    fotoSalida.CreateDate = DateTime.Now;

                    fotosSalida.Add(fotoSalida);
                }

                //se valida que la segunda foto de salida no venga nula
                if (fileFotoSalida2 != null)
                {
                    var logotipoBytes = fileManager.FileToByteArray(fileFotoSalida2);
                    var logotipoBase64 = Convert.ToBase64String(logotipoBytes);

                    var fotoSalida = new FotoSalida();
                    fotoSalida.ArchivoBase64 = logotipoBase64;
                    fotoSalida.ArchivoNombre = fileFotoSalida2.FileName;
                    fotoSalida.ArchivoType = fileFotoSalida2.ContentType;
                    fotoSalida.CreateUser = usuario;
                    fotoSalida.CreateDate = DateTime.Now;

                    fotosSalida.Add(fotoSalida);
                }

                //se valida que la tercer foto de salida no venga nula
                if (fileFotoSalida3 != null)
                {
                    var logotipoBytes = fileManager.FileToByteArray(fileFotoSalida3);
                    var logotipoBase64 = Convert.ToBase64String(logotipoBytes);

                    var fotoSalida = new FotoSalida();
                    fotoSalida.ArchivoBase64 = logotipoBase64;
                    fotoSalida.ArchivoNombre = fileFotoSalida3.FileName;
                    fotoSalida.ArchivoType = fileFotoSalida3.ContentType;
                    fotoSalida.CreateUser = usuario;
                    fotoSalida.CreateDate = DateTime.Now;

                    fotosSalida.Add(fotoSalida);
                }

                if (fotosSalida.Count() > 0)
                {
                    fichaTecnica.FotosSalida = fotosSalida;
                }

                #endregion

                //obtiene los datos del usuario actual y la fecha en que se actualiza
                fichaTecnica.UpdateUser = usuario;
                fichaTecnica.UpdateDate = DateTime.Now;


                var jsonObj = JsonConvert.SerializeObject(fichaTecnica);
                var result = api.Put(jsonObj, url + "/");

                if (result)
                {
                    return RedirectToAction("Listado");
                }

                ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                return View();
            }
            catch (Exception)
            {
                return View();
            }
        }

        /// <summary>
        /// Metodo que sirve para consultar el registro a borrar
        /// </summary>
        /// <param name="id">Recibe el id</param>
        /// <returns>Retorna la vista</returns>
        [SessionCheck]
        public ActionResult Borrar(int? id)
        {
            try
            {
                var json = api.Get(id, url + "/");
                var registro = JsonConvert.DeserializeObject<FichaTecnica>(json);
                return View(registro);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }

        /// <summary>
        /// Metodo que sirve para borrar un registro
        /// </summary>
        /// <param name="id">Recibe el id</param>
        /// <returns>Retorna una vista</returns>
        [SessionCheck]
        [HttpPost]
        public ActionResult Borrar(int id)
        {
            try
            {
                var result = api.Delete(id, url + "/");

                if (result)
                {
                    return RedirectToAction("Listado");
                }

                ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                return View();
            }
            catch (Exception)
            {
                return View();
            }
        }

        /// <summary>
        /// Metodo para cargar los listados
        /// </summary>
        public void CargarListados()
        {
            //listado de clientes
            var jsonClientes = api.Get(ApiUrl + "Clientes");
            var clientesListado = JsonConvert.DeserializeObject<List<Empresa>>(jsonClientes);
            ViewBag.Clientes = clientesListado.Select(x => new SelectListItem { Value = x.Code + "", Text = x.Nombre });

            //listados de tipos de tratamientos
            var jsonTiposTratamiento = api.Get(ApiUrl + "TiposTratamiento");
            var tiposTratamientoListado = JsonConvert.DeserializeObject<List<TipoTratamiento>>(jsonTiposTratamiento);
            ViewBag.TiposTratamiento = tiposTratamientoListado.Select(x => new SelectListItem { Value = x.Code + "", Text = x.Nombre });

            //listado de tipos de materiales
            var jsonTiposMaterial = api.Get(ApiUrl + "TiposMaterial");
            var tiposMaterialesListado = JsonConvert.DeserializeObject<List<TipoMaterial>>(jsonTiposMaterial);
            ViewBag.TiposMaterial = tiposMaterialesListado.Select(x => new SelectListItem { Value = x.Code + "", Text = x.Nombre });
        }

        /// <summary>
        /// Metodo que sirve para obtener el listado de las fotos de recibo
        /// </summary>
        /// <param name="fichaCode">Recibe el codigo del cliente</param>
        /// <returns>Retorna el json</returns>
        public string FotosRecibo(int fichaCode)
        {
            var ulrListadoFiltrado = ConfigurationManager.AppSettings["ApiUrl"].ToString() + "FotosRecibo/FichaTecnica/" + fichaCode;
            var listadoJson = api.Get(ulrListadoFiltrado);

            return listadoJson;
        }

        /// <summary>
        /// Metodo que sirve para obtener el listado de las fotos de raqueo
        /// </summary>
        /// <param name="fichaCode">Recibe el codigo del cliente</param>
        /// <returns>Retorna el json</returns>
        public string FotosRaqueo(int fichaCode)
        {
            var ulrListadoFiltrado = ApiUrl + "FotosRaqueo/FichaTecnica/" + fichaCode;
            var listadoJson = api.Get(ulrListadoFiltrado);

            return listadoJson;
        }

        /// <summary>
        /// Metodo que sirve para obtener el listado de las fotos de empaque
        /// </summary>
        /// <param name="fichaCode">Recibe el codigo del cliente</param>
        /// <returns>Retorna el json</returns>
        public string FotosEmpaque(int fichaCode)
        {
            var ulrListadoFiltrado = ApiUrl + "FotosEmpaque/FichaTecnica/" + fichaCode;
            var listadoJson = api.Get(ulrListadoFiltrado);

            return listadoJson;
        }

        /// <summary>
        /// Metodo que sirve para obtener el listado de las fotos de salida
        /// </summary>
        /// <param name="fichaCode">Recibe el codigo del cliente</param>
        /// <returns>Retorna el json</returns>
        public string FotosSalida(int fichaCode)
        {
            var ulrListadoFiltrado = ApiUrl + "FotosSalida/FichaTecnica/" + fichaCode;
            var listadoJson = api.Get(ulrListadoFiltrado);

            return listadoJson;
        }

    }
}