﻿using Business;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebSite.App_Start;
using WebSite.Models;

namespace WebSite.Controllers
{
    /// <summary>
    /// Nombre de la clase: ProductosController
    /// </summary>
    public class ProductosController : Controller
    {
        /// <summary>
        /// Se declara un objeto Api
        /// </summary>
        private Api api;

        private string hojasSeguridadNames;

        private string permisoVentaName;

        private string permisoConsumoName;

        /// <summary>
        /// Url del API
        /// </summary>
        private string ApiUrl;

        /// <summary>
        /// Url con el controlador
        /// </summary>
        private string url;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public ProductosController()
        {
            api = new Api();
            ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
            url = ApiUrl + "Productos";
            CargarListados();
        }

        /// <summary>
        /// Consulta el listado
        /// </summary>
        /// <returns>Retorna una vista con el listado</returns>
        [SessionCheck]
        public ActionResult Listado()
        {
            try
            {
                var json = api.Get(url);
                var listado = JsonConvert.DeserializeObject<List<Productos>>(json);

                return View(listado);
            }
            catch(Exception ex)
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }

        public string ListadoJSON(short ControladorID)
        {
            try
            {
                var json = api.Get(url + "/Listado?ControladorID=" + ControladorID);
                return json;
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return "Error";
            }
        }

        /// <summary>
        /// Consulta un registro mediante su id
        /// </summary>
        /// <param name="id">Recibe el id</param>
        /// <returns>Retorna la vista de detalle</returns>
        [SessionCheck]
        public ActionResult Detalles(int id)
        {
            try
            {
                var json = api.Get(id, url + "/");
                var registro = JsonConvert.DeserializeObject<Productos>(json);

                return View(registro);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }

        /// <summary>
        /// Retorna una vista para crear un nuevo registro
        /// </summary>
        /// <returns>Retorna la vista</returns>
        [SessionCheck]
        public ActionResult Crear()
        {
            return View(new Productos());
        }


        /// <summary>
        /// Sirve para crear un nuevo registro
        /// </summary>
        /// <param name="producto">Recibe el objeto</param>
        /// <returns>Retorna la vista/returns>
        [SessionCheck]
        [HttpPost]
        public ActionResult Crear(Productos producto, UploadedFiles files)
        {
            hojasSeguridadNames = "";
            permisoVentaName = "";
            permisoConsumoName = "";
            var listHojasSeguridad = files.HojasSeguridad.ToList();
            if (listHojasSeguridad[0] != null)
            {
                foreach (var file in files.HojasSeguridad)
                {
                    if (file.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(file.FileName);
                        var filePath = Path.Combine(Server.MapPath("~/HojasSeguridad"), fileName);
                        hojasSeguridadNames += $"{ fileName},";
                        file.SaveAs(filePath);
                    }
                }
                hojasSeguridadNames = hojasSeguridadNames.Remove(hojasSeguridadNames.Length - 1);
            }
            if (files.PermisoVenta != null)
            {
                var fileNamePermisoV = Path.GetFileName(files.PermisoVenta.FileName);
                var filePathPermisoV = Path.Combine(Server.MapPath("~/PermisosVentas"), fileNamePermisoV);
                permisoVentaName = fileNamePermisoV;
                files.PermisoVenta.SaveAs(filePathPermisoV);
            }
            if (files.PermisoConsumo != null)
            {
                var fileNamePermisoC = Path.GetFileName(files.PermisoConsumo.FileName);
                var filePathPermisoC = Path.Combine(Server.MapPath("~/PermisosConsumos"), fileNamePermisoC);
                permisoConsumoName = fileNamePermisoC;
                files.PermisoConsumo.SaveAs(filePathPermisoC);
            }

            try
            {
                producto.HojaSeguridad = hojasSeguridadNames;
                producto.PermisoVenta = permisoVentaName;
                producto.PermisoConsumo = permisoConsumoName;
                var jsonObj = JsonConvert.SerializeObject(producto);
                var result = api.Post(jsonObj, url);

                if (result)
                {
                    return RedirectToAction("Listado");
                }

                ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                return View();

            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }

        [SessionCheck]
        public ActionResult DownloadHojaSeguridad(string HojaSeguridad)
        {
            if (HojaSeguridad.Contains(','))
            {
                string[] paths = new string[99];
                string[] fileName = new string[99];
                int i = 0;
                var fileNames = HojaSeguridad.Split(',');
                foreach(string file in fileNames)
                { 
                    paths[i] = Server.MapPath($"~/HojasSeguridad/{file}");
                    fileName[i] = file;
                    i++;
                }
                using (var memoryStream = new MemoryStream())
                {
                    using (var ziparchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                    {
                        for (int j = 0; j < fileNames.Length; j++)
                        {
                            ziparchive.CreateEntryFromFile(paths[j], fileName[j]);

                        }
                    }
                    return File(memoryStream.ToArray(), "application/zip", "HojasSeguridad.zip");
                }

            }
            else
            {
                string path = Server.MapPath("~/HojasSeguridad");
                byte[] fileBytes = System.IO.File.ReadAllBytes($"{path}/{HojaSeguridad}");
                string fileName = HojaSeguridad;
                return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
            }
        }

        [SessionCheck]
        public ActionResult DownloadPermisoVenta(string PermisoVenta)
        {
            string path = Server.MapPath("~/PermisosVentas");
            byte[] fileBytes = System.IO.File.ReadAllBytes($"{path}/{PermisoVenta}");
            string fileName = PermisoVenta;
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        [SessionCheck]
        public ActionResult DownloadPermisoConsumo(string PermisoConsumo)
        {
            string path = Server.MapPath("~/PermisosConsumos");
            byte[] fileBytes = System.IO.File.ReadAllBytes($"{path}/{PermisoConsumo}");
            string fileName = PermisoConsumo;
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        /// <summary>
        /// Metodo que sirve para consultar el registro a editar
        /// </summary>
        /// <param name="id">Recibe el id del registro</param>
        /// <returns>retorna la vista</returns>
        [SessionCheck]
        public ActionResult Editar(int id)
        {
            try
            {
                var json = api.Get(id, url + "/");
                var registro = JsonConvert.DeserializeObject<Productos>(json);
                return View(registro);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }

        /// <summary>
        /// Metodo que sirve para editar el registro
        /// </summary>
        /// <param name="producto">Recibe el objeto</param>
        /// <returns>Retorna una vista</returns>
        [SessionCheck]
        [HttpPost]
        public ActionResult Editar(Productos producto, UploadedFiles files)
        {

            hojasSeguridadNames = "";
            permisoVentaName = "";
            permisoConsumoName = "";
            var listHojasSeguridad = files.HojasSeguridad.ToList();
            if (listHojasSeguridad[0] != null)
            {
                foreach (var file in files.HojasSeguridad)
                {
                    if (file.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(file.FileName);
                        var filePath = Path.Combine(Server.MapPath("~/HojasSeguridad"), fileName);
                        hojasSeguridadNames += $"{ fileName},";
                        file.SaveAs(filePath);
                    }
                }
                hojasSeguridadNames = hojasSeguridadNames.Remove(hojasSeguridadNames.Length - 1);
            }
            if (files.PermisoVenta != null)
            {
                var fileNamePermisoV = Path.GetFileName(files.PermisoVenta.FileName);
                var filePathPermisoV = Path.Combine(Server.MapPath("~/PermisosVentas"), fileNamePermisoV);
                permisoVentaName = fileNamePermisoV;
                files.PermisoVenta.SaveAs(filePathPermisoV);
            }
            if(files.PermisoConsumo != null)
            {
                var fileNamePermisoC = Path.GetFileName(files.PermisoConsumo.FileName);
                var filePathPermisoC = Path.Combine(Server.MapPath("~/PermisosConsumos"), fileNamePermisoC);
                permisoConsumoName = fileNamePermisoC;
                files.PermisoConsumo.SaveAs(filePathPermisoC);
            }
            

            try
            {
                if (hojasSeguridadNames != "" && producto.HojaSeguridad != null)
                {
                    producto.HojaSeguridad = hojasSeguridadNames;
                }
                else if(hojasSeguridadNames == "" && producto.HojaSeguridad == null)
                {
                    producto.HojaSeguridad = hojasSeguridadNames;
                }
                else if(hojasSeguridadNames != "" && producto.HojaSeguridad == null)
                {
                    producto.HojaSeguridad = hojasSeguridadNames;
                }
                if(permisoVentaName != "" && producto.PermisoVenta != null)
                {
                    producto.PermisoVenta = permisoVentaName;
                }
                else if(permisoVentaName == "" && producto.PermisoVenta == null)
                {
                    producto.PermisoVenta = permisoVentaName;
                }
                else if(permisoVentaName != "" && producto.PermisoVenta == null)
                {
                    producto.PermisoVenta = permisoVentaName;
                }
                if (permisoConsumoName != "" && producto.PermisoConsumo != null)
                {
                    producto.PermisoConsumo = permisoConsumoName;
                }
                else if(permisoConsumoName == "" && producto.PermisoConsumo == null)
                {
                    producto.PermisoConsumo = permisoConsumoName;
                }
                else if(permisoConsumoName != "" && producto.PermisoConsumo == null)
                {
                    producto.PermisoConsumo = permisoConsumoName;
                }
                var jsonObj = JsonConvert.SerializeObject(producto);
                var result = api.Put(jsonObj, url + "/");

                if (result)
                {
                    return RedirectToAction("Listado");
                }

                ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                return View();
            }
            catch (Exception)
            {
                return View();
            }
        }

        /// <summary>
        /// Metodo que sirve para consultar el registro a borrar
        /// </summary>
        /// <param name="id">Recibe el id</param>
        /// <returns>Retorna la vista</returns>
        [SessionCheck]
        public ActionResult Borrar(int? id)
        {
            try
            {
                var json = api.Get(id, url + "/");
                var registro = JsonConvert.DeserializeObject<Productos>(json);
                return View(registro);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }

        /// <summary>
        /// Metodo que sirve para borrar un registro
        /// </summary>
        /// <param name="id">Recibe el id</param>
        /// <returns>Retorna una vista</returns>
        [SessionCheck]
        [HttpPost]
        public ActionResult Borrar(int id)
        {
            try
            {
                var result = api.Delete(id, url + "/");

                if (result)
                {
                    return RedirectToAction("Listado");
                }

                ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                return View();
            }
            catch (Exception)
            {
                return View();
            }
        }


        public void CargarListados()
        {
            var jsonControladores = api.Get(ApiUrl + "Controladores");
            var listadoControladores = JsonConvert.DeserializeObject<List<Controlador>>(jsonControladores);
            ViewBag.Controladores = listadoControladores.Select(x => new SelectListItem { Value = x.Codigo + "", Text = x.Nombre });
        }
    }
}