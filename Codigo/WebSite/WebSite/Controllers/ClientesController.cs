﻿using Business;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebSite.App_Start;
using WebSite.Models;

namespace WebSite.Controllers
{
    /// <summary>
    /// Nombre de la clase: ClientesController
    /// </summary>
    public class ClientesController : Controller
    {
        /// <summary>
        /// Se declara un objeto Api
        /// </summary>
        private Api api;

        /// <summary>
        /// Url del API
        /// </summary>
        private string ApiUrl;

        /// <summary>
        /// Url con el controlador
        /// </summary>
        private string url;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public ClientesController()
        {
            api = new Api();
            ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
            url = ApiUrl + "Clientes";
            CargarListados();
        }

        /// <summary>
        /// Consulta el listado
        /// </summary>
        /// <returns>Retorna una vista con el listado</returns>
        [SessionCheck]
        public ActionResult Listado()
        {
            try
            {
                var json = api.Get(url);
                var listado = JsonConvert.DeserializeObject<List<Cliente>>(json);

                return View(listado);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }


        /// <summary>
        /// Consulta un registro mediante su id
        /// </summary>
        /// <param name="id">Recibe el id</param>
        /// <returns>Retorna la vista de detalle</returns>
        [SessionCheck]
        public ActionResult Detalles(int id)
        {
            try
            {
                var json = api.Get(id, url + "/");
                var registro = JsonConvert.DeserializeObject<Cliente>(json);

                return View(registro);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }

        /// <summary>
        /// Retorna una vista para crear un nuevo registro
        /// </summary>
        /// <returns>Retorna la vista</returns>
        [SessionCheck]
        public ActionResult Crear()
        {
            return View(new Cliente());
        }

        /// <summary>
        /// Sirve para crear un nuevo registro
        /// </summary>
        /// <param name="cliente">Recibe el objeto</param>
        /// <returns>Retorna la vista/returns>
        [SessionCheck]
        [HttpPost]
        public ActionResult Crear(Cliente cliente)
        {
            try
            {
                var usuario = System.Web.HttpContext.Current.Session["Usuario.NombreUsuario"].ToString();
                cliente.CreateUser = usuario;
                cliente.CreateDate = DateTime.Now;

                var jsonObj = JsonConvert.SerializeObject(cliente);
                var result = api.Post(jsonObj, url);

                if (result)
                {
                    return RedirectToAction("Listado");
                }

                ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                return View();

            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }

        /// <summary>
        /// Metodo que sirve para consultar el registro a editar
        /// </summary>
        /// <param name="id">Recibe el id del registro</param>
        /// <returns>retorna la vista</returns>
        [SessionCheck]
        public ActionResult Editar(int id)
        {
            try
            {
                var json = api.Get(id, url + "/");
                var registro = JsonConvert.DeserializeObject<Cliente>(json);

                if (registro.PaisCode == 28)
                {
                    var jsonProvincias = api.Get(ApiUrl + "Provincias");
                    var provinciasListado = JsonConvert.DeserializeObject<List<Provincia>>(jsonProvincias);
                    ViewBag.Provincias = provinciasListado.Select(x => new SelectListItem { Value = x.Code + "", Text = x.Nombre });

                    var jsonCantones = api.Get(ApiUrl + "Cantones");
                    var cantonesListado = JsonConvert.DeserializeObject<List<Canton>>(jsonCantones).Where(p => p.ProvinciaCode == registro.ProvinciaCode);
                    var cantonesFiltrados = cantonesListado.Select(x => new SelectListItem { Value = x.Code + "", Text = x.Nombre });
                    ViewBag.Cantones = cantonesFiltrados;

                    var jsonDistritos = api.Get(ApiUrl + "Distritos");
                    var distritosListado = JsonConvert.DeserializeObject<List<Distrito>>(jsonDistritos).Where(p => p.ProvinciaCode == registro.ProvinciaCode && p.CantonCode == registro.CantonCode);
                    var distritosFiltrados = distritosListado.Select(x => new SelectListItem { Value = x.DistritoCode + "", Text = x.Nombre });
                    ViewBag.Distritos = distritosFiltrados;
                }

                return View(registro);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }

        /// <summary>
        /// Metodo que sirve para editar el registro
        /// </summary>
        /// <param name="cliente">Recibe el objeto</param>
        /// <returns>Retorna una vista</returns>
        [SessionCheck]
        [HttpPost]
        public ActionResult Editar(Cliente cliente)
        {
            try
            {
                cliente.UpdateDate = DateTime.Now;
                cliente.UpdateUser = System.Web.HttpContext.Current.Session["Usuario.NombreUsuario"].ToString();

                var jsonObj = JsonConvert.SerializeObject(cliente);
                var result = api.Put(jsonObj, url + "/");

                if (result)
                {
                    return RedirectToAction("Listado");
                }

                ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                return View();
            }
            catch (Exception)
            {
                return View();
            }
        }

        /// <summary>
        /// Metodo que sirve para consultar el registro a borrar
        /// </summary>
        /// <param name="id">Recibe el id</param>
        /// <returns>Retorna la vista</returns>
        [SessionCheck]
        public ActionResult Borrar(int? id)
        {
            try
            {
                var json = api.Get(id, url + "/");
                var registro = JsonConvert.DeserializeObject<Cliente>(json);
                return View(registro);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }

        /// <summary>
        /// Metodo que sirve para borrar un registro
        /// </summary>
        /// <param name="id">Recibe el id</param>
        /// <returns>Retorna una vista</returns>
        [SessionCheck]
        [HttpPost]
        public ActionResult Borrar(int id)
        {
            try
            {
                var result = api.Delete(id, url + "/");

                if (result)
                {
                    return RedirectToAction("Listado");
                }

                ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                return View();
            }
            catch (Exception)
            {
                return View();
            }
        }

        /// <summary>
        /// Método que sirve para cargar las provincias
        /// </summary>
        /// <param name="parentVal">Recibe el code del pais</param>
        /// <returns>Retorna el listado de provincias</returns>
        [SessionCheck]
        [HttpPost]
        public JsonResult CargarProvincias(int parentVal)
        {
            if (parentVal == 28)
            {
                var jsonProvincias = api.Get(ApiUrl + "Provincias");
                var provinciasListado = JsonConvert.DeserializeObject<List<Provincia>>(jsonProvincias);
                var provincias = provinciasListado.Select(x => new SelectListItem { Value = x.Code + "", Text = x.Nombre });

                return Json(provincias, JsonRequestBehavior.AllowGet);
            }
            var GenericSelectList = new List<SelectListItem>();
            var item = new SelectListItem() { Text = "NINGUNO", Value = "0" };
            GenericSelectList.Add(item);

            return Json(GenericSelectList, JsonRequestBehavior.AllowGet);
          
        }

        /// <summary>
        /// Método que sirve para cargar los cantones
        /// </summary>
        /// <param name="parentVal">Recibe el code de la provincia</param>
        /// <returns>Retorna un listado</returns>
        [SessionCheck]
        [HttpPost]
        public JsonResult CargarCantones(int parentVal)
        {
            var jsonCantones = api.Get(ApiUrl + "Cantones");
            var cantonesListado = JsonConvert.DeserializeObject<List<Canton>>(jsonCantones).Where(p => p.ProvinciaCode == parentVal);
            var cantonesFiltrados = cantonesListado.Select(x => new SelectListItem { Value = x.Code + "", Text = x.Nombre });

            return Json(cantonesFiltrados, JsonRequestBehavior.AllowGet);  
        }

        /// <summary>
        /// Método que sirve para cargar los distritos
        /// </summary>
        /// <param name="parentVal">Recibe el code del canton</param>
        /// <returns>Retorna un listado</returns>
        [SessionCheck]
        [HttpPost]
        public JsonResult CargarDistritos(int parentVal1, int parentVal2)
        {

            var jsonDistritos = api.Get(ApiUrl + "Distritos");
            var distritosListado = JsonConvert.DeserializeObject<List<Distrito>>(jsonDistritos).Where(p => p.ProvinciaCode == parentVal1 && p.CantonCode == parentVal2);
            var distritosFiltrados = distritosListado.Select(x => new SelectListItem { Value = x.DistritoCode + "", Text = x.Nombre });

            return Json(distritosFiltrados, JsonRequestBehavior.AllowGet);  
        }

        public void CargarListados()
        {
            var json = api.Get(ApiUrl + "Empresas");
            var empresaslistado = JsonConvert.DeserializeObject<List<Empresa>>(json);
            ViewBag.Empresas = empresaslistado.Select(x => new SelectListItem { Value = x.Code + "", Text = x.Nombre });

            var jsonPaises = api.Get(ApiUrl + "Paises");
            var paisesListado = JsonConvert.DeserializeObject<List<Pais>>(jsonPaises);
            ViewBag.Paises = paisesListado.Select(x => new SelectListItem { Value = x.Code + "", Text = x.Nombre });

            //var jsonProvincias = api.Get(ApiUrl + "Provincias");
            //var provinciasListado = JsonConvert.DeserializeObject<List<Provincia>>(jsonProvincias);
            //ViewBag.Provincias = provinciasListado.Select(x => new SelectListItem { Value = x.Code + "", Text = x.Nombre });
            ViewBag.Provincias = new List<SelectListItem>();

            //var jsonProvincias = api.Get(ApiUrl + "Provincias");
            //var provinciasListado = JsonConvert.DeserializeObject<List<Provincia>>(jsonProvincias);
            ViewBag.Provincias = new List<SelectListItem>();

            //var jsonCantones = api.Get(ApiUrl + "Cantones");
            //var cantonesListado = JsonConvert.DeserializeObject<List<Canton>>(jsonCantones);
            //ViewBag.Cantones = cantonesListado.Select(x => new SelectListItem { Value = x.Code + "", Text = x.Nombre });
            ViewBag.Cantones = new List<SelectListItem>();

            //var jsonDistritos = api.Get(ApiUrl + "Distritos");
            //var distritosListado = JsonConvert.DeserializeObject<List<Distrito>>(jsonDistritos);
            //ViewBag.Distritos = distritosListado.Select(x => new SelectListItem { Value = x.DistritoCode + "", Text = x.Nombre });
            ViewBag.Distritos = new List<SelectListItem>();
        }
    }
}