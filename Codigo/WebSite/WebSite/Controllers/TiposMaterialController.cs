﻿using Business;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebSite.App_Start;
using WebSite.Models;

namespace WebSite.Controllers
{
    /// <summary>
    /// Nombre de la clase: TiposMaterialController
    /// </summary>
    public class TiposMaterialController : Controller
    {
        /// <summary>
        /// Se declara un objeto Api
        /// </summary>
        private Api api;

        /// <summary>
        /// Url del API
        /// </summary>
        private string ApiUrl;

        /// <summary>
        /// Url con el controlador
        /// </summary>
        private string url;

        /// <summary>
        /// Constructor
        /// </summary>
        public TiposMaterialController()
        {
            api = new Api();
            ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
            url = ApiUrl + "TiposMaterial";
        }

        /// <summary>
        /// Consulta el listado
        /// </summary>
        /// <returns>Retorna una vista con el listado</returns>
        [SessionCheck]
        public ActionResult Listado()
        {
            try
            {
                var json = api.Get(url);
                var listado = JsonConvert.DeserializeObject<List<TipoMaterial>>(json);

                return View(listado);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }


        /// <summary>
        /// Consulta un registro mediante su id
        /// </summary>
        /// <param name="id">Recibe el id</param>
        /// <returns>Retorna la vista de detalle</returns>
        [SessionCheck]
        public ActionResult Detalles(int id)
        {
            try
            {
                var json = api.Get(id, url + "/");
                var registro = JsonConvert.DeserializeObject<TipoMaterial>(json);

                return View(registro);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }

        /// <summary>
        /// Retorna una vista para crear un nuevo registro
        /// </summary>
        /// <returns>Retorna la vista</returns>
        [SessionCheck]
        public ActionResult Crear()
        {
            return View(new TipoMaterial());
        }

        /// <summary>
        /// Sirve para crear un nuevo registro
        /// </summary>
        /// <param name="tipoMaterial">Recibe el objeto</param>
        /// <returns>Retorna la vista/returns>
        [SessionCheck]
        [HttpPost]
        public ActionResult Crear(TipoMaterial tipoMaterial)
        {
            try
            {
                var usuario = System.Web.HttpContext.Current.Session["Usuario.NombreUsuario"].ToString();
                tipoMaterial.CreateUser = usuario;
                tipoMaterial.CreateDate = DateTime.Now;

                var jsonObj = JsonConvert.SerializeObject(tipoMaterial);
                var result = api.Post(jsonObj, url);

                if (result)
                {
                    return RedirectToAction("Listado");
                }

                ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                return View();

            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }

        /// <summary>
        /// Metodo que sirve para consultar el registro a editar
        /// </summary>
        /// <param name="id">Recibe el id del registro</param>
        /// <returns>retorna la vista</returns>
        [SessionCheck]
        public ActionResult Editar(int id)
        {
            try
            {
                var json = api.Get(id, url + "/");
                var registro = JsonConvert.DeserializeObject<TipoMaterial>(json);
                return View(registro);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }

        /// <summary>
        /// Metodo que sirve para editar el registro
        /// </summary>
        /// <param name="tipoMaterial">Recibe el objeto</param>
        /// <returns>Retorna una vista</returns>
        [SessionCheck]
        [HttpPost]
        public ActionResult Editar(TipoMaterial tipoMaterial)
        {
            try
            {
                var usuario = System.Web.HttpContext.Current.Session["Usuario.NombreUsuario"].ToString();
                tipoMaterial.UpdateUser = usuario;
                tipoMaterial.UpdateDate = DateTime.Now;

                var jsonObj = JsonConvert.SerializeObject(tipoMaterial);
                var result = api.Put(jsonObj, url + "/");

                if (result)
                {
                    return RedirectToAction("Listado");
                }

                ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                return View();
            }
            catch (Exception)
            {
                return View();
            }
        }

        /// <summary>
        /// Metodo que sirve para consultar el registro a borrar
        /// </summary>
        /// <param name="id">Recibe el id</param>
        /// <returns>Retorna la vista</returns>
        [SessionCheck]
        public ActionResult Borrar(int? id)
        {
            try
            {
                var json = api.Get(id, url + "/");
                var registro = JsonConvert.DeserializeObject<TipoMaterial>(json);
                return View(registro);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }

        /// <summary>
        /// Metodo que sirve para borrar un registro
        /// </summary>
        /// <param name="id">Recibe el id</param>
        /// <returns>Retorna una vista</returns>
        [SessionCheck]
        [HttpPost]
        public ActionResult Borrar(int id)
        {
            try
            {
                var result = api.Delete(id, url + "/");

                if (result)
                {
                    return RedirectToAction("Listado");
                }

                ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                return View();
            }
            catch (Exception)
            {
                return View();
            }
        }
    }
}