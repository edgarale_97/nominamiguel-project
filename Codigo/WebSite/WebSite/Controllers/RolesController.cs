﻿using Business;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebSite.App_Start;
using WebSite.Models;

namespace WebSite.Controllers
{
    /// <summary>
    /// Nombre de la clase: RolesController
    /// </summary>
    public class RolesController : Controller
    {
        /// <summary>
        /// Se declara un objeto Api
        /// </summary>
        private Api api;

        /// <summary>
        /// Url del API
        /// </summary>
        private string ApiUrl;

        /// <summary>
        /// Url con el controlador
        /// </summary>
        private string url;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public RolesController()
        {
            api = new Api();
            ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
            url = ApiUrl + "Roles";
        }

        /// <summary>
        /// Metodo que isrve para generar el listado
        /// </summary>
        /// <returns>retorna el listado</returns>
        [SessionCheck]
        public ActionResult Listado()
        {
            try
            {
                var json = api.Get(url);
                var listado = JsonConvert.DeserializeObject<List<Rol>>(json);

                return View(listado);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }

        [SessionCheck]
        public ActionResult Detalles(int? id)
        {
            try
            {
                var json = api.Get(id, url + "/");
                var rol = JsonConvert.DeserializeObject<Rol>(json);

                return View(rol);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }

        [SessionCheck]
        public ActionResult Crear()
        {
            return View(new Rol());
        }

        [SessionCheck]
        [HttpPost]
        public ActionResult Crear(Rol rol)
        {
            try
            {
                var jsonObj = JsonConvert.SerializeObject(rol);
                var result = api.Post(jsonObj, url);

                if (result)
                {
                    return RedirectToAction("Listado");
                }

                ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                return View();

            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }

        [SessionCheck]
        public ActionResult Editar(int? id)
        {
            try
            {
                var json = api.Get(id, url + "/");
                var rol = JsonConvert.DeserializeObject<Rol>(json);
                return View(rol);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }

        }

        [SessionCheck]
        [HttpPost]
        public ActionResult Editar(Rol rol)
        {
            try
            {
                var jsonObj = JsonConvert.SerializeObject(rol);
                var result = api.Put(jsonObj, url + "/");

                if (result)
                {
                    return RedirectToAction("Listado");
                }

                ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                return View();
            }
            catch (Exception)
            {
                return View();
            }
        }

        [SessionCheck]
        public ActionResult Borrar(int? id)
        {
            try
            {
                var json = api.Get(id, url + "/");
                var rol = JsonConvert.DeserializeObject<Rol>(json);
                return View(rol);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }

        [SessionCheck]
        [HttpPost]
        public ActionResult Borrar(int id)
        {
            try
            {
                var result = api.Delete(id, url + "/");

                if (result)
                {
                    return RedirectToAction("Listado");
                }

                ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                return View();
            }
            catch (Exception)
            {
                return View();
            }
        }
    }
}