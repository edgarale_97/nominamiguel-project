﻿using Business;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebSite.App_Start;
using WebSite.Models;

namespace WebSite.Controllers
{
    /// <summary>
    /// Nombre de la clase: UsuariosController
    /// </summary>
    public class UsuariosController : Controller
    {
        /// <summary>
        /// Se declara un objeto Api
        /// </summary>
        private Api api;

        /// <summary>
        /// Url del API
        /// </summary>
        private string ApiUrl;

        /// <summary>
        /// Url con el controlador
        /// </summary>
        private string url;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public UsuariosController()
        {
            api = new Api();
            ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
            url = ApiUrl + "Usuarios";
            CargarListados();
        }

        [SessionCheck]
        public ActionResult Listado()
        {
            try
            {
                var json = api.Get(url);
                var listado = JsonConvert.DeserializeObject<List<Usuario>>(json);

                return View(listado);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }

        /// <summary>
        /// Metodo que sirve para obtener un listado en formato json
        /// </summary>
        /// <returns>retorna el listado</returns>
        public string ListadoJson()
        {
            try
            {
                var json = api.Get(url);
                return json;
            }
            catch
            {
                return "";
            }
        }

        /// <summary>
        /// Metodo que sirve para validar el usuario
        /// </summary>
        /// <param name="cedula">recibe la cedula</param>
        /// <param name="clave">recibe la clave</param>
        /// <returns>retorna la repuesta</returns>
        public bool ValidarUsuario(string cedula, string clave)
        {
            var json = api.Get(url);
            var listado = JsonConvert.DeserializeObject<List<Usuario>>(json);

            var registro = listado.Where(p => p.Cedula == cedula && p.Contrasenia == clave).FirstOrDefault();

            if(registro != null)
            {
                return true;
            }
            return false;
        }

        [SessionCheck]
        public ActionResult Detalles(int? id)
        {
            try
            {

                var json = api.Get(id, url + "/");
                var usuario = JsonConvert.DeserializeObject<Usuario>(json);

                return View(usuario);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }

        [SessionCheck]
        public ActionResult Perfil()
        {
            ViewBag.Resultado = "";
            ViewBag.Error = "";

            var id = Convert.ToInt32(System.Web.HttpContext.Current.Session["UserID"].ToString());
            var json = api.Get(id, url + "/");
            var usuario = JsonConvert.DeserializeObject<Usuario>(json);

            return View(usuario);
        }

        [SessionCheck]
        [HttpPost]
        public ActionResult Perfil(Usuario usuario, string ConfirmacionClave)
        {
            ViewBag.Resultado = "";
            ViewBag.Error = "";

            if (usuario.Contrasenia == ConfirmacionClave)
            {
                var jsonObj = JsonConvert.SerializeObject(usuario);
                var result = api.Put(jsonObj, url + "/");

                if (result)
                {
                    ViewBag.Resultado = "Datos actualziados correctamente";
                }
                else
                {
                    ViewBag.Error = "Se presento un error al guardar los datos";
                }
            }
            else
            {
                ViewBag.Error = "La clave y la confirmación son distintas";
            }

            return View(usuario);
        }

        [SessionCheck]
        public ActionResult Crear()
        {
            return View(new Usuario());
        }

        [SessionCheck]
        [HttpPost]
        public ActionResult Crear(Usuario usuario)
        {
            try
            {
                usuario.CreateDate = DateTime.Now;
                var jsonObj = JsonConvert.SerializeObject(usuario);
                var result = api.Post(jsonObj, url);

                if (result)
                {
                    return RedirectToAction("Listado");
                }

                ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                return View();

            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }

        [SessionCheck]
        public ActionResult Editar(int? id)
        {
            try
            {
                var json = api.Get(id, url + "/");
                var usuario = JsonConvert.DeserializeObject<Usuario>(json);
                return View(usuario);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }

        }

        [SessionCheck]
        [HttpPost]
        public ActionResult Editar(Usuario usuario)
        {
            try
            {
                usuario.UpdateUser = "SYSTEM";
                usuario.UpdateDate = DateTime.Now;

                var jsonObj = JsonConvert.SerializeObject(usuario);
                var result = api.Put(jsonObj, url + "/");

                if (result)
                {
                    return RedirectToAction("Listado");
                }

                ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                return View();
            }
            catch (Exception)
            {
                return View();
            }
        }

        [SessionCheck]
        public ActionResult Borrar(int? id)
        {
            try
            {
                var json = api.Get(id, url + "/");
                var usuario = JsonConvert.DeserializeObject<Usuario>(json);
                return View(usuario);
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Server error.");
                return View();
            }
        }

        [SessionCheck]
        [HttpPost]
        public ActionResult Borrar(int id)
        {
            try
            {
                var result = api.Delete(id, url + "/");

                if (result)
                {
                    return RedirectToAction("Listado");
                }

                ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                return View();
            }
            catch (Exception)
            {
                return View();
            }
        }

        /// <summary>
        /// Metodo que sirve para cargar los listados
        /// </summary>
        public void CargarListados()
        {
            var jsonEmpresas = api.Get(ApiUrl + "Empresas");
            var listadoEmpresas = JsonConvert.DeserializeObject<List<Empresa>>(jsonEmpresas);
            ViewBag.Empresas = listadoEmpresas.Select(p => new SelectListItem { Value = p.Code + "", Text = p.Nombre });

            var jsonRoles = api.Get(ApiUrl + "Roles");
            var listadoRoles = JsonConvert.DeserializeObject<List<Rol>>(jsonRoles);
            ViewBag.Roles = listadoRoles.Select(p => new SelectListItem { Value = p.Code + "", Text = p.Nombre });
        }
    }
}