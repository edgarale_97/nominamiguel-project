﻿/***
* Grid.Mvc spanish language (es-MX) http://gridmvc.codeplex.com/
*/
window.GridMvc = window.GridMvc || {};
window.GridMvc.lang = window.GridMvc.lang || {};
GridMvc.lang.es = {
    filterTypeLabel: "Tipo: ",
    filterValueLabel: "Valor :",
    applyFilterButtonText: "Aplicar",
    filterSelectTypes: {
        Equals: "Igual",
        StartsWith: "Inicia con",
        Contains: "Contiene",
        EndsWith: "Termina con",
        GreaterThan: "Mayor que",
        LessThan: "Menor que",
        GreaterThanOrEquals: "Mayor o igual a",
        LessThanOrEquals: "Menor o igual a",
    },
    code: 'es',
    boolTrueLabel: "Si",
    boolFalseLabel: "No",
    clearFilterLabel: "Limpiar Filtro"
};