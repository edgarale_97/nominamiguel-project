﻿$(function () {
    $.ajaxSetup({
        global: true,
        cache: false,
        statusCode: {
            412: function (xhr) {
                toastr.options = {
                    timeOut: 1500,
                    onHidden: function () {
                        window.location.href = siteLocation + "/Account/Login";
                    }
                }
                toastr["error"]("Tu sesión ha expirado", "Toolboy");
            }
        }
    });
    $.blockUI.defaults.message = "<h1>Espera por favor...</h1>";
});


