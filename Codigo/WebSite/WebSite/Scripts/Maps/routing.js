﻿

function initializeRouting() {
    cleanDraws('RUTA');

    drawing_manager = new google.maps.drawing.DrawingManager({
        drawingMode: google.maps.drawing.OverlayType.MARKER,
        drawingControl: true,
        drawingControlOptions: {
            position: google.maps.ControlPosition.TOP_CENTER,
            drawingModes: [
              google.maps.drawing.OverlayType.MARKER
            ]
        },
        markerOptions: {
            clickable: true,
            draggable: true,
            zIndex: 1
        }

    });

    drawing_manager.setMap(map); // add DrawingManager to map

    google.maps.event.addListener(drawing_manager, 'markercomplete', function (route) { pushRouteElement(route, google.maps.drawing.OverlayType.MARKER, 0, 0, false); });

    jQuery('#hiddenWeight').val('1');
    //InitializeSlider();
}

function InitializeSlider() {
    var value = jQuery('#hiddenWeight').val();

    // slider with fixed minimum
    jQuery("#sliderWeight").slider({
        range: "min",
        value: value,
        min: 1,
        max: 200,
        slide: function (event, ui) {
            jQuery("#spanWeight").text(ui.value);
            jQuery('#hiddenWeight').val(ui.value);

            //The slides chaged here.
            buffer_distance = ui.value;
            drawBuffer();
        }
    });

    jQuery("#spanWeight").text(jQuery("#sliderWeight").slider("value"));
}

function pushRouteElement(route, type, minDuration, maxDuration, isReadOnly) {

    var overlay = new Overlay();
    overlay.route = route;
    overlay.index = route_overlays.length;
    overlay.elementNumber = overlay.index + 1;
    overlay.type = type;
    overlay.maxDuration = maxDuration;
    overlay.minDuration = minDuration;

    var pinImage = getPinImage(overlay.index + 1);

    // Agregamos un Marker a los POLYLINE
    if (overlay.type == google.maps.drawing.OverlayType.POLYLINE) {
        route.setMap(map);
        var overlayInitialPosition = overlay.route.getPath().getArray()[0];

        var marker = new window.google.maps.Marker({
            position: overlayInitialPosition,
            map: map,
            icon: pinImage
        });

        overlay.marker = marker;
    }

    if (overlay.type == google.maps.drawing.OverlayType.MARKER) {
        route.icon = pinImage;
        route.setMap(map);
        overlay.marker = route;
    }

    addRouteListener(overlay, !isReadOnly);
    route_overlays.push(overlay);  // Save element into array.
}

function addRouteListener(overlay, editable) {

    var contenido = "";

    if (editable == true) {
        contenido =
        "<div style='line-height:1.35;overflow:hidden;white-space:nowrap;'>" +
            "<table class='table'>" +
                "<tr>" +
                    "<td>" +
                        "<a href='#' class='btn btn-circle'><i class='icon-white'>" + overlay.elementNumber + "</i></a>" +
                    "</td>" +
                    "<td>" +
                        "&nbsp;" +
                    "</td>" +
                    "<td>" +
                        "<a href=\"javascript:removeRouteElement('" + overlay.index + "');\" >Eliminar</a>" +
                    "</td>" +
                "</tr>" +
                "<tr>" +
                    "<td>" +
                        "<label>M&iacute;nimo</label>" +
                    "</td>" +
                    "<td>" +
                        "<input type='text' class='input-small' style='width: 40px;' value='{0}' id='txtMinDuration' onkeyup='return SetMinDuration(this.value, " + overlay.index + ")'></input>" +
                    "</td>" +
                    "<td>" +
                        " <label>minutos</label>" +
                    "</td>" +
                "</tr>" +
                "<tr>" +
                    "<td>" +
                        "<label>M&aacute;ximo</label>" +
                    "</td>" +
                    "<td>" +
                        "<input type='text' class='input-small' style='width: 40px;' value='{1}' id='txtMaxDuration' onkeyup='return SetMaxDuration(this.value, " + overlay.index + ")'></input>" +
                    "</td>" +
                    "<td>" +
                        " <label>minutos</label>" +
                    "</td>" +
                "</tr>" +
            "</table>" +
        "</div>";
    }
    else {
        contenido =
        "<div style='line-height:1.35;overflow:hidden;white-space:nowrap;'>" +
            "<table class='table'>" +
                "<tr>" +
                    "<td>" +
                        "<a href='#' class='btn btn-circle'><i class='icon-white'>" + overlay.elementNumber + "</i></a>" +
                    "</td>" +
                "</tr>" +
                "<tr>" +
                    "<td>" +
                        "<label>M&iacute;nimo: " + overlay.minDuration + " minutos</label>" +
                    "</td>" +
                "</tr>" +
                "<tr>" +
                    "<td>" +
                        "<label>M&aacute;ximo: " + overlay.maxDuration + " minutos</label>" +
                    "</td>" +
                "</tr>" +
            "</table>" +
        "</div>";
    }

    if (overlay.type == google.maps.drawing.OverlayType.POLYLINE) {

        // Se pone el listener para que cuando hagan click en el marker, despliegue la opción de eliminar (en lugar de en el área)
        google.maps.event.addListener(overlay.marker, 'click', function (event) {

            // aseguramos la persistencia de min y maxDuration.
            var infocontent = contenido.replace("{0}", overlay.minDuration).replace("{1}", overlay.maxDuration);

            // Show the info window.
            infowindow.setContent(infocontent);
            infowindow.setPosition(event.latLng);
            infowindow.open(map);
        });

        // when a single point has been moved
        google.maps.event.addListener(overlay.route.getPath(), 'set_at', function () {
            drawBuffer();

            // Nos aseguramos que el marker que indica la posicion inicial siempre se encuentre sincronizado.
            var overlayInitialPosition = overlay.route.getPath().getArray()[0];
            overlay.marker.setPosition(overlayInitialPosition);
        });

        // when a new point has been added
        google.maps.event.addListener(overlay.route.getPath(), 'insert_at', function () {
            drawBuffer();
        });
    }
    if (overlay.type == google.maps.drawing.OverlayType.MARKER) {
        google.maps.event.addListener(overlay.route, 'dragend', function () {
            drawBuffer();
        });

        overlay.route.setIcon(getPinImage(overlay.index + 1));
    }

    overlay.listenerHandle = google.maps.event.addListener(overlay.route, 'click', function (event) {

        // aseguramos la persistencia de min y maxDuration.
        var infocontent = contenido.replace("{0}", overlay.minDuration).replace("{1}", overlay.maxDuration);

        // Show the info window.
        infowindow.setContent(infocontent);
        infowindow.setPosition(event.latLng);
        infowindow.open(map);
    });
}

function removeRouteElement(index) {

    google.maps.event.clearListeners(map, 'click');

    //Remove the element from array
    route_overlays[index].route.setMap(null);

    if (route_overlays[index].type == google.maps.drawing.OverlayType.POLYLINE) {
        route_overlays[index].marker.setMap(null);
    }

    route_overlays.splice(index, 1);

    //Se reestructura el index
    var i = 0;
    for (i = 0; i < route_overlays.length; i++) {
        route_overlays[i].index = i;
        route_overlays[i].elementNumber = i + 1;

        //google.maps.event.removeListener(route_overlays[i].listenerHandle);
        addRouteListener(route_overlays[i], true);

        // Cambiamos la imagen del marker
        if (route_overlays[i].type == google.maps.drawing.OverlayType.POLYLINE) {
            route_overlays[i].marker.setIcon(getPinImage(i + 1));
        }

        if (route_overlays[i].type == google.maps.drawing.OverlayType.MARKER) {
            route_overlays[i].route.setIcon(getPinImage(i + 1));
        }
    };

    //Se oculta el infowindow.
    infowindow.close();
    drawBuffer();
}

function drawBuffer() {

    if (buffer_route_zone) {
        for (var i = 0; i < buffer_route_zone.length; i++) {
            buffer_route_zone[i].setMap(null);
            buffer_route_zone[i] = null;
        }

        buffer_route_zone = [];
    }

    if (draw_type == 'RUTA') {

        if (!route_overlays) {
            return;
        } // route on map validation

        // Para todos los elementos Polyline les dibujamos un buffer.
        var i = 0;
        for (i = 0; i < route_overlays.length; i++) {

            if (route_overlays[i].type == google.maps.drawing.OverlayType.POLYLINE) {

                var overviewPath = route_overlays[i].route.getPath().getArray(); //Las coordenadas se guardan en un array

                var overviewPathGeo = [];
                for (var j = 0; j < overviewPath.length; j++) {
                    overviewPathGeo.push(
                        [overviewPath[j].lat(), overviewPath[j].lng()]
                    );
                }

                var distance = 0.00001 * buffer_distance,
                    geoInput = {
                        type: "LineString",
                        coordinates: overviewPathGeo
                    };

                var geoReader = new jsts.io.GeoJSONReader(),
                    geoWriter = new jsts.io.GeoJSONWriter();
                var geometry = geoReader.read(geoInput).buffer(distance);
                var polygon = geoWriter.write(geometry);

                var path = [];

                jQuery.each(polygon.coordinates[0], function (index, item) {
                    path.push(new google.maps.LatLng(item[0], item[1]));
                });

                var buffer = new google.maps.Polygon({
                    paths: path,
                    strokeColor: '#0866C6',
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillColor: '#0866C6',
                    fillOpacity: 0.35,
                    editable: false
                });

                buffer.setMap(map);

                buffer_route_zone.push(buffer);
            }

            if (route_overlays[i].type == google.maps.drawing.OverlayType.MARKER) {
                var overviewPath = route_overlays[i].route.getPosition();

                var buffer = new google.maps.Circle({
                    center: overviewPath,
                    radius: buffer_distance,
                    strokeColor: '#0866C6',
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillColor: '#0866C6',
                    fillOpacity: 0.35,
                    editable: false
                });

                buffer.setMap(map);

                buffer_route_zone.push(buffer);
            }
        };
    }

}

function guardarRouting() {
    if (draw_type == 'RUTA') {

        if (!route_overlays) {
            mensajeAlerta(message);
            return false;
        } // route on map validation

        coordinates = getJSONRouteCoordinates();
        jQuery('#coordinates').val(coordinates);
        
        __doPostBack('SaveLink', '');
    } else {
        mensajeAlerta(message);
        return false;
    }

    return true;
}

function getJSONRouteCoordinates() {

    var coordinatesArray = [];

    // Para todos los elementos Polyline les dibujamos un buffer.
    var i = 0;
    for (i = 0; i < route_overlays.length; i++) {

        var route_coordinate = new RouteCoordinate();
        route_coordinate.type = route_overlays[i].type;

        if (route_overlays[i].type == google.maps.drawing.OverlayType.POLYLINE) {

            var routePathArray = route_overlays[i].route.getPath().getArray();
            var coordinates = [];

            jQuery.each(routePathArray, function (index, value) {
                var coordinate = new Coordinate();
                coordinate.lat = value.lat();
                coordinate.lng = value.lng();
                coordinates.push(coordinate);
            });

            route_coordinate.coordinates = coordinates;
        }

        if (route_overlays[i].type == google.maps.drawing.OverlayType.MARKER) {
            route_coordinate.coordinates = [];
            var coordinate = new Coordinate();

            var test = route_overlays[i].route.getPosition().lat();

            coordinate.lat = route_overlays[i].route.getPosition().lat();
            coordinate.lng = route_overlays[i].route.getPosition().lng();
            route_coordinate.coordinates.push(coordinate);
        }
        
        route_coordinate.maxDuration = route_overlays[i].maxDuration;
        route_coordinate.minDuration = route_overlays[i].minDuration;

        coordinatesArray.push(route_coordinate);
    }

    var serializedstring = JSON.stringify(coordinatesArray);

    return serializedstring;
}

function drawRouting(id, readonly) {

    var obj;

    var valor = jQuery('#' + id).val();
    obj = jQuery.parseJSON(valor);

    cleanDraws(obj.type);

    if (obj.type == 'RUTA') {

        jQuery.each(obj.events, function (eventIndex, eventItem) {

            if (eventItem.type == "PUNTO") {

                var marker = new window.google.maps.Marker({
                    position: new google.maps.LatLng(eventItem.center.lat, eventItem.center.long),
                    //strokeColor: '#ff0000',
                    clickable: true,
                    draggable: !readonly,
                    zIndex: 1
                });

                pushRouteElement(marker, google.maps.drawing.OverlayType.MARKER, eventItem.minDuration, eventItem.maxDuration, readonly);
            }

            if (eventItem.type == "LINEA") {
                var path = [];

                jQuery.each(eventItem.coordinates, function (index, item) {
                    if (item) {
                        path.push(new google.maps.LatLng(item.lat, item.long));
                    }
                });

                var route = new google.maps.Polyline({
                    //strokeColor: '#ff0000',
                    path: path,
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    clickable: true,
                    editable: !readonly,
                    zIndex: 1
                });

                pushRouteElement(route, google.maps.drawing.OverlayType.POLYLINE, eventItem.minDuration, eventItem.maxDuration, readonly);
            }
        });

        buffer_distance = obj.buffer;
        drawBuffer();
    }
}