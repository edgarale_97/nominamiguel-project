﻿var map;
var trafficLayer;

var drawing_manager = null;
var circle_zone = null;
var polygon_zone = null;
var route_zone = null;
var buffer_route_zone = []; // Contiene los elementos que se dibujan (Buffer) de una Ruta.
var buffer_distance = 1;
var draw_type;

var coordinates;
var center;
var radius;

var locations = [];
var markersArray = [];
var markerClusterer = null;
var infowindow;

var route_overlays = []; // Contiene los elementos que se dibujan en una Ruta.
var route_markers = [];
var equipos_seleccionados; // Contiene los id´s de los equipos seleccionados separados por coma

google.maps.Circle.prototype.contains = function(latLng) {
    return this.getBounds().contains(latLng) && google.maps.geometry.spherical.computeDistanceBetween(this.getCenter(), latLng) <= this.getRadius();
};

google.maps.Polygon.prototype.contains = function (point) {
    // ray casting alogrithm http://rosettacode.org/wiki/Ray-casting_algorithm
    var crossings = 0,
        path = this.getPath();

    // for each edge
    for (var i = 0; i < path.getLength() ; i++) {
        var a = path.getAt(i),
            j = i + 1;
        if (j >= path.getLength()) {
            j = 0;
        }
        var b = path.getAt(j);
        if (rayCrossesSegment(point, a, b)) {
            crossings++;
        }
    }

    // odd number of crossings?
    return (crossings % 2 == 1);

    function rayCrossesSegment(point, a, b) {
        var px = point.lng(),
            py = point.lat(),
            ax = a.lng(),
            ay = a.lat(),
            bx = b.lng(),
            by = b.lat();
        if (ay > by) {
            ax = b.lng();
            ay = b.lat();
            bx = a.lng();
            by = a.lat();
        }
        // alter longitude to cater for 180 degree crossings
        if (px < 0) { px += 360 };
        if (ax < 0) { ax += 360 };
        if (bx < 0) { bx += 360 };

        if (py == ay || py == by) py += 0.00000001;
        if ((py > by || py < ay) || (px > Math.max(ax, bx))) return false;
        if (px < Math.min(ax, bx)) return true;

        var red = (ax != bx) ? ((by - ay) / (bx - ax)) : Infinity;
        var blue = (ax != px) ? ((py - ay) / (px - ax)) : Infinity;
        return (blue >= red);
    }
};

function initialize() {
    var mapOptions = {
        zoom: 12,
        mapTypeId: window.google.maps.MapTypeId.ROADMAP,
        center: new window.google.maps.LatLng(19.432641, -99.13326),
        mapTypeControlOptions:
        {
            style: window.google.maps.MapTypeControlStyle.DROPDOWN_MENU,
            poistion: window.google.maps.ControlPosition.TOP_RIGHT,
            mapTypeIds: [window.google.maps.MapTypeId.ROADMAP,
                window.google.maps.MapTypeId.TERRAIN,
                window.google.maps.MapTypeId.HYBRID,
                window.google.maps.MapTypeId.SATELLITE]
        }
    };

    map = new google.maps.Map(document.getElementById('mapContainer'), mapOptions);
    trafficLayer = new google.maps.TrafficLayer();

    infowindow = new window.google.maps.InfoWindow();
    
    TrafficToggleButton();

    drawing_manager = new google.maps.drawing.DrawingManager({ drawingControl: false });
}

function ResizeMap() {
    window.google.maps.event.trigger(map, "resize");
}

function getPinImage(number) {
    var markerColor = "FE7569";

    return new window.google.maps.MarkerImage(
        "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=" + number + "|" + markerColor,
        new window.google.maps.Size(21, 34),
        new window.google.maps.Point(0, 0),
        new window.google.maps.Point(10, 34));
}

// Se recorren todos los vehículos, y sino están dentro del área, mandamos mensaje de confirmacion (si desea continuar).
function warnningVehiculos() {
    
    if (draw_type == 'CIRCUNFERENCIA') {
        for (var i = 0; i < locations.length; i++) {
            if (!circle_zone.contains(new window.google.maps.LatLng(locations[i].latitud, locations[i].longuitud))) {
                var r = confirm('Hay vehículos que no se encuentran dentro de la circunferencia, desea continuar?');
                if (r == false) {
                    // CANCEL
                    return true;
                } else {
                    // OK
                    return false;
                }
            }
        }
    }
    
    if (draw_type == 'POLIGONO') {
        for (var i = 0; i < locations.length; i++) {
            if (!polygon_zone.contains(new window.google.maps.LatLng(locations[i].latitud, locations[i].longuitud))) {
                var r = confirm('Hay vehículos que no se encuentran dentro del polígono, desea continuar?');
                if (r == false) {
                    // CANCEL
                    return true;
                } else {
                    // OK
                    return false;
                }
            }
        }
    }

    return false;
}

function mensajeAlerta(mensaje) {
    bootstrap_alert_danger.show(mensaje);
}

function cleanDraws(type) {

    // Se define el tipo de dibujo que se está manejando
    draw_type = type;
    jQuery("#labelTipo").text(draw_type);
    jQuery("#TipoHidden").val(draw_type);

    if (draw_type == 'RUTA') {
        showRutaParametro(true);
    } else {
        showRutaParametro(false);
    }


    if (circle_zone) {
        circle_zone.setMap(null); // remove it from map
        circle_zone = null;
    }
    if (polygon_zone) {
        polygon_zone.setMap(null);
        polygon_zone = null;
    }
    if (route_overlays) {
        var i = 0;
        $(route_overlays).each(function (i) {
            
            if (route_overlays[i].type == google.maps.drawing.OverlayType.POLYLINE) {
                route_overlays[i].marker.setMap(null);
            }

            route_overlays[i].route.setMap(null);
            route_overlays[i].route = null;
            google.maps.event.removeListener(route_overlays[i].listenerHandle);
        });

        route_overlays = [];
    }
    if (buffer_route_zone) {
        for (var i = 0; i < buffer_route_zone.length; i++) {
            buffer_route_zone[i].setMap(null);
            buffer_route_zone[i] = null;
        }
        
        buffer_route_zone = [];
    }

    if (drawing_manager) {
        drawing_manager.setMap(null);
    }
    
    if (type == '') {
        for (var i = 0; i < markersArray.length; i++) {
            markersArray[i].setMap(null);
        }

        if (markerClusterer) {
            markerClusterer.clearMarkers();
        }

        markersArray = [];
    }
}

function showRutaParametro(valor) {
    if (valor) {
        jQuery('#rutaParametro1').show();
        jQuery('#rutaParametro2').show();
    } else {
        jQuery('#rutaParametro1').hide();
        jQuery('#rutaParametro2').hide();
    }
}

function guardar() {
    if (!savePoints()) {
        return;
    }

    if (draw_type == 'CIRCUNFERENCIA') {
        jQuery('#center').val(center);
        jQuery('#radius').val(radius);
    } else {
        jQuery('#coordinates').val(coordinates);
    }

    __doPostBack('SaveLink', '');
}

function guardarModificacion() {
    if (!savePoints()) {
        return;
    }

    if (draw_type == 'CIRCUNFERENCIA') {
        jQuery('#center').val(center);
        jQuery('#radius').val(radius);
    } else {
        jQuery('#coordinates').val(coordinates);
    }

    __doPostBack('ModifySaveLink', '');
}

// Pinta las geocercas para ser modificadas.
function drawToModify(id, type, param_int) {

    // Separamos los puntos y los guardamos en un array
    var points = jQuery('#' + id).val().split('|');
    var path = [];

    jQuery.each(points, function (index, item) {
        if (item != '') {
            var point = item.split(',');
            path.push(new google.maps.LatLng(point[0], point[1]));
        }
    });

    cleanDraws(type);

    if (type == 'CIRCUNFERENCIA') {
        circle_zone = new google.maps.Circle({
            center: path[0],
            radius: param_int,
            strokeColor: '#ff0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#ff0000',
            fillOpacity: 0.35,
            editable: true
        });

        circle_zone.setMap(map);
    }

    if (type == 'POLIGONO') {
        polygon_zone = new google.maps.Polygon({
            paths: path,
            strokeColor: '#ff0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#ff0000',
            fillOpacity: 0.35,
            editable: true
        });

        polygon_zone.setMap(map);
    }
    
    if (type == 'RUTA') {
        route_zone = new google.maps.Polyline({
            path: path,
            strokeColor: '#ff0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            editable: true
        });

        // when a single point has been moved
        google.maps.event.addListener(route_zone.getPath(), 'set_at', function () {
            drawBuffer();
        });

        // when a new point has been added
        google.maps.event.addListener(route_zone.getPath(), 'insert_at', function () {
            drawBuffer();
        });

        route_zone.setMap(map);
        buffer_distance = param_int;
        drawBuffer();
    }
    
    map.panTo(path[0]);
}

function drawEvento(id, readonly) {

    var obj;

    var valor = jQuery('#' + id).val();
    obj = jQuery.parseJSON(valor);
    
    cleanDraws(obj.type);

    if (obj.type == 'CIRCUNFERENCIA') {
        var center = new google.maps.LatLng(obj.center.lat, obj.center.long);
        circle_zone = new google.maps.Circle({
            center: center,
            radius: obj.radius,
            //strokeColor: '#ff0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillOpacity: 0.35,
            editable: !readonly
        });

        circle_zone.setMap(map);
        map.panTo(center);
    }

    if (obj.type == 'POLIGONO') {
        var path = [];

        jQuery.each(obj.coordinates, function (index, item) {
            if (item) {
                path.push(new google.maps.LatLng(item.lat, item.long));
            }
        });

        polygon_zone = new google.maps.Polygon({
            paths: path,
            //strokeColor: '#ff0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillOpacity: 0.35,
            editable: !readonly
        });

        polygon_zone.setMap(map);
        map.panTo(path[0]);
    }

    if (obj.type == 'RUTA') {

        jQuery.each(obj.events, function(eventIndex, eventItem) {

            if (eventItem.type == "PUNTO") {
                
                var marker = new window.google.maps.Marker({
                    position: new google.maps.LatLng(eventItem.center.lat, eventItem.center.long),
                    //strokeColor: '#ff0000',
                    clickable: true,
                    draggable: !readonly,
                    zIndex: 1
                });

                pushRouteElement(marker, google.maps.drawing.OverlayType.MARKER, eventItem.minDuration, eventItem.maxDuration, readonly);
            }
            
            if (eventItem.type == "LINEA") {
                var path = [];

                jQuery.each(eventItem.coordinates, function (index, item) {
                    if (item) {
                        path.push(new google.maps.LatLng(item.lat, item.long));
                    }
                });

                var route = new google.maps.Polyline({
                    //strokeColor: '#ff0000',
                    path: path,
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    clickable: true,
                    editable: !readonly,
                    zIndex: 1
                });
                
                pushRouteElement(route, google.maps.drawing.OverlayType.POLYLINE, eventItem.minDuration, eventItem.maxDuration, readonly);
            }
        });

        buffer_distance = obj.buffer;
        drawBuffer();
    }
}


//***********************************
//****** MARKERS FUNCTIONS **********
//***********************************

function AddMarker(idVehiculo, latitud, longitud, title, showInfoWindow, pinColor) {

    var loc = new MarkerLocation();
    loc.idVehiculo = idVehiculo;
    loc.latitud = latitud;
    loc.longitud = longitud;
    loc.title = title;
    loc.showInfoWindow = showInfoWindow;
    loc.pinColor = pinColor;

    //agregamos el elemenyto al array.
    locations.push(loc);

    UpdateMap();
}

function AddMarkerWithoutUpdate(idVehiculo, latitud, longitud, title, showInfoWindow, pinColor, alarmed, alarmType) {

    var loc = new MarkerLocation();
    loc.idVehiculo = idVehiculo;
    loc.latitud = latitud;
    loc.longitud = longitud;
    loc.title = title;
    loc.showInfoWindow = showInfoWindow;
    loc.pinColor = pinColor;
    loc.alarmed = alarmed;
    loc.alarmType = alarmType;

    //agregamos el elemenyto al array.
    locations.push(loc);
}

function RemoveMarker(idVehiculo) {

    //Quitamos el elemento del array.
    var removeMe;
    $.each(locations, function (i, location) {
        if (location.idVehiculo == idVehiculo) {
            removeMe = i;
        }
    });

    locations.splice(removeMe, 1);
    UpdateMap();
}

function RemoveAllMarkers() {
    locations = [];
    equipos_seleccionados = '';
    UpdateMap();
}

function UpdateMap() {

    clearOverlays();

    var countAddedLocation = 0;
    equipos_seleccionados = '';

    $.each(locations, function (i, location) {

        countAddedLocation++;
        var image = "";
        var pinImage;

        if (location.alarmed) {
            image = siteLocation + "Content/Images/Markers/caution.png";
            
            pinImage = new window.google.maps.MarkerImage(
            image,
            new window.google.maps.Size(32, 37),
            new window.google.maps.Point(0, 0),
            new window.google.maps.Point(16, 37));
        } else {
            image = "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + location.pinColor;
            
            pinImage = new window.google.maps.MarkerImage(
            image,
            new window.google.maps.Size(21, 34),
            new window.google.maps.Point(0, 0),
            new window.google.maps.Point(10, 34));
        }

        var marker = new window.google.maps.Marker
                                        (
                                            {
                                                position: new window.google.maps.LatLng(location.latitud, location.longitud),
                                                map: map,
                                                title: location.title,
                                                icon: pinImage,
                                            }
                                        );

        markersArray.push(marker);
        equipos_seleccionados += location.idVehiculo + ',';

        map.panTo(marker.getPosition());

        (function (fMarker, fLocation) {

            window.google.maps.event.addListener(fMarker, 'click', function () {
                BuildInfoWindow(fMarker, fLocation, true);
            });

        })(marker, location);
    });

    // Guardamos el valor de los equipos seleccionados en un valor hidden
    $('#EquiposSeleccionadosHidden').val(equipos_seleccionados.substring(0, equipos_seleccionados.length - 1));
}

function BuildInfoWindow(fMarker, fLocation, fIsAvanzado) {
    
    infowindow.close();
    infowindow = new window.google.maps.InfoWindow();
    var contenidoMarker = "";

    if (fLocation.alarmed && (fLocation.alarmType == 'landmarks' || fLocation.alarmType == 'safe_routing')) {

        var id = $('#IdSeleccionado').val();

        contenidoMarker = "" +
        "<div id='info-form'>" +
        "<center><br/><img src='" + siteLocation + "Content/Images/loader.gif'/> &nbsp; Cargando...</center>" +
        "</div>" +
        "<iframe id='info-iframe' src='ResetAlarmedStatus?id=" + id + "&idVehiculo=" + fLocation.idVehiculo + "&type=" + fLocation.alarmType + "' ></iframe>";

    } else {

        contenidoMarker = "" +
        "<div id='info-form'>" +
        "<center><br/><img src='" + siteLocation + "Content/Images/loader.gif'/> &nbsp; Cargando...</center>" +
        "</div>" +
        "<iframe id='info-iframe' src='../Vehiculos/Info/" + fLocation.idVehiculo + "' ></iframe>";
    }

    infowindow.setContent(contenidoMarker);

    // Calling the open method of the infoWindow 
    infowindow.open(map, fMarker);
}

function UpdateMapSimple() {

    clearOverlays();

    var countAddedLocation = 0;

    $.each(locations, function (i, location) {

        countAddedLocation++;
        var image;

        if (isMarkerNumber) {
            image = "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=" + (i + 1).toString() + "|" + location.pinColor;
        } else {
            image = "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + location.pinColor;
        }

        var pinImage = new window.google.maps.MarkerImage(
            image,
            new window.google.maps.Size(21, 34),
            new window.google.maps.Point(0, 0),
            new window.google.maps.Point(10, 34));

        var marker = new window.google.maps.Marker
                                        (
                                            {
                                                position: new window.google.maps.LatLng(location.latitud, location.longitud),
                                                map: map,
                                                title: location.title,
                                                icon: pinImage,
                                            }
                                        );

        markersArray.push(marker);
    });

    map.panTo(markersArray[0].getPosition());
}

function openPopUp(url, idVehiculo) {
    var params = [
    'height=' + $(window).height(),
    'width=' + $(window).width(),
    'fullscreen=yes',
    'resizable = 1'
    ].join(',');

    var popup = window.open(url + '?idVehiculo=' + idVehiculo, 'popup_window', params);
    popup.moveTo(0, 0);
}

function SetCenter(latitude, longitude) {
    map.setCenter(latitude, longitude);
}

function clearOverlays() {
    for (var i = 0; i < markersArray.length; i++) {
        markersArray[i].setMap(null);
    }
}

function TrafficToggleButton() {

    var controlDiv = document.createElement('DIV');
    $(controlDiv).addClass('gmap-control-container')
                 .addClass('gmnoprint');

    var controlUI = document.createElement('DIV');
    $(controlUI).addClass('gmap-control');
    $(controlUI).text('Tráfico');
    $(controlDiv).append(controlUI);

    var legend = '<ul>'
               + '<li><span style="background-color: #30ac3e">&nbsp;&nbsp;</span><span style="color: #30ac3e"> &gt; 80 km por hora</span></li>'
               + '<li><span style="background-color: #ffcf00">&nbsp;&nbsp;</span><span style="color: #ffcf00"> 40 - 80 km por hora</span></li>'
               + '<li><span style="background-color: #ff0000">&nbsp;&nbsp;</span><span style="color: #ff0000"> &lt; 40 km por hora</span></li>'
               + '<li><span style="background-color: #c0c0c0">&nbsp;&nbsp;</span><span style="color: #c0c0c0"> Sin información</span></li>'
               + '</ul>';

    var controlLegend = document.createElement('DIV');
    $(controlLegend).addClass('gmap-control-legend');
    $(controlLegend).html(legend);
    $(controlLegend).hide();
    $(controlDiv).append(controlLegend);

    // Set hover toggle event
    $(controlUI)
        .mouseenter(function () {
            $(controlLegend).show();
        })
        .mouseleave(function () {
            $(controlLegend).hide();
        });

    var trafficLayer = new google.maps.TrafficLayer();

    google.maps.event.addDomListener(controlUI, 'click', function () {
        if (typeof trafficLayer.getMap() == 'undefined' || trafficLayer.getMap() === null) {
            $(controlUI).addClass('gmap-control-active');
            trafficLayer.setMap(map);
        } else {
            trafficLayer.setMap(null);
            $(controlUI).removeClass('gmap-control-active');
        }
    });

    map.controls[google.maps.ControlPosition.TOP_RIGHT].push(controlDiv);
}

function SetMinDuration(e, index) {
    
    if (!/^[0-9]+$/.test(e)) 
    { 
        e = e.substring(0, e.length-1);
    }
    
    $('#txtMinDuration').val(e);
    route_overlays[index].minDuration = e;
}

function SetMaxDuration(e, index) {

    if (!/^[0-9]+$/.test(e)) {
        e = e.substring(0, e.length - 1);
    }

    $('#txtMaxDuration').val(e);
    route_overlays[index].maxDuration = e;
}

function DisableAlarm(idVehiculo) {
    $.each(locations, function(i, location) {

        if (location.idVehiculo == idVehiculo) {
            
            var image = "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + location.pinColor;

            var pinImage = new window.google.maps.MarkerImage(
            image,
            new window.google.maps.Size(21, 34),
            new window.google.maps.Point(0, 0),
            new window.google.maps.Point(10, 34));

            location.alarmed = false;
            infowindow.close();

            markersArray[i].setIcon(pinImage);
        }
    });
}
