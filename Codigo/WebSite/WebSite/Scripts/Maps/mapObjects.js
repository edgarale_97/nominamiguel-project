﻿/* Contiene todos los objetos que se utilizan en los mapas */

function Overlay() {
    this.route = null;
    this.index = 0;
    this.elementNumber = 0;
    this.type = null; //google.maps.drawing.OverlayType
    this.listenerHandle = null;
    this.marker = null; // Cuando es un POLYLINE, se le agrega un marker para indicar el inicio.
    this.maxDuration = 0; //
    this.minDuration = 0;
}

function RouteCoordinate() {
    this.coordinates = null;
    this.type = null;
    this.maxDuration = 0;
    this.minDuration = 0;
}

function Coordinate() {
    this.lat = 0;
    this.lng = 0;
}

function MarkerLocation() {
    this.idVehiculo = 0;
    this.latitud = 0.0;
    this.longitud = 0.0;
    this.title = '';
    this.showInfoWindow = false;
    this.pinColor = '#ff0000';
    this.tipoUbicacion = tiposUbicacion.Normal;
    this.alarmed = false;
    this.alarmType = '';
    this.address = '';
    this.date = '';
    this.markerPosition = 0;
    this.image = '';
}

function MarkerInfoWindow() {
    this.vehiculo = '';
    this.estatusEquipo = '';
    this.latitud = 0.0;
    this.longitud = 0.0;
    this.imagenSenal = '';
    this.estatusSenal = '';
    this.colorSenal = '';
    this.fechaUbicacion = '';
}

var tiposUbicacion = {
    Normal : 1,
    HarshAcceleration : 2,
    HarshBreak : 3,
    StartTrip : 4,
    EndTrip : 5,
    PanicButton : 6,
    SpeedLimit : 7,
    LowBattery : 8,
    GpsCut : 9,
    Tow: 10,
    DeviceDisconnected : 11
};

function PolygonZone() {
    this.id = 0;
    this.name = '';
    this.polygon = null;
    this.path = null;
}

function CircleZone() {
    this.id = 0;
    this.name = '';
    this.circle = null;
    this.path = null;
}