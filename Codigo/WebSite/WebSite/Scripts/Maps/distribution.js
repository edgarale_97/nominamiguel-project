﻿// Mapa de distribución y de calor.

var map, pointArray, heatmap, pointsData = [], markersArray = [];
var markerClusterer = null;
var heatmapRadius = 60;
var heatmapIntensity = 60;

var styler = [
{
    "featureType": "administrative",
    "stylers": [
        {
            "visibility": "off"
        }
    ]
},
{
    "featureType": "poi",
    "stylers": [
        {
            "visibility": "simplified"
        }
    ]
},
{
    "featureType": "road",
    "stylers": [
        {
            "visibility": "simplified"
        }
    ]
},
{
    "featureType": "water",
    "stylers": [
        {
            "visibility": "simplified"
        }
    ]
},
{
    "featureType": "transit",
    "stylers": [
        {
            "visibility": "simplified"
        }
    ]
},
{
    "featureType": "landscape",
    "stylers": [
        {
            "visibility": "simplified"
        }
    ]
},
{
    "featureType": "road.highway",
    "stylers": [
        {
            "visibility": "off"
        }
    ]
},
{
    "featureType": "road.local",
    "stylers": [
        {
            "visibility": "on"
        }
    ]
},
{
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
        {
            "visibility": "on"
        }
    ]
},
{
    "featureType": "road.arterial",
    "stylers": [
        {
            "visibility": "off"
        }
    ]
},
{
    "featureType": "water",
    "stylers": [
        {
            "color": "#5f94ff"
        },
        {
            "lightness": 26
        },
        {
            "gamma": 5.86
        }
    ]
},
{},
{
    "featureType": "road.highway",
    "stylers": [
        {
            "weight": 0.6
        },
        {
            "saturation": -85
        },
        {
            "lightness": 61
        }
    ]
},
{
    "featureType": "road"
},
{},
{
    "featureType": "landscape",
    "stylers": [
        {
            "hue": "#0066ff"
        },
        {
            "saturation": 74
        },
        {
            "lightness": 100
        }
    ]


}];

function initializeDistribucionFlota() {

    var options =
    {
        zoom: 5,
        mapTypeId: window.google.maps.MapTypeId.ROADMAP,

        center: new window.google.maps.LatLng(24.624175, -103.128420),

        mapTypeControlOptions:
        {
            //style: window.google.maps.MapTypeControlStyle.DROPDOWN_MENU,
            styles: styler,
            poistion: window.google.maps.ControlPosition.TOP_RIGHT,
            mapTypeIds: [window.google.maps.MapTypeId.ROADMAP,
              window.google.maps.MapTypeId.TERRAIN,
              window.google.maps.MapTypeId.HYBRID,
              window.google.maps.MapTypeId.SATELLITE]
        },
        panControl: true,
        panControlOptions: {
            position: google.maps.ControlPosition.LEFT_TOP
        },
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.LARGE,
            position: google.maps.ControlPosition.LEFT_TOP
        },
        scaleControl: true,
        scaleControlOptions: {
            position: google.maps.ControlPosition.LEFT_TOP
        },
        streetViewControl: true,
        streetViewControlOptions: {
            position: google.maps.ControlPosition.LEFT_TOP
        }
    };

    map = new window.google.maps.Map(document.getElementById("mapContainer"), options);
    
    $("#rangeRadius").on('slidestop', function (event) {
        heatmapRadius = parseInt($('#rangeRadius').val());
        heatmapIntensity = parseInt($('#rangeIntensity').val());

        changeHeatmap();
    });

    $("#rangeIntensity").on('slidestop', function (event) {

        heatmapRadius = parseInt($('#rangeRadius').val());
        heatmapIntensity = parseInt($('#rangeIntensity').val());

        changeHeatmap();
    });
}

function wu(id, lat, lon, tag) {

    var position = new window.google.maps.LatLng(lat, lon);
    var marker = new window.google.maps.Marker({ position: position, title: tag });

    markersArray.push(marker);
    pointsData.push(position);
}

function up(isCluster) {

    if (isCluster === false) {

        //if (!heatmap) {
            pointArray = new google.maps.MVCArray(pointsData);

            heatmap = new google.maps.visualization.HeatmapLayer({
                data: pointArray
            });
        //}

        heatmap.setMap(map);
        heatmap.set('radius', heatmapRadius);
        heatmap.set('maxIntensity', heatmapIntensity); // todo: dependiendo del zoom level vamos cambiando el valor.
        heatmap.set('dissipating', true);
    } else {
        markerClusterer = new MarkerClusterer(map, markersArray);
    }
}

function cl() {
    if (markerClusterer) {
        markerClusterer.clearMarkers();
    }

    if (heatmap) {
        heatmap.setMap(null);
    }
}

function changeCluster() {
    cl();
    up(true);
}

function changeHeatmap() {
    cl();
    up(false);
}

google.maps.event.addDomListener(window, 'load', initializeDistribucionFlota);