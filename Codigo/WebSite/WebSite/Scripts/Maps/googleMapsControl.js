﻿var map;
var panorama;
var trafficLayer;

// Localization variablesAddMarker
var infowindow;
var locations = [];
var markersArray = [];
var linesArray = [];
var geocoder;
var directionsDisplay;
var directionsService = new google.maps.DirectionsService();
var isMarkerNumber;
var avanzado = false;
var idGrupo;
var controlActividad;
var controlTemperatura;
var controlVelocidad;
var markerClusterer = null;
var isClustered = false;

google.maps.event.addDomListener(window, 'load', initialize);

function initialize() {

    var options =
    {
        zoom: 12,
        mapTypeId: window.google.maps.MapTypeId.ROADMAP,

        center: new window.google.maps.LatLng(19.432641, -99.13326),

        mapTypeControlOptions:
        {
            style: window.google.maps.MapTypeControlStyle.DROPDOWN_MENU,
            poistion: window.google.maps.ControlPosition.TOP_RIGHT,
            mapTypeIds: [window.google.maps.MapTypeId.ROADMAP,
              window.google.maps.MapTypeId.TERRAIN,
              window.google.maps.MapTypeId.HYBRID,
              window.google.maps.MapTypeId.SATELLITE]
        },
        panControl: true,
        panControlOptions: {
            position: google.maps.ControlPosition.LEFT_TOP
        },
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.LARGE,
            position: google.maps.ControlPosition.LEFT_TOP
        },
        scaleControl: true,
        scaleControlOptions: {
            position: google.maps.ControlPosition.LEFT_TOP
        },
        streetViewControl: true,
        streetViewControlOptions: {
            position: google.maps.ControlPosition.LEFT_TOP
        }
    };

    map = new window.google.maps.Map(document.getElementById("mapContainer"), options);
    trafficLayer = new google.maps.TrafficLayer();

    infowindow = new window.google.maps.InfoWindow();
    directionsDisplay = new window.google.maps.DirectionsRenderer();

    TrafficToggleButton();
}

function AddMarker(idVehiculo, latitud, longitud, title, showInfoWindow, pinColor) {

    var loc = new MarkerLocation();
    loc.idVehiculo = idVehiculo;
    loc.latitud = latitud;
    loc.longitud = longitud;
    loc.title = title;
    loc.showInfoWindow = showInfoWindow;
    loc.pinColor = pinColor;

    //agregamos el elemenyto al array.
    locations.push(loc);

    UpdateMap();
}

function AddMarkerWithoutUpdate(idVehiculo, latitud, longitud, title, showInfoWindow, tipoUbicacion, address, date) {

    var loc = new MarkerLocation();
    loc.idVehiculo = idVehiculo;
    loc.latitud = latitud;
    loc.longitud = longitud;
    loc.title = title;
    loc.showInfoWindow = showInfoWindow;
    loc.tipoUbicacion = tipoUbicacion;
    loc.pinColor = "0866c6";
    loc.address = address;
    loc.date = date;

    //agregamos el elemenyto al array.
    locations.push(loc);
}

function RemoveMarker(idVehiculo) {

    //Quitamos el elemento del array.
    var removeMe;
    $.each(locations, function (i, location) {
        if (location.idVehiculo == idVehiculo) {
            removeMe = i;
        }
    });

    locations.splice(removeMe, 1);
    UpdateMap();
}

function RemoveAllMarkers() {
    locations = [];
    UpdateMap();
}

function UpdateMap() {

    clearOverlays();

    var countAddedLocation = 0;

    $.each(locations, function (i, location) {

        countAddedLocation++;
        var image;

        if (isMarkerNumber) {
            image = "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=" + (i + 1).toString() + "|" + location.pinColor;
        } else {
            image = "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + location.pinColor;
        }

        var pinImage = new window.google.maps.MarkerImage(
            image,
            new window.google.maps.Size(21, 34),
            new window.google.maps.Point(0, 0),
            new window.google.maps.Point(10, 34));

        var marker;

        if (isClustered) {
            marker = new window.google.maps.Marker
                                        (
                                            {
                                                position: new window.google.maps.LatLng(location.latitud, location.longitud),
                                                title: location.title,
                                                icon: pinImage,
                                            }
                                        );
        } else {
            marker = new window.google.maps.Marker
                                        (
                                            {
                                                position: new window.google.maps.LatLng(location.latitud, location.longitud),
                                                map: map,
                                                title: location.title,
                                                icon: pinImage,
                                            }
                                        );
        }

        markersArray.push(marker);

        map.panTo(marker.getPosition());

        (function (fMarker, fLocation, fIsAvanzado) {

            window.google.maps.event.addListener(fMarker, 'click', function () {
                if (fLocation.showInfoWindow) {
                    BuildInfoWindow(fMarker, fLocation, fIsAvanzado);
                }
            });

        })(marker, location, avanzado);
    });

    if (isClustered) {
        updateMapClustered();
    }
}

function BuildInfoWindow(fMarker, fLocation, fIsAvanzado) {

    infowindow.close();
    infowindow = new window.google.maps.InfoWindow();

    var contenidoMarker = "" +
        "<div id='info-form'>" +
        "<center><br/><img src='" + siteLocation + "Content/Images/loader.gif'/> &nbsp; Cargando...</center>" +
        "</div>" +
        "<iframe id='info-iframe' src='Info/" + fLocation.idVehiculo + "' ></iframe>";

    infowindow.setContent(contenidoMarker);

    // Calling the open method of the infoWindow 
    infowindow.open(map, fMarker);
}

function BuildInfoWindowHistorico(fMarker, fLocation) {

    infowindow.close();
    infowindow = new window.google.maps.InfoWindow();

    if (fLocation.tipoUbicacion == tiposUbicacion.Normal) {

        var latlng = new google.maps.LatLng(fLocation.latitud, fLocation.longitud);
        var geocoder = new google.maps.Geocoder();

        var direccion = "";

        geocoder.geocode({ 'latLng': latlng }, function (results, status) {

            if (status == google.maps.GeocoderStatus.OK) {

                if (results[0]) {
                    direccion = results[0].formatted_address;
                    infowindow.setContent(direccion);
                }
            }
        });

    } else {
        infowindow.setContent(fLocation.title);
    }

    // Calling the open method of the infoWindow 
    infowindow.open(map, fMarker);
}

function UpdateMapSimple() {

    clearOverlays();

    var countAddedLocation = 0;
    var countMarkerNumber = 0;
    
    var direcciones = jQuery('#locations-ul');
    direcciones.empty();

    $.each(locations, function (i, location) {

        location.markerPosition = countAddedLocation;
        countAddedLocation++;

        switch (location.tipoUbicacion) {

            case tiposUbicacion.Normal:

                if (isMarkerNumber) {
                    location.image = "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=" + (countMarkerNumber + 1).toString() + "|" + location.pinColor;
                    countMarkerNumber++;
                } else {
                    location.image = "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + location.pinColor;
                }
                break;
            case tiposUbicacion.HarshAcceleration:
                location.image = siteLocation + "Content/Images/Markers/acceleration.png";
                location.title = "Aceleración Brusca";
                break;
            case tiposUbicacion.HarshBreak:
                location.image = siteLocation + "Content/Images/Markers/break.png";
                location.title = "Frenado Brusco";
                break;
            case tiposUbicacion.StartTrip:
                location.image = siteLocation + "Content/Images/Markers/start.png";
                location.title = "Inicio de Viaje";
                break;
            case tiposUbicacion.EndTrip:
                location.image = siteLocation + "Content/Images/Markers/finish.png";
                location.title = "Fin de Viaje";
                break;
            case tiposUbicacion.PanicButton:
                location.image = siteLocation + "Content/Images/Markers/caution.png";
                location.title = "Boton de Pánico";
                break;
            case tiposUbicacion.SpeedLimit:
                location.image = siteLocation + "Content/Images/Markers/speed.png";
                location.title = "Límite de Velocidad";
                break;
            case tiposUbicacion.LowBattery:
                location.image = siteLocation + "Content/Images/Markers/battery.png";
                location.title = "Batería Baja";
                break;
            case tiposUbicacion.GpsCut:
                location.image = siteLocation + "Content/Images/Markers/gps.png";
                location.title = "Antena Gps Cortada";
                break;
            case tiposUbicacion.Tow:
                location.image = siteLocation + "Content/Images/Markers/tow.png";
                location.title = "Remolque";
                break;
            case tiposUbicacion.DeviceDisconnected:
                location.image = siteLocation + "Content/Images/Markers/caution.png";
                location.title = "Equipo Desconectado";
                break;
            default:
                location.image = siteLocation + "Content/Images/Markers/car.png";
                location.title = "Localización";
                break;
        }

        var pinImage;

        if (location.tipoUbicacion != tiposUbicacion.Normal) {
            pinImage = new window.google.maps.MarkerImage(
            location.image,
            new window.google.maps.Size(32, 37),
            new window.google.maps.Point(0, 0),
            new window.google.maps.Point(16, 37));
            location.address = location.title + ' - ' + location.address;
        }
        else {
            pinImage = new window.google.maps.MarkerImage(
            location.image,
            new window.google.maps.Size(21, 34),
            new window.google.maps.Point(0, 0),
            new window.google.maps.Point(10, 34));
        }

        var marker = new window.google.maps.Marker
                                        (
                                            {
                                                position: new window.google.maps.LatLng(location.latitud, location.longitud),
                                                map: map,
                                                title: location.title,
                                                icon: pinImage,
                                                zIndex: location.tipoUbicacion != tiposUbicacion.Normal ? 1000 : (countMarkerNumber + 1)
                                            }
                                        );

        markersArray.push(marker);

        (function (fMarker, fLocation) {

            window.google.maps.event.addListener(fMarker, 'click', function () {
                BuildInfoWindowHistorico(fMarker, fLocation);
            });

        })(marker, location);
        
        if (location.tipoUbicacion != tiposUbicacion.Normal) {
            direcciones.append('<li><a href="#" id="marker-address-' + location.markerPosition + '"><img src="' + location.image + '" style="height: 17px; width: 15px;"/> &nbsp; ' + location.date + ' - ' + location.address + '</a></li>');
        } else {
            direcciones.append('<li><a href="#" id="marker-address-' + location.markerPosition + '"><img src="' + location.image + '" style="height: 17px; width: 11px;"/> &nbsp; ' + location.date + ' - ' + location.address + '</a></li>');
        }
    });

    $('[id^=marker-address-]').each(function () {
        $(this).unbind('click');
        $(this).click(function (e) {
            e.preventDefault();

            var markerPosition = this.id.replace('marker-address-', '');

            // Aqui obtenemos el marker, y lo animamos
            // iteramos sobre todos y les quitamos la animacion y solo se la dejamos al elemento seleccionado
            $.each(markersArray, function (index, value) {
                if (value.getAnimation() != null) {
                    value.setAnimation(null);
                }

                if (markerPosition == index) {
                    value.setAnimation(google.maps.Animation.BOUNCE);
                    $('html,body').animate({ scrollTop: 0 }, 'slow', function () { });
                    map.panTo(markersArray[index].getPosition());
                }
            });

            return false;
        });
    });

    map.panTo(markersArray[0].getPosition());
}

function SetCenter(latitude, longitude) {
    map.setCenter(latitude, longitude);
}

function clearOverlays() {
    for (var i = 0; i < markersArray.length; i++) {
        markersArray[i].setMap(null);
    }

    for (var j = 0; j < linesArray.length; j++) {
        linesArray[j].setMap(null);
    }

    if (isClustered) {
        if (markerClusterer) {
            markerClusterer.clearMarkers();
        }
    }

    markersArray = [];
}

function ResizeMap() {
    window.google.maps.event.trigger(map, "resize");
    if (panorama != null) {
        window.google.maps.event.trigger(panorama, "resize");
    }
}

function openPopUpGrupo(url) {
    var params = [
    'height=' + $(window).height(),
    'width=' + $(window).width(),
    'fullscreen=yes',
    'resizable = 1'
    ].join(',');

    var popup = window.open(url + '?idGrupo=' + idGrupo, 'popup_window', params);
    popup.moveTo(0, 0);
}

function InitializePanorama(locationsIndex) {
    var location = locations[locationsIndex];

    var panoramaOptions = { position: new window.google.maps.LatLng(location.latitud, location.longitud), pov: { heading: 34, pitch: 10, zoom: 1 } };

    panorama = new window.google.maps.StreetViewPanorama(document.getElementById("pano"), panoramaOptions);
    map.setStreetView(panorama);
}

function CalcularRuta(calle, numero, colonia, municipio, estado, pais) {

    //Se elimina la ultima posicion
    locations.splice(1, 1);

    var address = "";

    if (calle) {
        address += calle + ",";
    }

    if (numero) {
        address += numero + ",";
    }

    if (colonia) {
        address += colonia + ",";
    }

    if (municipio) {
        address += municipio + ",";
    }

    if (estado) {
        address += estado + ",";
    }

    address += pais;

    geocoder = new window.google.maps.Geocoder();

    geocoder.geocode({ 'address': address }, function (results, status) {
        if (status == window.google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);

            AddMarker(0, results[0].geometry.location.lat(), results[0].geometry.location.lng(), address, false, 'FF0000');
            calcRoute(new window.google.maps.LatLng(locations[0].latitud, locations[0].longitud), new window.google.maps.LatLng(locations[1].latitud, locations[1].longitud));
        }
        else {
            alert("Geocode was not successful for the following reason: " + status);
        }
    });
}

function CalcularRutaLocations(latitud, longitud) {

    //Se elimina la ultima posicion
    locations.splice(1, 1);

    AddMarker(0, latitud, longitud, '', false, 'FF0000');
    calcRoute(new window.google.maps.LatLng(locations[0].latitud, locations[0].longitud), new window.google.maps.LatLng(locations[1].longitud, locations[1].longitud));
    map.setCenter(new window.google.maps.LatLng(locations[1].latitud, locations[1].longitud));
}

function calcRoute(startLocation, endLocation) {
    var start = startLocation;
    var end = endLocation;
    var request = {
        origin: start,
        destination: end,
        travelMode: window.google.maps.DirectionsTravelMode.DRIVING
    };

    if (directionsDisplay != null) {
        directionsDisplay.setMap(null);
        directionsDisplay = null;
    }

    directionsDisplay = new window.google.maps.DirectionsRenderer({ 'draggable': true });
    directionsDisplay.setMap(map);
    directionsDisplay.setOptions({ suppressMarkers: true });

    directionsDisplay.setPanel(document.getElementById('directionsPanel'));

    directionsService.route(request, function (response, status) {
        if (status == window.google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
        }
    });
}

function UpdateMapWithNumbers(value) {
    isMarkerNumber = value;
}

function DrawLine(lat1, lng1, lat2, lng2) {

    var pos1 = new google.maps.LatLng(lat1, lng1);
    var pos2 = new google.maps.LatLng(lat2, lng2);

    var polyline = new google.maps.Polygon({
        paths: [pos1, pos2],
        strokeColor: '#0866c6',
        strokeOpacity: 0.8,
        strokeWeight: 3,
        fillColor: '#0866c6',
        fillOpacity: 0.35
    });

    polyline.setMap(map);

    linesArray.push(polyline);
}

function TrafficToggleButton() {

    var controlDiv = document.createElement('DIV');
    $(controlDiv).addClass('gmap-control-container')
                 .addClass('gmnoprint');

    var controlUI = document.createElement('DIV');
    $(controlUI).addClass('gmap-control');
    $(controlUI).text('Tráfico');
    $(controlDiv).append(controlUI);

    var legend = '<ul>'
               + '<li><span style="background-color: #30ac3e">&nbsp;&nbsp;</span><span style="color: #30ac3e"> &gt; 80 km por hora</span></li>'
               + '<li><span style="background-color: #ffcf00">&nbsp;&nbsp;</span><span style="color: #ffcf00"> 40 - 80 km por hora</span></li>'
               + '<li><span style="background-color: #ff0000">&nbsp;&nbsp;</span><span style="color: #ff0000"> &lt; 40 km por hora</span></li>'
               + '<li><span style="background-color: #c0c0c0">&nbsp;&nbsp;</span><span style="color: #c0c0c0"> Sin información</span></li>'
               + '</ul>';

    var controlLegend = document.createElement('DIV');
    $(controlLegend).addClass('gmap-control-legend');
    $(controlLegend).html(legend);
    $(controlLegend).hide();
    $(controlDiv).append(controlLegend);

    // Set hover toggle event
    $(controlUI)
        .mouseenter(function () {
            $(controlLegend).show();
        })
        .mouseleave(function () {
            $(controlLegend).hide();
        });

    var trafficLayer = new google.maps.TrafficLayer();

    google.maps.event.addDomListener(controlUI, 'click', function () {
        if (typeof trafficLayer.getMap() == 'undefined' || trafficLayer.getMap() === null) {
            $(controlUI).addClass('gmap-control-active');
            trafficLayer.setMap(map);
        } else {
            trafficLayer.setMap(null);
            $(controlUI).removeClass('gmap-control-active');
        }
    });

    map.controls[google.maps.ControlPosition.TOP_RIGHT].push(controlDiv);
}

function AddBotonesAvanzado(velocidad, temperatura, actividad) {

    //Velocidad
    if (velocidad) {
        if (controlVelocidad === null || controlVelocidad == undefined) {
            controlVelocidad = document.createElement('DIV');
            $(controlVelocidad).addClass('gmap-control-container')
                .addClass('gmnoprint');

            var controlVelocidadUI = document.createElement('DIV');
            $(controlVelocidadUI).addClass('gmap-control');
            $(controlVelocidadUI).text('Velocidad');
            $(controlVelocidad).append(controlVelocidadUI);

            google.maps.event.addDomListener(controlVelocidadUI, 'click', function () {
                openPopUpGrupo('Graficas/velocidad.aspx');
            });
            map.controls[google.maps.ControlPosition.TOP_CENTER].push(controlVelocidad);
        } else {
            $(controlVelocidad).show();
        }
    }

    //Temperatura
    if (temperatura) {
        if (controlTemperatura === null || controlTemperatura == undefined) {
            controlTemperatura = document.createElement('DIV');
            $(controlTemperatura).addClass('gmap-control-container')
                                 .addClass('gmnoprint');

            var controlTemperaturaUI = document.createElement('DIV');
            $(controlTemperaturaUI).addClass('gmap-control');
            $(controlTemperaturaUI).text('Temperatura');
            $(controlTemperatura).append(controlTemperaturaUI);

            google.maps.event.addDomListener(controlTemperaturaUI, 'click', function () {
                openPopUpGrupo('Graficas/temperatura.aspx');
            });

            map.controls[google.maps.ControlPosition.TOP_CENTER].push(controlTemperatura);
        } else {
            $(controlTemperatura).show();
        }
    }

    //Actividad
    if (actividad) {
        if (controlActividad === null || controlActividad == undefined) {
            controlActividad = document.createElement('DIV');
            $(controlActividad).addClass('gmap-control-container')
                               .addClass('gmnoprint');

            var controlActividadUI = document.createElement('DIV');
            $(controlActividadUI).addClass('gmap-control');
            $(controlActividadUI).text('Actividad');
            $(controlActividad).append(controlActividadUI);

            google.maps.event.addDomListener(controlActividadUI, 'click', function () {
                openPopUpGrupo('Graficas/actividad.aspx');
            });

            map.controls[google.maps.ControlPosition.TOP_CENTER].push(controlActividad);
        } else {
            $(controlActividad).show();
        }
    }
}

function RemoveBotonesAvanzado() {
    if (controlVelocidad !== null || controlVelocidad != undefined) {
        $(controlVelocidad).hide();
    }
    if (controlTemperatura !== null || controlTemperatura != undefined) {
        $(controlTemperatura).hide();
    }
    if (controlActividad !== null || controlActividad != undefined) {
        $(controlActividad).hide();
    }
}

function updateMapClustered() {
    markerClusterer = new MarkerClusterer(map, markersArray);
}

function CloseInfo(idVehiculo) {
    $.each(locations, function (i, location) {

        if (location.idVehiculo == idVehiculo) {
            infowindow.close();
        }
    });
}