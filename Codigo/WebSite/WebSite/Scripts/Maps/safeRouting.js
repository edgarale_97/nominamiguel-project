﻿var map, ren, ser;
var directionsService = new google.maps.DirectionsService();
var drawing_manager = null;
var polygon_zone = null;

function initializeSafeRouting() {

    var mapOptions = {
        zoom: 12,
        mapTypeId: window.google.maps.MapTypeId.ROADMAP,
        center: new window.google.maps.LatLng(19.432641, -99.13326),
        mapTypeControlOptions:
        {
            style: window.google.maps.MapTypeControlStyle.DROPDOWN_MENU,
            poistion: window.google.maps.ControlPosition.TOP_RIGHT,
            mapTypeIds: [window.google.maps.MapTypeId.ROADMAP,
                window.google.maps.MapTypeId.TERRAIN,
                window.google.maps.MapTypeId.HYBRID,
                window.google.maps.MapTypeId.SATELLITE]
        }
    };

    map = new google.maps.Map(document.getElementById('mapContainer'), mapOptions);

    ren = new google.maps.DirectionsRenderer({ 'draggable': true });
    ren.setMap(map);
    //ren.setPanel(document.getElementById('directions-panel'));

    ser = new google.maps.DirectionsService();

    var input = (document.getElementById('pac-input'));
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    var autocomplete = new google.maps.places.Autocomplete(input);

    google.maps.event.addListener(autocomplete, 'place_changed', function () {

        var place = autocomplete.getPlace();

        if (!place.geometry) {
            return;
        }

        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
        }

        // Validamos que almenos 1 vehiculo se encuentre seleccionado para tomarlo como referencia de inicio.
        if (locations == null || locations.length == 0) {
            mensajeAlerta('Debe seleccionar un vehículo');
            autocomplete.set('place', void (0));
            return;
        } else {
            ser.route(
                {
                    'origin': new window.google.maps.LatLng(locations[0].latitud, locations[0].longitud),
                    'destination': place.geometry.location,
                    'travelMode': google.maps.DirectionsTravelMode.DRIVING
                },
                function (res, sts) {
                    if (sts == 'OK') {
                        ren.setDirections(res);
                    }
                });
        }
    });
    
    trafficLayer = new google.maps.TrafficLayer();
    TrafficToggleButton();
    
    infowindow = new window.google.maps.InfoWindow();
    
    drawing_manager = new google.maps.drawing.DrawingManager({
        //drawingMode: google.maps.drawing.OverlayType.POLYGON,
        drawingControl: true,
        drawingControlOptions: {
            position: google.maps.ControlPosition.TOP_CENTER,
            drawingModes: [
              google.maps.drawing.OverlayType.CIRCLE,
              google.maps.drawing.OverlayType.POLYGON
            ]
        },
        polygonOptions: {
            clickable: true,
            editable: false,
            zIndex: 1
        }
    });

    drawing_manager.setMap(map); // add DrawingManager to map
}

function guardarSafeRouting() {
    
    if (ren.directions.routes.length > 0) {
        
        coordinates = getJSONSafeRouteCoordinates();
        jQuery('#coordinates').val(coordinates);

        __doPostBack('SaveLink', '');
        return true;
    } 

    mensajeAlerta("Debe ingresar una ruta.");
    return false;
}

function getJSONSafeRouteCoordinates() {

    var coordinatesArray = [];

    var route_coordinate = new RouteCoordinate();
       
    var routePathArray = ren.directions.routes[0].overview_path;
    
    var coordinates = [];

    jQuery.each(routePathArray, function (index, value) {
                var coordinate = new Coordinate();
                coordinate.lat = value.lat();
                coordinate.lng = value.lng();
                coordinates.push(coordinate);
    });

    route_coordinate.coordinates = coordinates;
       
    coordinatesArray.push(route_coordinate);
   
    var serializedstring = JSON.stringify(coordinatesArray);

    // Set distance value
    jQuery('#DistanciaHidden').val(ren.directions.routes[0].legs[0].distance.value);
    //alert(ren.directions.routes[0].legs[0].distance.text);

    return serializedstring;
}

function cleanSafeRouting() {
    
    //ren.setDirections({ routes: [] });
    
    if (ren != null) {
        ren.setMap(null);
        ren = null;
        
        ren = new google.maps.DirectionsRenderer({ 'draggable': true });
        ren.setMap(map);
    }
}

function drawRouting(id, readonly) {

    var obj;

    var valor = jQuery('#' + id).val();
    obj = jQuery.parseJSON(valor);

    cleanDraws(obj.type);

        var path = [];

        jQuery.each(obj.coordinates, function (index, item) {
            if (item) {
                path.push(new google.maps.LatLng(item.lat, item.long));
            }
        });

        polygon_zone = new google.maps.Polygon({
            paths: path,
            //strokeColor: '#ff0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillOpacity: 0.35,
            editable: false
        });

        polygon_zone.setMap(map);
        map.panTo(path[0]);
    
        drawing_manager.setOptions({
            drawingControl: false
        });
}