﻿function initializeLandmark() {
    
    drawing_manager = new google.maps.drawing.DrawingManager({
        //drawingMode: google.maps.drawing.OverlayType.POLYGON,
        drawingControl: true,
        drawingControlOptions: {
            position: google.maps.ControlPosition.TOP_CENTER,
            drawingModes: [
              google.maps.drawing.OverlayType.CIRCLE,
              google.maps.drawing.OverlayType.POLYGON
            ]
        },
        circleOptions: {
            clickable: true,
            editable: true,
            zIndex: 1
        },
        polygonOptions: {
            clickable: true,
            editable: true,
            zIndex: 1
        },
    });

    drawing_manager.setMap(map); // add DrawingManager to map

    google.maps.event.addListener(drawing_manager, 'polygoncomplete',
        function (polygon) { // event handler
            
            polygon.setEditable(true); // allow to edit
            polygon_zone = polygon; // save polygon on map to variable
            drawing_manager.setDrawingMode(null); // disable DrawingManager
            drawing_manager.drawingControl = false;
            
            drawing_manager.setOptions({
                drawingControl: false
            });

            draw_type = 'POLIGONO';
            jQuery("#TipoHidden").val(draw_type);
        });
    google.maps.event.addListener(drawing_manager, 'circlecomplete',
        function (circle) { // event handler
            
            circle.setEditable(true); // allow to edit
            circle_zone = circle; // save circle on map to variable
            drawing_manager.setDrawingMode(null); // disable DrawingManager
            
            drawing_manager.setOptions({
                drawingControl: false
            });
            
            draw_type = 'CIRCUNFERENCIA';
            jQuery("#TipoHidden").val(draw_type);
        });
}

function guardarLandmark() {

    var message = 'No se ha trazado un landmark.';

    if (draw_type == 'CIRCUNFERENCIA') {

        if (!circle_zone) {
            mensajeAlerta(message);
            return false;
        } // circle on map validation

        center = circle_zone.getCenter(); // get circle center
        radius = parseInt(circle_zone.getRadius(), 10);  // get circle radius (must be int)

        jQuery('#center').val(center);
        jQuery('#radius').val(radius);

        __doPostBack('SaveLink', '');
    }
    else if (draw_type == 'POLIGONO') {

        if (!polygon_zone) {
            mensajeAlerta(message);
            return false;
        } // polygon on map validation

        coordinates = polygon_zone.getPath().getArray(); //Las coordenadas se guardan en un array

        jQuery('#coordinates').val(coordinates);

        __doPostBack('SaveLink', '');
    } else {
        mensajeAlerta(message);
        return false;
    }
}


function nuevo() {
    drawing_manager.setOptions({
        drawingControl: true
    });
}

function consulta() {
    drawing_manager.setOptions({
        drawingControl: false
    });
}

function drawLandmark(id, readonly) {

    var obj;

    var valor = jQuery('#' + id).val();
    obj = jQuery.parseJSON(valor);

    cleanDraws(obj.type);

    if (obj.type == 'CIRCUNFERENCIA') {
        var center = new google.maps.LatLng(obj.center.lat, obj.center.long);
        circle_zone = new google.maps.Circle({
            center: center,
            radius: obj.radius,
            //strokeColor: '#ff0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillOpacity: 0.35,
            editable: !readonly
        });

        circle_zone.setMap(map);
        map.panTo(center);
    }

    if (obj.type == 'POLIGONO') {
        var path = [];

        jQuery.each(obj.coordinates, function (index, item) {
            if (item) {
                path.push(new google.maps.LatLng(item.lat, item.long));
            }
        });

        polygon_zone = new google.maps.Polygon({
            paths: path,
            //strokeColor: '#ff0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillOpacity: 0.35,
            editable: !readonly
        });

        polygon_zone.setMap(map);
        map.panTo(path[0]);
    }
    
    if (readonly == false) {
        drawing_manager.setOptions({
            drawingControl: false
        });
    }
}