﻿

$("#buscar").on('click', 'button.boton', function () {
    var palabra = $("#palabra").val();

    var mvcGridPageData = siteLocation + "Servicios/ListService?palabra=" + palabra;
    pageGrids.serviciosGrid.ajaxify({
        getPagedData: mvcGridPageData,
        getData: mvcGridPageData
    });
    pageGrids.serviciosGrid.onGridLoaded(function () {
    });
    pageGrids.serviciosGrid.onRowSelect(function (e) {
    });

    pageGrids.serviciosGrid.updateGrid();

});




GridMvcAjax = (function (my, $) {
    my.init = function () {
        $(function () {
            $.ajaxSetup({
                cache: false
            });

            var palabra = $("#palabra").val();

            var mvcGridPageData = siteLocation + "Servicios/ListService?palabra=" + palabra;
            pageGrids.serviciosGrid.ajaxify({
                getPagedData: mvcGridPageData,
                getData: mvcGridPageData
            });
            pageGrids.serviciosGrid.onGridLoaded(function () {
            });
            pageGrids.serviciosGrid.onRowSelect(function (e) {
            });
        });
    };
    return my;
}({}, jQuery)).init();