﻿var id= $("#id").val();

GridMvcAjax = (function (my, $) {
    my.init = function () {
        $(function () {
            $.ajaxSetup({
                cache: false
            });

            var mvcGridPageData = siteLocation + "Servicios/ListaServiciosProveedores?id="+id;
            pageGrids.mvcGrid.ajaxify({
                getPagedData: mvcGridPageData,
                getData: mvcGridPageData
            });
            pageGrids.mvcGrid.onGridLoaded(function () {
               
            });
            pageGrids.mvcGrid.onRowSelect(function (e) {

            });
        });
    };
    return my;

}({}, jQuery)).init();