﻿


GridMvcAjax = (function (my, $) {
    my.init = function () {
        $(function () {
            $.ajaxSetup({
                cache: false
            });
            

           // var mvcGridPageData =  "Proveedores/ListaProveedores?tipo="+ti;
            var mvcGridPageData = siteLocation + "Proveedores/ListaProveedores";
            var mvcGridExcel = siteLocation + 'Proveedores/ProveedoresExcel?tipoReporte=Excel';
            pageGrids.mvcGrid.ajaxify({
                getPagedData: mvcGridPageData,
                getData: mvcGridPageData,
                getExcelData: mvcGridExcel
            });
            pageGrids.mvcGrid.onGridLoaded(function () {
                CargarListeners();
            });
            pageGrids.mvcGrid.onRowSelect(function (e) {

            });
        });
    };
    return my;

}({}, jQuery)).init();



function CargarListeners() {
    $(".desactivarProveedor").click(function () {
        $("#txtAction").val("desactivar");
        $("#txtIdUser").val($(this).attr('data'));
        $("#myModalTitle").html("Desactivar Proveedor");
        $("#mensaje").html("¿Desea Desactivar el Proveedor?");
        $("#myModal2").modal('show');
    });

    $(".activarProveedor").click(function () {
        $("#txtAction").val("activar");
        $("#txtIdUser").val($(this).attr('data'));
        $("#myModalTitle").html("Activar Proveedor");
        $("#mensaje").html("¿Desea activar el Proveedor?");
        $("#myModal2").modal('show');
    });

}

$(document).ready(function () {
    
    $('#btnExcel').click(function (e) {
        e.preventDefault();
        pageGrids.mvcGrid.excelGrid();
        return false;
    });

    pageGrids.mvcGrid.updateGrid();
    $("#btnActivarDesactivar").click(function () {
        $('#myModal2').modal('toggle');
        var action = $("#txtAction").val();
        if (action == "activar") {
            $.blockUI();
            var id = $("#txtIdUser").val();
            var postUrl = siteLocation + "Proveedores/ActivarProveedor";
            var params = {
                idproveedores: id
            };
            $.post(postUrl,
                params,
                function (res) {
                    if (res.Error) {
                        toastr['error'](res.Error, "Toolboy");
                        $.unblockUI();
                        return;
                    }
                    toastr['success']("El proveedor se activo exitosamente.", "Toolboy");
                    $.unblockUI();
                    pageGrids.mvcGrid.updateGrid();
                },
                'json');
        } else {
            $.blockUI();
            var id = $("#txtIdUser").val();
            var postUrl = siteLocation + "Proveedores/DesactivarProveedor";
            var params = {
                idproveedor: id
            };
           
            $.post(postUrl, params, function (res) {
                if (res.Error) {
                    toastr['error'](res.Error, "Toolboy");
                    $.unblockUI();
                    return;
                }
                toastr['success']("El proveedor se desactivo exitosamente.", "Toolboy");
                $.unblockUI();
                pageGrids.mvcGrid.updateGrid();
            }, 'json');
        }
    });
});