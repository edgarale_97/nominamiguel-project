﻿
GridMvcAjax = (function (my, $) {
    my.init = function () {
        $(function () {
            $.ajaxSetup({
                cache: false
            });

            var mvcGridPageData = siteLocation + "Alertas/ListaAlertas";
            pageGrids.mvcGrid.ajaxify({
                getPagedData: mvcGridPageData,
                getData: mvcGridPageData
            });
            pageGrids.mvcGrid.onGridLoaded(function () {
              
            });
            pageGrids.mvcGrid.onRowSelect(function (e) {

            });
           
        });
    };
    return my;

}({}, jQuery)).init();


$(function () {
    $(".atender").click(function () {

        $("#txtIdAlerta").val($(this).attr('dat'));
        $("#myModalTitle").html("Atender Alerta");
        $("#mensaje").html("Observaciones");
        $("#myModal2").modal('show');
    });

});



$("#btnAlerta").click(function () {
        $('#myModal2').modal('toggle');
        var obs = $("#txtobservaciones").val();
        var id = $("#txtIdAlerta").val();
        $.blockUI();

        var postUrl = siteLocation + "Alertas/AtenderAlerta";
        var params = {
            idalerta: id,
            observacion: obs
        };
        $.post(postUrl,
            params,
            function (res) {
                if (res.Error) {
                    toastr['error'](res.Error, "Toolboy");
                    $.unblockUI();
                    return;
                }
                toastr['success']("La alerta se atendió exitosamente.", "Toolboy");
                $.unblockUI();
                pageGrids.mvcGrid.updateGrid();
            },
            'json');

    });

   