﻿GridMvcAjax = (function (my, $) {
    my.init = function () {
        $(function () {
            $.ajaxSetup({
                cache: false
            });

            var mvcGridPageData = siteLocation + "Chat/ListaChat";
            pageGrids.mvcGrid.ajaxify({
                getPagedData: mvcGridPageData,
                getData: mvcGridPageData
            });
            pageGrids.mvcGrid.onGridLoaded(function () {
                CargarListeners();
            });
            pageGrids.mvcGrid.onRowSelect(function (e) {

            });
        });
    };
    return my;

}({}, jQuery)).init();