﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebSite.Models
{
    /// <summary>
    /// Nombre de la clase: ROL_ROLES
    /// </summary>
    [Table("ROL_ROLES")]
    public class Rol
    {
        /// <summary>
        /// Codigo 
        /// </summary>
        [Display(Name = "Código")]
        [Key]
        [Column("ROL_CODE")]
        public short Code { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>
        [Display(Name = "Nombre")]
        [Column("ROL_NOMBRE")]
        public string Nombre { get; set; }

        /// <summary>
        /// Visibilidad
        /// </summary>
        [Display(Name = "Activo")]
        [Column("ROL_VISIBLE")]
        public bool Visible { get; set; }

        /// <summary>
        /// Fecha de creacion
        /// </summary>
        [Display(Name = "Creado")]
        [Column("ROL_CREATEDATE")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Fecha de actualizacion
        /// </summary>
        [Display(Name = "Actualizado")]
        [Column("ROL_UPDATEDATE")]
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// Usuario que crea
        /// </summary>
        [Display(Name = "Creado por")]
        [Column("ROL_CREATEUSER")]
        public string CreateUser { get; set; }

        /// <summary>
        /// Usuario que actualiza
        /// </summary>
        [Display(Name = "Actualizado por")]
        [Column("ROL_UPDATEUSER")]
        public string UpdateUser { get; set; }
    }
}