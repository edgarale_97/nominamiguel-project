﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebSite.Models
{
    /// <summary>
    /// Nombre de la clase: Canton
    /// </summary>
    [Table("CAN_CANTONES")]
    public class Canton
    {
        /// <summary>
        /// Codigo del canton
        /// </summary>
        [Display(Name = "Cantón")]
        [Key]
        [Column("CAN_CODE")]
        public short Code { get; set; }

        /// <summary>
        /// Codigo de la provincia
        /// </summary>
        [Display(Name = "Provincia")]
        [Required]
        [Column("PROV_CODE")]
        public short ProvinciaCode { get; set; }

        /// <summary>
        /// Nombre del canton
        /// </summary>
        [Display(Name = "Nombre")]
        [StringLength(30)]
        [Required]
        [Column("CAN_NAME")]
        public string Nombre { get; set; }

        /// <summary>
        /// Descripcion del canton
        /// </summary>
        [Display(Name = "Descripción")]
        [StringLength(100)]
        [Column("CAN_DESCRIPCION")]
        public string Descripcion { get; set; }

        /// <summary>
        /// Visibilidad del canton
        /// </summary>
        [Display(Name = "Activo")]
        [Column("CAN_VISIBLE")]
        public bool Visible { get; set; }

        /// <summary>
        /// Fecha de creacion del registro
        /// </summary>
        [Display(Name = "Creado")]
        [Required]
        [Column("CAN_CREATEDATE")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Usuario que crea
        /// </summary>
        [Display(Name = "Creado Por")]
        [Required]
        [StringLength(20)]
        [Column("CAN_CREATEUSER")]
        public string CreateUser { get; set; }

        /// <summary>
        /// Fecha de actualizacion del registro
        /// </summary>
        [Display(Name = "Actualizado")]
        [Column("CAN_UPDATEDATE")]
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// Usuario que actualiza
        /// </summary>
        [Display(Name = "Actualizado Por")]
        [StringLength(20)]
        [Column("CAN_UPDATEUSER")]
        public string UpdateUser { get; set; }
    }
}