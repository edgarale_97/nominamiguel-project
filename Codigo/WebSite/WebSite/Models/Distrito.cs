﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebSite.Models
{
    /// <summary>
    /// Nombre de la clase: Distrito
    /// </summary>
    [Table("DIS_DISTRITOS")]
    public class Distrito
    {
        /// <summary>
        /// Codigo del distrito
        /// </summary>
        [Display(Name = "Distrito")]
        [Key]
        [Column("DIS_CODE")]
        public short DistritoCode { get; set; }

        /// <summary>
        /// Codigo de la provincia
        /// </summary>
        [Display(Name = "Provincia")]
        [Required]
        [Column("PROV_CODE")]
        public short ProvinciaCode { get; set; }

        /// <summary>
        /// Codigo del canton
        /// </summary>
        [Display(Name = "Cantón")]
        [Required]
        [Column("CAN_CODE")]
        public short CantonCode { get; set; }

        /// <summary>
        /// Nombre 
        /// </summary>
        [Display(Name = "Nombre")]
        [Required]
        [StringLength(30)]
        [Column("DIS_NAME")]
        public string Nombre { get; set; }

        /// <summary>
        /// Descripcion 
        /// </summary>
        [Display(Name = "Descripción")]
        [StringLength(100)]
        [Column("DIS_DESCRIPCION")]
        public string Descripcion { get; set; }

        /// <summary>
        /// Visible
        /// </summary>
        [Display(Name = "Activo")]
        [Column("DIS_VISIBLE")]
        public bool Visible { get; set; }

        /// <summary>
        /// Fecha de creacion
        /// </summary>
        [Display(Name = "Creado")]
        [Required]
        [Column("DIS_CREATEDATE")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Usuario que crea
        /// </summary>
        [Display(Name = "Creado Por")]
        [Required]
        [StringLength(20)]
        [Column("DIS_CREATEUSER")]
        public string CreateUser { get; set; }

        /// <summary>
        /// Fecha de actualizacion
        /// </summary>
        [Display(Name = "Actualizado")]
        [Column("DIS_UPDATEDATE")]
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// Usuario que actualiza
        /// </summary>
        [Display(Name = "Actualizado Por")]
        [StringLength(20)]
        [Column("DIS_UPDATEUSER")]
        public string UpdateUser { get; set; }
    }
}