﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebSite.Models
{
    /// <summary>
    /// Nombre de la clase: FichaTecnica
    /// </summary>
    [Table("FITA_FICHAS_TECNICAS_ANODIZADOS")]
    public class FichaTecnica
    {
        /// <summary>
        /// Codigo
        /// </summary>
        [Display(Name = "Código")]
        [Key]
        [Column("FITA_CODE")]
        public int Code { get; set; }

        /// <summary>
        /// Cliente
        /// </summary>
        [Display(Name = "Cliente")]
        [Required]
        [Column("CLI_CODE")]
        public int ClienteCode { get; set; }

        /// <summary>
        /// Codigo que define el usuario
        /// </summary>
        [Display(Name = "Ficha")]
        [Column("FITA_CODIGO")]
        public string Codigo { get; set; }

        /// <summary>
        /// Codigo del tipo de tratamiento
        /// </summary>
        [Display(Name = "Tratamiento")]
        [Column("TITRA_CODE")]
        public int TipoTratamientoCode { get; set; }

        /// <summary>
        /// Codigo que define el usuario
        /// </summary>
        [Display(Name = "Revisión Plano")]
        [Column("FITA_REVISIONPLANO")]
        public string RevisionPlano { get; set; }

        /// <summary>
        /// Codigo del tipo de material
        /// </summary>
        [Display(Name = "Material")]
        [Column("TIMA_CODE")]
        public int TipoMaterialCode { get; set; }

        /// <summary>
        /// Medidas criticas de la inspeccion de recibido
        /// </summary>
        [Display(Name = "Medidas Críticas")]
        [Column("FITA_RECIBO_MEDIDAS_CRITICAS")]
        public string ReciboMedidaCriticas { get; set; }

        /// <summary>
        /// Recomendacion de raqueo
        /// </summary>
        [Display(Name = "Recomendación")]
        [Column("FITA_RAQUEO_RECOMENDACION")]
        public string RaqueoRecomendacion { get; set; }

        /// <summary>
        /// Tiempo de desangre
        /// </summary>
        [Display(Name = "Tiempo de desengrase")]
        [Column("FITA_TIEMPO_DESENGRASE")]
        public decimal TiempoDesengrase { get; set; }

        /// <summary>
        /// Tiempo de etching
        /// </summary>
        [Display(Name = "Tiempo Etching")]
        [Column("FITA_TIEMPO_ETCHING")]
        public decimal TiempoEtching { get; set; }

        /// <summary>
        /// amperaje o voltaje
        /// </summary>
        [Display(Name = "Amperaje o voltaje")]
        [Column("FITA_AMPERAJE_VOLTAJE")]
        public decimal AmperajeVoltaje { get; set; }

        /// <summary>
        /// Tiempo de anodizado
        /// </summary>
        [Display(Name = "Tiempo o voltaje")]
        [Column("FITA_TIEMPO_ANODIZADO")]
        public decimal TiempoAnodizado { get; set; }

        /// <summary>
        /// Tiempo de inmersion tinta
        /// </summary>
        [Display(Name = "Tiempo inmersión tinta")]
        [Column("FITA_TIEMPO_TINTA")]
        public decimal TiempoTinta { get; set; }

        /// <summary>
        /// Tiempo de inmersion sello
        /// </summary>
        [Display(Name = "Tiempo inmersión sello")]
        [Column("FITA_TIEMPO_SELLO")]
        public decimal TiempoSello { get; set; }

        /// <summary>
        /// Cuidados especiales
        /// </summary>
        [Display(Name = "Cuidados especiales")]
        [Column("FITA_CUIDADOS_ESPECIALES")]
        public string CuidadosEspeciales { get; set; }

        /// <summary>
        /// Otros
        /// </summary>
        [Display(Name = "Otros")]
        [Column("FITA_OTROS")]
        public string Otros { get; set; }

        /// <summary>
        /// Criterios de aceptacion de la salida
        /// </summary>
        [Display(Name = "Criterios de aceptación")]
        [Column("FITA_SALIDA_CRITERIOS_ACEPTACION")]
        public string SalidaCriteriosAceptacion { get; set; }

        /// <summary>
        /// Recomendacion de empaque
        /// </summary>
        [Display(Name = "Recomendación")]
        [Column("FITA_EMPAQUE_RECOMENDACION")]
        public string EmpaqueRecomendacion { get; set; }

        /// <summary>
        /// Fecha de creacion
        /// </summary>
        [Display(Name = "Creado")]
        [Required]
        [Column("FITA_CREATEDATE")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Usuario que crea
        /// </summary>
        [Display(Name = "Creado Por")]
        [Required]
        [Column("FITA_CREATEUSER")]
        public string CreateUser { get; set; }

        /// <summary>
        /// Fecha de actualizacion
        /// </summary>
        [Display(Name = "Actualizado")]
        [Column("FITA_UPDATEDATE")]
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// Usuario que actualiza
        /// </summary>
        [Display(Name = "Actualizado Por")]         
        [Column("FITA_UPDATEUSER")]
        public string UpdateUser { get; set; }

        /// <summary>
        /// Listado de fotos de recibo
        /// </summary>
        public virtual List<FotoRecibo> FotosRecibo { get; set; }

        /// <summary>
        /// Listado de fotos de raqueo
        /// </summary>
        public virtual List<FotoRaqueo> FotosRaqueo { get; set; }

        /// <summary>
        /// Listado de fotos de empaque
        /// </summary>
        public virtual List<FotoEmpaque> FotosEmpaque { get; set; }

        /// <summary>
        /// Listado de fotos de salida
        /// </summary>
        public virtual List<FotoSalida> FotosSalida { get; set; }
    }
}