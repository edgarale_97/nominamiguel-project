﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebSite.Models
{
    /// <summary>
    /// Nombre de la clase: FotoRaqueo
    /// </summary>
    [Table("FORE_FOTOS_RECIBO_FICHA")]
    public class FotoRaqueo
    {
        /// <summary>
        /// Copdigo
        /// </summary>
        [Display(Name = "Código")]
        [Column("FORA_CODE")]
        public int Codigo { get; set; }

        /// <summary>
        /// Codigo de la ficha tecnica
        /// </summary>
        [Display(Name = "Ficha")]
        [Column("FITA_CODE")]
        public int FichaCodigo { get; set; }

        /// <summary>
        /// Archivo en base 64
        /// </summary>
        [Display(Name = "Archivo")]
        [Column("FORA_ARCHIVO_BASE64")]
        public string ArchivoBase64 { get; set; }

        /// <summary>
        /// Nombre del archivo
        /// </summary>
        [Display(Name = "Nombre")]
        [Column("FORA_ARCHIVO_NOMBRE")]
        public string ArchivoNombre { get; set; }

        /// <summary>
        /// Tipo de archivo
        /// </summary>
        [Display(Name = "Formato")]
        [Column("FORA_ARCHIVO_TYPE")]
        public string ArchivoType { get; set; }

        /// <summary>
        /// Fecha de creacion
        /// </summary>
        [Display(Name = "Creado")]
        [Column("FORA_CREATEDATE")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Usuario que crea
        /// </summary>
        [Display(Name = "Creado por")]
        [Column("FORA_CREATEUSER")]
        public string CreateUser { get; set; }

        /// <summary>
        /// Fecha en que actualizan
        /// </summary>
        [Display(Name = "Actualizado")]
        [Column("FORA_UPDATEDATE")]
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// Usuario que actualiza
        /// </summary>
        [Display(Name = "Actualizado por")]
        [Column("FORA_UPDATEUSER")]
        public string UpdateUser { get; set; }
    }
}