﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebSite.Models
{
    /// <summary>
    /// Nombre de la clase: TipoMaterial
    /// </summary>
    public class TipoMaterial
    {
        /// <summary>
        /// Codigo autoincremental
        /// </summary>
        [Display(Name = "Código")]
        [Key]
        [Column("TIMA_CODE")]
        public int Code { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>
        [Display(Name = "Nombre")]
        [Column("ACCSA_NOMBRE")]
        public string Nombre { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>
        [Display(Name = "Descripción")]
        [Column("TIMA_DESCRIPCION")]
        public string Descripcion { get; set; }

        /// <summary>
        /// Indica si es visible
        /// </summary>
        [Display(Name = "Visible")]
        [Column("TIMA_VISIBLE")]
        public bool Visible { get; set; }

        /// <summary>
        /// Fecha de creacion
        /// </summary>
        [Display(Name = "Creación")]
        [Column("TIMA_CREATEDATE")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Usuario que crea
        /// </summary>
        [Display(Name = "Creado por")]
        [Column("TIMA_CREATEUSER")]
        public string CreateUser { get; set; }

        /// <summary>
        /// Fecha de actualizacion
        /// </summary>
        [Display(Name = "Actualización")]
        [Column("TIMA_UPDATEDATE")]
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// Usuario que actualiza
        /// </summary>
        [Display(Name = "Actualizado por")]
        [Column("TIMA_UPDATEUSER")]
        public string UpdateUser { get; set; }
    }
}