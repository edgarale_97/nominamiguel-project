﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Net.Http;
using Business;
using Newtonsoft.Json;

namespace WebSite.Models
{

    /// <summary>
    /// Nombre de la clase: Productos
    /// </summary>
    [Table("PROD_PRODUCTOQUIMICO")]
    public class Productos
    {

        private Api api;
        private string ApiUrl;
        public Productos()
        {
            api = new Api();
            ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
        }

        /// <summary>
        /// Codigo
        /// </summary>
        [Display(Name = "Código")]
        [Key]
        [Column("PROD_CODE")]
        public short Code { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>
        [Display(Name = "Nombre")]
        [Column("PROD_NAME")]
        public string Nombre { get; set; }

        /// <summary>
        /// Fabricante
        /// </summary>
        [Display(Name = "Fabricante")]
        [Column("PROD_FABRICANTE")]
        public string Fabricante { get; set; }

        /// <summary>
        /// Proveedor
        /// </summary>
        [Display(Name = "Proveedor")]
        [Column("PROD_PROVEEDOR")]
        public string Proveedor { get; set; }

        /// <summary>
        /// Codigo Interno del producto
        /// </summary>
        [Display(Name = "Código interno del producto")]
        [Column("PROD_CODINTPRODUCTO")]
        public string CodIntProducto { get; set; }

        /// <summary>
        /// Nro. de registro químico
        /// </summary>
        [Display(Name = "Nro. de registro químico")]
        [Column("PROD_NROREGQUIMICO")]
        public string NroRegQuimico { get; set; }

        /// <summary>
        /// Precursor
        /// </summary>
        [Display(Name = "Precursor")]
        [Column("PROD_PRECURSOR")]
        public bool Precursor { get; set; }

        /// <summary>
        /// Hojas de seguridad
        /// </summary>
        [Display(Name = "Hojas de seguridad")]
        [Column("PROD_HOJASEGURIDAD")]
        public string HojaSeguridad { get; set; }

        /// <summary>
        /// Peligrosidad
        /// </summary>
        [Display(Name = "Peligrosidad")]
        [Column("PROD_PELIGROSIDAD")]
        public string Peligrosidad { get; set; }

        /// <summary>
        /// Permiso de venta
        /// </summary>
        [Display(Name = "Permiso de venta")]
        [Column("PROD_PERMISOVENTA")]
        public string PermisoVenta { get; set; }

        /// <summary>
        /// Permiso de consumo
        /// </summary>
        [Display(Name = "Permiso de consumo")]
        [Column("PROD_PERMISOCONSUMO")]
        public string PermisoConsumo { get; set; }

        /// <summary>
        /// Material
        /// </summary>
        [Display(Name = "Material")]
        [Column("PROD_MATERIAL")]
        public string Material { get; set; }

        /// <summary>
        /// Volumen
        /// </summary>
        [Display(Name = "Volumen")]
        [Column("PROD_VOLUMEN")]
        public string Volumen { get; set; }

        /// <summary>
        /// Densidad
        /// </summary>
        [Display(Name = "Densidad")]
        [Column("PROD_DENSIDAD")]
        public string Densidad { get; set; }

        /// <summary>
        /// Tipo de residuo
        /// </summary>
        [Display(Name = "Tipo de residuo")]
        [Column("PROD_TIPORESIDUO")]
        public string TipoResiduo { get; set; }

        /// <summary>
        /// Gestor autorizado
        /// </summary>
        [Display(Name = "Gestor autorizado")]
        [Column("PROD_GESTORAUTH")]
        public string Gestor { get; set; }

        /// <summary>
        /// Fecha de creacion
        /// </summary>
        [Display(Name = "Fecha de creación")]
        [Column("PROD_CREATEDATE")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Fecha de actualizacion
        /// </summary>
        [Display(Name = "Fecha de actualización")]
        [Column("PROD_UPDATEDATE")]
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// Usuario que crea
        /// </summary>
        [Display(Name = "Creado por")]
        [Column("PROD_CREATEUSER")]
        public int CreateUser { get; set; }

        /// <summary>
        /// Usuario que actualiza
        /// </summary>
        [Display(Name = "Actualizado por")]
        [Column("PROD_UPDATEUSER")]
        public int UpdateUser { get; set; }

        /// <summary>
        /// Usuario que crea
        /// </summary>
        [Display(Name = "Creado por")]
        [Column("PROD_CREATEUSER")]
        public string CreateUserName
        {
            get
            {
                return GetUserName(CreateUser);

            }

            set
            {
                this.CreateUserName = value;
            }
        }

        /// <summary>
        /// Usuario que actualiza
        /// </summary>
        [Display(Name = "Actualizado por")]
        [Column("PROD_UPDATEUSER")]
        public string UpdateUserName
        {
            get
            {
                return GetUserName(UpdateUser);
            }
            set { this.UpdateUserName = value; }
        }

        public string GetUserName(int userCode)
        {
            Usuario usuario = null;
            if (userCode != 0)
            {
                try
                {
                    var jsonUsuario = api.Get(Convert.ToInt32(userCode), ApiUrl + "Usuarios/");
                    usuario = JsonConvert.DeserializeObject<Usuario>(jsonUsuario);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                return usuario.Nombre;
            }
            else
            {
                return "Ninguno";
            }

        }

    }
}