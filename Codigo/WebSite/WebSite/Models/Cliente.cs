﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebSite.Models
{
    /// <summary>
    /// Nombre del cliente: Cliente
    /// </summary>
    public class Cliente
    {
        /// <summary>
        /// Codigo del cliente
        /// </summary>
        [Display(Name = "Código")]
        [Key]
        [Column("CLI_CODE")]
        public int Code { get; set; }

        /// <summary>
        /// Codigo de la empresa
        /// </summary>
        [Display(Name = "Empresa")]
        [Required]
        [Column("EMP_CODE")]
        public short EmpresaCode { get; set; }

        /// <summary>
        /// Cedula
        /// </summary>
        [Display(Name = "Cédula")]
        [Required]
        [StringLength(20)]
        [Column("CLI_CEDULA")]
        public string Cedula { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>
        [Display(Name = "Nombre")]
        [Required]
        [StringLength(100)]
        [Column("CLI_NAME")]
        public string Nombre { get; set; }

        /// <summary>
        /// Razon social
        /// </summary>
        [Display(Name = "Razón Social")]
        [Required]
        [StringLength(100)]
        [Column("CLI_RAZONSOCIAL")]
        public string RazonSocial { get; set; }

        /// <summary>
        /// Pais
        /// </summary>
        [Display(Name = "País")]
        [Required]
        [Column("PAI_CODE")]
        public short PaisCode { get; set; }

        /// <summary>
        /// Provincia
        /// </summary>
        [Display(Name = "Provincia")]
        [Column("PROV_CODE")]
        public short ProvinciaCode { get; set; }

        /// <summary>
        /// Canton
        /// </summary>
        [Display(Name = "Cantón")]
        [Column("CAN_CODE")]
        public short CantonCode { get; set; }

        /// <summary>
        /// Distrito
        /// </summary>
        [Display(Name = "Distrito")]
        [Column("DIS_CODE")]
        public short DistritoCode { get; set; }

        /// <summary>
        /// Codigo postal
        /// </summary>
        [Display(Name = "Código Postal")]
        [StringLength(15)]
        [Column("CLI_CODIGO_POSTAL")]
        public string CodigoPostal { get; set; }

        /// <summary>
        /// Direccion 1
        /// </summary>
        [Display(Name = "Dirección 1")]
        [Required]
        [StringLength(500)]
        [Column("CLI_DIRECCION1")]
        public string Direccion1 { get; set; }

        /// <summary>
        /// Direccion 2
        /// </summary>
        [Display(Name = "Dirección 2")]
        [StringLength(500)]
        [Column("CLI_DIRECCION2")]
        public string Direccion2 { get; set; }

        /// <summary>
        /// Telefono 1
        /// </summary>
        [Display(Name = "Teléfono 1")]
        [Required]
        [StringLength(15)]
        [Column("CLI_TELEFONO1")]
        public string Telefono1 { get; set; }

        /// <summary>
        /// Telefono 2
        /// </summary>
        [Display(Name = "Teléfono 2")]
        [StringLength(15)]
        [Column("CLI_TELEFONO2")]
        public string Telefono2 { get; set; }

        /// <summary>
        /// Fax
        /// </summary>
        [Display(Name = "Fax")]
        [StringLength(15)]
        [Column("CLI_FAX")]
        public string Fax { get; set; }

        /// <summary>
        /// Correo
        /// </summary>
        [Display(Name = "Email")]
        [Required]
        [StringLength(70)]
        [Column("CLI_EMAIL")]
        public string Email { get; set; }

        /// <summary>
        /// Direccion del website
        /// </summary>
        [Display(Name = "Web")]
        [StringLength(100)]
        [Column("CLI_WEB")]
        public string Web { get; set; }

        /// <summary>
        /// Descripcion
        /// </summary>
        [Display(Name = "Descripción")]
        [StringLength(500)]
        [Column("CLI_DESCRIPCION")]
        public string Descripcion { get; set; }


        /// <summary>
        /// Visibilidad
        /// </summary>
        [Display(Name = "Activo")]
        [Column("CLI_VISIBLE")]
        public bool Visible { get; set; }

        /// <summary>
        /// Fecha de creacion
        /// </summary>
        [Display(Name = "Creado")]
        [Required]
        [Column("CLI_CREATEDATE")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Usuario que crea
        /// </summary>
        [Display(Name = "Creado Por")]
        [Required]
        [Column("CLI_CREATEUSER")]
        public string CreateUser { get; set; }

        /// <summary>
        /// Fecha de actualizacion
        /// </summary>
        [Display(Name = "Actualizado")]
        [Column("CLI_UPDATEDATE")]
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// Usuario que actualiza
        /// </summary>
        [Display(Name = "Actualizado Por")]
        [Column("CLI_UPDATEUSER")]
        public string UpdateUser { get; set; }
    }
}