﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebSite.Models
{
    /// <summary>
    /// Nombre de la clase: RolPermiso
    /// </summary>
    [Table("ROLPER_ROLES_PERMISOS")]
    public class RolPermiso
    {
        /// <summary>
        /// Codigo 
        /// </summary>
        [Display(Name = "Código")]
        [Key]
        [Column("ROLPER_CODE")]
        public string Code { get; set; }

        /// <summary>
        /// Codigo del rol 
        /// </summary>
        [Display(Name = "Rol")]
        [Column("ROL_CODE")]
        public short RolCode { get; set; }

        /// <summary>
        /// Codigo del controlador
        /// </summary>
        [Display(Name = "Controlador")]
        [Column("CON_CODE")]
        public short ControladorCode { get; set; }

        /// <summary>
        /// Accion del controlador
        /// </summary>
        [Display(Name = "Acción")]
        [Column("ACC_CODE")]
        public short AccionCode { get; set; }

        /// <summary>
        /// Visibilidad
        /// </summary>
        [Display(Name = "Activo")]
        [Column("ROLPER_VISIBLE")]
        public bool Visibilidad { get; set; }

        /// <summary>
        /// Fecha de creacion
        /// </summary>
        [Display(Name = "Creado")]
        [Column("ROLPER_CREATEDATE")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Fecha de actualizacion
        /// </summary>
        [Display(Name = "Actualizado")]
        [Column("ROLPER_UPDATEDATE")]
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// Usuario que crea
        /// </summary>
        [Display(Name = "Creado por")]
        [Column("ROLPER_CREATEUSER")]
        public string CreateUser { get; set; }

        /// <summary>
        /// Usuario que actualiza
        /// </summary>
        [Display(Name = "Actualizado por")]
        [Column("ROLPER_UPDATEUSER")]
        public string UpdateUser { get; set; }
    }
}