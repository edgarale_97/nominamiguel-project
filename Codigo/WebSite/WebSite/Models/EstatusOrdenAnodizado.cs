﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebSite.Models
{
    /// <summary>
    /// Nombre de la clase: EstatusOrdenAnodizado
    /// </summary>
    public class EstatusOrdenAnodizado
    {
        /// <summary>
        /// Codigo autoincremental
        /// </summary>
        [Display(Name = "Código")]
        [Key]
        [Column("ESOR_CODE")]
        public int Code { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>
        [Display(Name = "Nombre")]
        [Column("ESOR_NOMBRE")]
        public string Nombre { get; set; }

        /// <summary>
        /// Indica si es visible
        /// </summary>
        [Display(Name = "Activo")]
        [Column("ESOR_VISIBLE")]
        public bool Visible { get; set; }

        /// <summary>
        /// Fecha de creacion
        /// </summary>
        [Display(Name = "Creado")]
        [Column("ESOR_CREATEDATE")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Usuario que crea
        /// </summary>
        [Display(Name = "Creado por")]
        [Column("ESOR_CREATEUSER")]
        public string CreateUser { get; set; }

        /// <summary>
        /// Fecha de actualizacion
        /// </summary>
        [Display(Name = "Actualizado")]
        [Column("ESOR_UPDATEDATE")]
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// Usuario que actualiza
        /// </summary>
        [Display(Name = "Actualizado por")]
        [Column("ESOR_UPDATEUSER")]
        public string UpdateUser { get; set; }
    }
}