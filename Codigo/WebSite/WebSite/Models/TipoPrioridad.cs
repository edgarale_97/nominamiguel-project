﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebSite.Models
{
    /// <summary>
    /// Nombre de la clase: TipoPrioridad
    /// </summary>
    public class TipoPrioridad
    {
        /// <summary>
        /// Codigo autoincremental
        /// </summary>
        [Display(Name = "Código")]
        [Key]
        [Column("TIPRI_CODE")]
        public int Code { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>
        [Display(Name = "Nombre")]
        [Column("TIPRI_NOMBRE")]
        public string Nombre { get; set; }

        /// <summary>
        /// Indica si es visible
        /// </summary>
        [Display(Name = "Visible")]
        [Column("TIPRI_VISIBLE")]
        public bool Visible { get; set; }

        /// <summary>
        /// Fecha de creacion
        /// </summary>
        [Display(Name = "Creación")]
        [Column("TIPRI_CREATEDATE")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Usuario que crea
        /// </summary>
        [Display(Name = "Creado por")]
        [Column("TIPRI_CREATEUSER")]
        public string CreateUser { get; set; }

        /// <summary>
        /// Fecha de actualizacion
        /// </summary>
        [Display(Name = "Actualización")]
        [Column("TIPRI_UPDATEDATE")]
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// Usuario que actualiza
        /// </summary>
        [Display(Name = "Actualizado por")]
        [Column("TIPRI_UPDATEUSER")]
        public string UpdateUser { get; set; }
    }
}