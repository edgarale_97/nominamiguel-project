﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSite.Models
{
	public class UploadedFiles
	{
		public IEnumerable<HttpPostedFileBase> HojasSeguridad { get; set; }
		public HttpPostedFileBase PermisoVenta { get; set; }
		public HttpPostedFileBase PermisoConsumo { get; set; }
		

	}
}