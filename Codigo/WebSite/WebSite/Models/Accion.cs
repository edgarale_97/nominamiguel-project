﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebSite.Models
{
    /// <summary>
    /// Nombre de la clase: Accion
    /// </summary>
    [Table("ACC_ACCIONES")]
    public class Accion
    {
        /// <summary>
        /// Codigo
        /// </summary>
        [Key]
        [Display(Name = "Código")]
        [Column("ACC_CODE")]
        public int Codigo { get; set; }

        /// <summary>
        /// Codigo del controlador
        /// </summary>
        [Column("CON_CODE")]
        [Display(Name = "Controlador")]
        public int CodigoControlador { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>
        [Display(Name = "Nombre")]
        [Column("ACC_NOMBRE")]
        public string Nombre { get; set; }

        /// <summary>
        /// Visible
        /// </summary>
        [Display(Name = "Visible")]
        [Column("ACC_VISIBLE")]
        public bool Visible { get; set; }

        /// <summary>
        /// Fecha de creacion
        /// </summary>
        [Display(Name = "Creado")]
        [Column("ACC_CREATEDATE")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Fecha de actualizacion
        /// </summary>
        [Column("ACC_UPDATEDATE")]
        [Display(Name = "Acualizado")]
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// Usuario que crea
        /// </summary>
        [Column("ACC_CREATEUSER")]
        [Display(Name = "Creado Por")]
        public string CreateUser { get; set; }

        /// <summary>
        /// Usuario que actualiza
        /// </summary>
        [Display(Name = "Actualizado Por")]
        [Column("ACC_UPDATEUSER")]
        public string UpdateUser { get; set; }
    }
}