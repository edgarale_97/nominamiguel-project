﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebSite.Models
{
    /// <summary>
    /// Nombre de la clase: Pais
    /// </summary>
    [Table("PAI_PAIS")]
    public class Pais
    {
        [Display(Name = "Código")]
        [Key]
        [Column("PAI_CODE")]
        public short Code { get; set; }

        [Display(Name = "ISO")]
        [Column("PAI_CODE_ISO")]
        public short CodeIso { get; set; }

        [Display(Name = "Nombre")]
        [Required]
        [StringLength(30)]
        [Column("PAI_NAME")]
        public string Nombre { get; set; }

        [Display(Name = "Descripción")]
        [StringLength(100)]
        [Column("PAI_DESCRIPCION")]
        public string Descripcion { get; set; }

        [Display(Name = "Activo")]
        [Column("PAI_VISIBLE")]
        public bool Visible { get; set; }

        [Display(Name = "Creado")]
        [Required]
        [Column("PAI_CREATEDATE")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "Creado por")]
        [Required]
        [StringLength(20)]
        [Column("PAI_CREATEUSER")]
        public string CreateUser { get; set; }

        [Display(Name = "Actualizado")]
        [Column("PAI_UPDATEDATE")]
        public DateTime? UpdateDate { get; set; }

        [Display(Name = "Actualizado por")]
        [StringLength(20)]
        [Column("PAI_UPDATEUSER")]
        public string UpdateUser { get; set; }
    }
}