﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebSite.Models
{
    /// <summary>
    /// Nombre de la clase: Controlador
    /// </summary>
    [Table("CON_CONTROLADOR")]
    public class Controlador
    {
        /// <summary>
        /// Codigo
        /// </summary>
        [Display(Name = "Código")]
        [Key]
        [Column("CON_CODE")]
        public int Codigo { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>
        [Display(Name = "Nombre")]
        [Column("CON_NOMBRE")]
        public string Nombre { get; set; }

        /// <summary>
        /// Visibilidad
        /// </summary>
        [Display(Name = "Activo")]
        [Column("CON_VISIBLE")]
        public bool Visibilidad { get; set; }

        /// <summary>
        /// Fecha de creacion
        /// </summary>
        [Display(Name = "Creado")]
        [Column("CON_CREATEDATE")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Fecha de actualizacion
        /// </summary>
        [Display(Name = "Actualizado")]
        [Column("CON_UPDATEDATE")]
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// Usuario que crea
        /// </summary>
        [Display(Name = "Creado por")]
        [Column("CON_CREATEUSER")]
        public string CreateUser { get; set; }

        /// <summary>
        /// Usuario que actualiza
        /// </summary>
        [Display(Name = "Actualizado por")]
        [Column("CON_UPDATEUSER")]
        public string UpdateUser { get; set; }
    }
}