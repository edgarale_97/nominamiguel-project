﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebSite.Models
{
    /// <summary>
    /// Nombre de la clase: Provincia
    /// </summary>
    [Table("PROV_PROVINCIAS")]
    public class Provincia
    {
        /// <summary>
        /// Codigo
        /// </summary>
        [Display(Name = "Provincia")]
        [Key]
        [Column("PROV_CODE")]
        public short Code { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>
        [Display(Name = "Nombre")]
        [Required]
        [StringLength(30)]
        [Column("PROV_NAME")]
        public string Nombre { get; set; }

        /// <summary>
        /// Descripcion
        /// </summary>
        [Display(Name = "Descripción")]
        [StringLength(100)]
        [Column("PROV_DESCRIPCION")]
        public string Descripcion { get; set; }

        /// <summary>
        /// Visible
        /// </summary>
        [Display(Name = "Activo")]
        [Column("PROV_VISIBLE")]
        public bool Visible { get; set; }

        /// <summary>
        /// Fecha de creacion
        /// </summary>
        [Display(Name = "Creado")]
        [Required]
        [Column("PROV_CREATEDATE")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Usuario que crea
        /// </summary>
        [Display(Name = "Creado Por")]
        [Required]
        [StringLength(20)]
        [Column("PROV_CREATEUSER")]
        public string CreateUser { get; set; }

        /// <summary>
        /// Fecha de actualizacion
        /// </summary>
        [Display(Name = "Actualizado")]
        [Column("PROV_UPDATEDATE")]
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// Usuario que actualiza
        /// </summary>
        [Display(Name = "Actualizado por ")]
        [StringLength(20)]
        [Column("PROV_UPDATEUSER")]
        public string UpdateUser { get; set; }
    }
}