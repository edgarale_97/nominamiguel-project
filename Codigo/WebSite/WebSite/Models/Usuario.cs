﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebSite.Models
{
    /// <summary>
    /// Nombre de la clase: Usuario
    /// </summary>
    [Table("USU_USUARIO")]
    public class Usuario
    {
        /// <summary>
        /// Código de tabla USU_USUARIO
        /// </summary>
        [Display(Name = "Código")]
        [Key]
        [Column("USU_CODE")]
        public int Code { get; set; }

        /// <summary>
        /// Nombre de usuario de tabla USU_USUARIO
        /// </summary>
        [Display(Name = "Usuario")]
        [Column("USU_USERNAME")]
        public string NombreUsuario { get; set; }

        /// <summary>
        /// Nombre de usuario de tabla USU_USUARIO
        /// </summary>
        [Display(Name = "Empresa")]
        [Column("EMP_CODE")]
        public string EmpresaCode { get; set; }

        /// <summary>
        /// Rol de tabla USU_USUARIO 
        /// </summary>
        [Display(Name = "Rol")]
        [Column("ROL_CODE")]
        public short RolCode { get; set; }

        /// <summary>
        /// Nombre de tabla USU_USUARIO
        /// </summary>
        [Display(Name = "Nombre")]
        [Column("USU_NOMBRE")]
        public string Nombre { get; set; }

        /// <summary>
        /// Cédula de tabla USU_USUARIO
        /// </summary>
        [Display(Name = "Cédula")]
        [Column("USU_CEDULA")]
        public string Cedula { get; set; }

        /// <summary>
        /// Primer apellido de tabla USU_USUARIO
        /// </summary>
        [Display(Name = "Primer apellido")]
        [Column("USU_APELLIDO1")]
        public string Apellido1 { get; set; }

        /// <summary>
        /// Segundo apellido de tabla USU_USUARIO
        /// </summary>
        [Display(Name = "Segundo apellido")]
        [Column("USU_APELLIDO2")]
        public string Apellido2 { get; set; }

        /// <summary>
        /// Contraseña de tabla USU_USUARIO
        /// </summary>
        [Display(Name = "Contraseña")]
        [DataType(DataType.Password)]
        [Column("USU_PASSWORD")]
        public string Contrasenia { get; set; }

        /// <summary>
        /// Teléfono de tabla USU_USUARIO
        /// </summary>
        [Display(Name = "Teléfono")]
        [Column("USU_TELEFONO")]
        public string Telefono { get; set; }

        /// <summary>
        /// Celular de tabla USU_USUARIO
        /// </summary>
        [Display(Name = "Celular")]
        [Column("USU_CELULAR")]
        public string Celular { get; set; }

        /// <summary>
        /// Correo de tabla USU_USUARIO
        /// </summary>
        [Display(Name = "Correo")]
        [Column("USU_EMAIL")]
        public string Correo { get; set; }

        /// <summary>
        /// Validacion activo o inactivo de tabla USU_USUARIO
        /// </summary>
        [Display(Name = "Activo")]
        [Column("USU_VISIBLE")]
        public bool Visible { get; set; }

        /// <summary>
        /// Fecha de creación de tabla USU_USUARIO
        /// </summary>
        [Display(Name = "Creado")]
        [Column("USU_CREATEDATE")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Fecha de modificación de tabla USU_USUARIO
        /// </summary>
        [Display(Name = "Actualizado")]
        [Column("USU_UPDATEDATE")]
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// Usuario que lo creó de tabla USU_USUARIO
        /// </summary>
        [Display(Name = "Creado por")]
        [Column("USU_CREATEUSER")]
        public string CreateUser { get; set; }

        /// <summary>
        /// Usuario que lo modifico de tabla USU_USUARIO
        /// </summary>
        [Display(Name = "Actualizado por")]
        [Column("USU_UPDATEUSER")]
        public string UpdateUser { get; set; }

    }
}