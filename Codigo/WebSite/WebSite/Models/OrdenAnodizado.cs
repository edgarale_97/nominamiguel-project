﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebSite.Models
{
    /// <summary>
    /// Nombre de la clase: OrdenAnodizado
    /// </summary>
    [Table("ORDA_ORDENES_ANODIDIZADO")]
    public class OrdenAnodizado
    {
        /// <summary>
        /// Id auto incremental
        /// </summary>
        [Column("ORDA_CODE")]
        public int Code { get; set; }

        /// <summary>
        /// El codigo que le da el cliente a la orden
        /// </summary>
        [Display(Name = "Consecutivo")]
        [Column("ORDA_CODIGO")]
        public string Codigo { get; set; }

        /// <summary>
        /// Codigo del cliente
        /// </summary>
        [Display(Name = "Cliente")]
        [Column("CLI_CODE")]
        public int ClienteCode { get; set; }

        /// <summary>
        /// Codigo del cliente
        /// </summary>
        [Display(Name = "Ficha técnica")]
        [Column("FITA_CODE")]
        public int FichaCode { get; set; }

        /// <summary>
        /// Indica si es una orden normal o de reproceso
        /// </summary>
        [Display(Name = "Tipo")]
        [Column("ORDA_TIPO")]
        public string Tipo { get; set; }

        /// <summary>
        /// Indica el codigo de referencia si es de reproceso
        /// </summary>
        [Display(Name = "Referencia")]
        [Column("ORDA_CODIGOREFERENCIA")]
        public string Referencia { get; set; }


        /// <summary>
        /// Cantidad total
        /// </summary>
        [Display(Name = "Cantidad total")]
        [Column("ORDA_CANTIDAD_TOTAL")]
        public int CantidadTotal { get; set; }

        /// <summary>
        /// Cantidad total lotes
        /// </summary>
        [Display(Name = "Cantidad lotes")]
        [Column("ORDA_CANTIDAD_LOTES")]
        public int CantidadTotalLotes { get; set; }

        /// <summary>
        /// Fecha vencimiento
        /// </summary>
        [Display(Name = "Fecha vencimiento")]
        [Column("ORDA_FECHA_VENCIMIENTO")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? FechaVencimiento { get; set; }

        /// <summary>
        /// Numero lote
        /// </summary>
        [Display(Name = "Número lote")]
        [Column("ORDA_NUMERO_LOTE")]
        public int NumeroLote { get; set; }

        /// <summary>
        /// Codigo orden de trabajo
        /// </summary>
        [Display(Name = "Orden Trabajo")]
        [Column("ORDA_ORDEN_TRABAJO")]
        public string OrdenTrabajo { get; set; }

        /// <summary>
        /// Revision plano
        /// </summary>
        [Display(Name = "Revisión Plano")]
        [Column("ORDA_REVISION_PLANO")]
        public string RevisionPlano { get; set; }

        /// <summary>
        /// Tipo de tratamiento
        /// </summary>
        [Display(Name = "Tipo Tratamiento")]
        [Column("TITRA_CODE")]
        public int TipoTratamiento { get; set; }


        #region Recibido

        /// <summary>
        /// Recibido Cantidad
        /// </summary>
        [Display(Name = "Código")]
        [Column("ORDA_RECIBIDO_CODIGO")]
        public string RecibidoCodigo { get; set; }

        /// <summary>
        /// Recibido cantidad
        /// </summary>
        [Display(Name = "Cantidad")]
        [Column("ORDA_RECIBIDO_CANTIDAD")]
        public int RecibidoCantidad { get; set; }

        /// <summary>
        /// Recibido metricas criticas
        /// </summary>
        [Display(Name = "Medidas Críticas")]
        [Column("ORDA_RECIBIDO_MEDIDAS_CRITICAS")]
        public string RecibidoMedidasCriticas { get; set; }

        /// <summary>
        /// Recibido responsable
        /// </summary>
        [Display(Name = "Responsable")]
        [Column("ORDA_RECIBIDO_RESPONSABLE")]
        public string RecibidoResponsable { get; set; }

        /// <summary>
        /// Recibido fecha
        /// </summary>
        [Display(Name = "Fecha")]
        [Column("ORDA_RECIBIDO_FECHA")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? RecibidoFecha { get; set; }

        /// <summary>
        /// Indica si la seccion esta activa
        /// </summary>
        [Column("ORDA_RECIBIDO_ACTIVO")]
        public bool RecibidoActivo { get; set; }
        #endregion

        #region Raqueo

        /// <summary>
        /// Raqueo recomendacion
        /// </summary>
        [Display(Name = "Recomendación")]
        [Column("ORDA_RAQUEO_RECOMENDACION")]
        public string RaqueoRecomendacion { get; set; }

        /// <summary>
        /// Raqueo cantidad
        /// </summary>
        [Display(Name = "Cantidad")]
        [Column("ORDA_RAQUEO_CANTIDAD")]
        public int RaqueoCantidad { get; set; }

        /// <summary>
        /// Raqueo responsable
        /// </summary>
        [Display(Name = "Responsable")]
        [Column("ORDA_RAQUEO_RESPONSABLE")]
        public string RaqueoResponsable { get; set; }

        /// <summary>
        /// raqueo fecha
        /// </summary>
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Fecha")]
        [Column("ORDA_RAQUEO_FECHA")]
        public DateTime? RaqueoFecha { get; set; }

        /// <summary>
        /// Indica si la seccion esta activa
        /// </summary>
        [Column("ORDA_RAQUEO_ACTIVO")]
        public bool RaqueoActivo { get; set; }

        #endregion

        #region Proceso

        /// <summary>
        /// proceso desengrase minutos
        /// </summary>
        [Display(Name = "Minutos")]
        public decimal? ProcesoDesengraseMinutos { get; set; }

        /// <summary>
        /// proceso desengrase tanque
        /// </summary>
        [Display(Name = "Tanque")]
        public int? ProcesoDesengraseTanque { get; set; }

        /// <summary>
        /// proceso desengrase hora inicial
        /// </summary>
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Hora Inicial")]
        public DateTime? ProcesoDesengraseHoraInicio { get; set; }

        /// <summary>
        /// proceso desengrase hora final
        /// </summary>
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Hora Final")]
        public DateTime? ProcesoDesengraseHoraFinal { get; set; }

        /// <summary>
        /// proceso eatching
        /// </summary>
        [Display(Name = "Etching")]
        [Column("ORDA_PROCESO_ETCHING")]
        public decimal? ProcesoEtching { get; set; }

        /// <summary>
        /// proceso etching tanque
        /// </summary>
        [Display(Name = "Tanque")]
        [Column("ORDA_PROCESO_ETCHING_TANQUE")]
        public int? ProcesoEtchingTanque { get; set; }

        /// <summary>
        /// proceso etching hora inicial
        /// </summary>
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Hora Inicial")]
        [Column("ORDA_PROCESO_ETCHING_HORA_INICIAL")]
        public DateTime? ProcesoEtchingHoraInicial { get; set; }

        /// <summary>
        /// proceso etching hora final
        /// </summary>
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Hora Final")]
        [Column("ORDA_PROCESO_ETCHING_HORA_FINAL")]
        public DateTime? ProcesoEtchingHoraFinal { get; set; }

        /// <summary>
        /// proceso amperaje
        /// </summary>
        [Display(Name = "Amperaje")]
        [Column("ORDA_PROCESO_AMPERAJE")]
        public decimal? ProcesoAmperaje { get; set; }

        /// <summary>
        /// proceso amperaje tanque
        /// </summary>
        [Display(Name = "Tanque")]
        [Column("ORDA_PROCESO_AMPERAJE_TANQUE")]
        public int? ProcesoAmperajeTanque { get; set; }

        /// <summary>
        /// proceso amperaje hora inicial
        /// </summary>
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Hora Inicial")]
        [Column("ORDA_PROCESO_AMPERAJE_HORA_INICIAL")]
        public DateTime? ProcesoAmperajeHoraInicio { get; set; }

        /// <summary>
        /// Recibido proceso amperaje hora final
        /// </summary>
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Hora Final")]
        [Column("ORDA_PROCESO_AMPERAJE_HORA_FINAL")]
        public DateTime? ProcesoAmperajeHoraFinal { get; set; }

        /// <summary>
        /// proceso anodizado minutos
        /// </summary>
        [Display(Name = "Minutos")]
        [Column("ORDA_PROCESO_ANODIZADO_MINUTOS")]
        public decimal? ProcesoAnodizadoMinutos { get; set; }

        /// <summary>
        /// proceso anodizado tanque
        /// </summary>
        [Display(Name = "Tanque")]
        [Column("ORDA_PROCESO_ANODIZADO_TANQUE")]
        public int? ProcesoAnodizadoTanque { get; set; }

        /// <summary>
        /// proceso anodizado hora inicio
        /// </summary>
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Hora Inicial")]
        [Column("ORDA_PROCESO_ANODIZADO_HORA_INICIAL")]
        public DateTime? ProcesoAnodizadoHoraInicio { get; set; }


        /// <summary>
        /// proceso anodizado hora final
        /// </summary>
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Hora Final")]
        [Column("ORDA_PROCESO_ANODIZADO_HORA_FINAL")]
        public DateTime? ProcesoAnodizadoHoraFinal { get; set; }

        /// <summary>
        /// proceso anodizado tinta minutos
        /// </summary>
        [Display(Name = "Tinta Minutos")]
        [Column("ORDA_PROCESO_TINTA_MINUTOS")]
        public decimal? ProcesoTintaMinutos { get; set; }

        /// <summary>
        /// proceso anodizado tinta tanque
        /// </summary
        [Display(Name = "Tinta Tanque")]
        [Column("ORDA_PROCESO_TINTA_TANQUE")]
        public int? ProcesoTintaTanque { get; set; }

        /// <summary>
        /// proceso anodizado tinta hora inicial
        /// </summary
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Hora Inicial")]
        [Column("ORDA_PROCESO_TINTA_HORA_INICIAL")]
        public DateTime? ProcesoTintaHoraInicio { get; set; }

        /// <summary>
        /// proceso anodizado tinta hora final
        /// </summary>
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Hora Final")]
        [Column("ORDA_PROCESO_TINTA_HORA_FINAL")]
        public DateTime? ProcesoTintaHoraFinal { get; set; }

        /// <summary>
        /// proceso sello minutos
        /// </summary>
        [Display(Name = "Minutos")]
        [Column("ORDA_PROCESO_SELLO_MINUTOS")]
        public decimal? ProcesoSelloMinutos { get; set; }

        /// <summary>
        /// proceso sello tanque
        /// </summary>
        [Display(Name = "Tanque")]
        [Column("ORDA_PROCESO_SELLO_TANQUE")]
        public int? ProcesoSelloTanque { get; set; }

        /// <summary>
        /// proceso sello hora inicial
        /// </summary
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Hora Inicial")]
        [Column("ORDA_PROCESO_SELLO_HORA_INICIAL")]
        public DateTime? ProcesoSelloHoraInicial { get; set; }

        /// <summary>
        /// proceso sello hora final
        /// </summary>
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Hora Final")]
        [Column("ORDA_PROCESO_SELLO_HORA_FINAL")]
        public DateTime? ProcesoSelloHoraFinal { get; set; }

        /// <summary>
        /// proceso otros
        /// </summary>
        [Display(Name = "Otros")]
        [Column("ORDA_PROCESO_OTROS")]
        public string ProcesoOtros { get; set; }

        /// <summary>
        /// proceso otros minutos
        /// </summary>
        [Display(Name = "Minutos")]
        [Column("ORDA_PROCESO_OTROS_MINUTOS")]
        public decimal? ProcesoOtrosMinutos { get; set; }

        /// <summary>
        /// proceso otros tanque
        /// </summary>
        [Display(Name = "Tanque")]
        [Column("ORDA_PROCESO_OTROS_TANQUE")]
        public int? ProcesoOtrosTanque { get; set; }

        /// <summary>
        /// proceso otros hora inicial
        /// </summary>
        [Display(Name = "Hora Inicial")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Column("ORDA_PROCESO_OTROS_HORA_INICIAL")]
        public DateTime? ProcesoOtrosHoraInicial { get; set; }

        /// <summary>
        /// proceso otros hora final
        /// </summary>
        [Display(Name = "Hora Final")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Column("ORDA_PROCESO_OTROS_HORA_FINAL")]
        public DateTime? ProcesoOtrosHoraFinal { get; set; }

        /// <summary>
        /// proceso cuidados especiales
        /// </summary>
        [Display(Name = "Cuidados Especiales")]
        [Column("ORDA_PROCESO_CUIDADOS_ESPECIALES")]
        public string ProcesoCiudadosEspeciales { get; set; }

        /// <summary>
        /// proceso responsable
        /// </summary>
        [Display(Name = "Responsable")]
        [Column("ORDA_PROCESO_RESPONSABLE")]
        public string ProcesoResponsable { get; set; }

        /// <summary>
        /// Indica si la seccion esta activa
        /// </summary>
        [Column("ORDA_PROCESO_ACTIVO")]
        public bool ProcesoActivo { get; set; }
        #endregion

        #region Salida
        /// <summary>
        /// salida criterios de aceptacion
        /// </summary>
        [Display(Name = "Criterios de aceptación")]
        [Column("ORDA_SALIDA_CRITERIOSACEPTACION")]
        public string SalidaCriteriosAceptacion { get; set; }

        /// <summary>
        /// salida cantidad aprobada
        /// </summary>
        [Display(Name = "Cantidad aprobada")]
        [Column("ORDA_SALIDA_CANTIAPROBADA")]
        public int SalidaCantiAprobada { get; set; }

        /// <summary>
        /// salida cantidad rechazada
        /// </summary>
        [Display(Name = "Cantidad rechazada")]
        [Column("ORDA_SALIDA_CANTIRECHAZADA")]
        public int SalidaCantiRechazada { get; set; }

        /// <summary>
        /// salida causas de rechazo
        /// </summary>
        [Display(Name = "Causas")]
        [Column("ORDA_SALIDA_CAUSAS")]
        public string SalidaCausas { get; set; }

        /// <summary>
        /// salida acciones con las piezas rechazadas
        /// </summary>
        [Display(Name = "Acción")]
        [Column("ORDA_SALIDA_ACCION")]
        public int SalidaAccion { get; set; }

        /// <summary>
        /// salida cantidad de piezas aprobadas
        /// </summary>
        [Display(Name = "Liberación lote")]
        [Column("ORDA_SALIDA_APROBADO")]
        public bool SalidaAprobadas { get; set; }

        /// <summary>
        /// salida fecha
        /// </summary>
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Fecha")]
        [Column("ORDA_SALIDA_FECHA")]
        public DateTime? SalidaFecha { get; set; }

        /// <summary>
        /// salida responsable
        /// </summary>
        [Display(Name = "Responsable")]
        [Column("ORDA_SALIDA_RESPONSABLE")]
        public string SalidaResponsable { get; set; }


        /// <summary>
        /// Indica si la seccion esta activa
        /// </summary>
        [Column("ORDA_SALIDA_ACTIVO")]
        public bool SalidaActivo { get; set; }
        #endregion

        #region Empaque
        /// <summary>
        /// empaque responsable
        /// </summary>
        [Display(Name = "Recomendación")]
        [Column("ORDA_EMPAQUE_RECOMENDACION")]
        public string EmpaqueRecomendacion { get; set; }

        /// <summary>
        /// empaque cantidad
        /// </summary>
        [Display(Name = "Cantidad")]
        [Column("ORDA_EMPAQUE_CANTIDAD")]
        public int EmpaqueCantidad { get; set; }

        /// <summary>
        /// empaque fecha
        /// </summary>
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Fecha")]
        [Column("ORDA_EMPAQUE_FECHA")]
        public DateTime? EmpaqueFecha { get; set; }

        /// <summary>
        /// empaque cantidad
        /// </summary>
        [Display(Name = "Responsable")]
        [Column("ORDA_EMPAQUE_RESPONSABLE")]
        public string EmpaqueResponsable { get; set; }

        /// <summary>
        /// Indica si la seccion esta activa
        /// </summary>
        [Column("ORDA_EMPAQUE_ACTIVO")]
        public bool EmpaqueActivo { get; set; }
        #endregion

        #region Despacho
        /// <summary>
        /// despacho cantidad
        /// </summary>
        [Display(Name = "Cantidad")]
        [Column("ORDA_DESPACHO_CANTIDAD")]
        public int DespachoCantidad { get; set; }

        /// <summary>
        /// Despacho certificado 
        /// </summary>
        [Display(Name = "Certificado")]
        [Column("ORDA_DESPACHO_CERTIFICADO")]
        public string DespachoCertificado { get; set; }

        /// <summary>
        /// despacho factura
        /// </summary>
        [Display(Name = "N Factura")]
        [Column("ORDA_DESPACHO_NFACTURA")]
        public string DespachoFactura { get; set; }

        /// <summary>
        /// despacho fecha
        /// </summary>
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Fecha")]
        [Column("ORDA_DESPACHO_FECHA")]
        public DateTime? DespachoFecha { get; set; }

        /// <summary>
        /// despacho responsable
        /// </summary>
        [Display(Name = "Responsable")]
        [Column("ORDA_DESPACHO_RESPONSABLE")]
        public string DespachoResponsable { get; set; }

        /// <summary>
        /// Indica si la seccion esta activa
        /// </summary>
        [Column("ORDA_DESPACHO_ACTIVO")]
        public bool DespachoActivo { get; set; }
        #endregion

        /// <summary>
        /// visible
        /// </summary>
        [Display(Name = "Activo")]
        [Column("ORDA_VISIBLE")]
        public bool Visible { get; set; }


        /// <summary>
        /// Estado de la orden
        /// </summary>
        [Display(Name = "Estado")]
        [Column("ESOR_CODE")]
        public int Estado { get; set; }

        /// <summary>
        /// Estado de la orden
        /// </summary>
        [Display(Name = "Prioridad")]
        [Column("TIPRI_CODE")]
        public int TipoPrioridadCode { get; set; }

        /// <summary>
        /// fecha de creacion
        /// </summary>
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Creado")]
        [Column("ORDA_CREATEDATE")]
        public DateTime? FechaCreacion { get; set; }

        /// <summary>
        /// creado por
        /// </summary>
        [Display(Name = "Creado Por")]
        [Column("ORDA_CREATEUSER")]
        public string CreadoPor { get; set; }

        /// <summary>
        /// fecha de creacion
        /// </summary>
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Actualizado")]
        [Column("ORDA_UPDATEDATE")]
        public DateTime? FechaActualizacion { get; set; }

        /// <summary>
        /// actualizado por
        /// </summary>
        [Display(Name = "Actualizado Por")]
        [Column("ORDA_UPDATEUSER")]
        public string ActualizadoPor { get; set; }

    }
}