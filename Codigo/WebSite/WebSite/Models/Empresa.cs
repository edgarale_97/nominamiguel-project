﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Net.Http;
using Business;
using Newtonsoft.Json;

namespace WebSite.Models
{


    /// <summary>
    /// Nombre de la clase: Empresa
    /// </summary>
    [Table("EMP_EMPRESA")]
    public class Empresa
    {

        private Api api;
        private string ApiUrl;
        public Empresa()
        {
            api = new Api();
            ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();
        }


        /// <summary>
        /// Codigo
        /// </summary>
        [Display(Name = "Código")]
        [Key]
        [Column("EMP_CODE")]
        public short Code { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>
        [Display(Name = "Nombre")]
        [Column("EMP_NAME")]
        public string Nombre { get; set; }

        /// <summary>
        /// Telefono
        /// </summary>
        [Display(Name = "Teléfono")]
        [Column("EMP_TELEFONO")]
        public string Telefono { get; set; }

        /// <summary>
        /// Fax
        /// </summary>
        [Display(Name = "Fax")]
        [Column("EMP_FAX")]
        public string Fax { get; set; }

        /// <summary>
        /// Correo electronico
        /// </summary>
        [Display(Name = "Correo")]
        [Column("EMP_MAIL")]
        public string Correo { get; set; }

        /// <summary>
        /// Url website
        /// </summary>
        [Display(Name = "Sitio Web")]
        [Column("EMP_URL")]
        public string Url { get; set; }

        /// <summary>
        /// Direccion
        /// </summary>
        [Display(Name = "Dirección")]
        [Column("EMP_DIRECCION")]
        public string Direccion { get; set; }

        /// <summary>
        /// Cedula juridica
        /// </summary>
        [Display(Name = "Cédula")]
        [Column("EMP_CEDULAJURIDICA")]
        public string CedulaJuridica { get; set; }

        /// <summary>
        /// Descripcion
        /// </summary>
        [Display(Name = "Descripción")]
        [Column("EMP_DESCRIPCION")]
        public string Descripcion { get; set; }

        /// <summary>
        /// Logotipo
        /// </summary>
        [Display(Name = "Logotipo")]
        [Column("EMP_LOGO")]
        public string Logo { get; set; }

        /// <summary>
        /// Visible
        /// </summary>
        [Display(Name = "Activo")]
        [Column("EMP_VISIBLE")]
        public bool Visible { get; set; }

        /// <summary>
        /// Fecha de creacion
        /// </summary>
        [Display(Name = "Creado")]
        [Column("EMP_CREATEDATE")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Fecha de actualizacion
        /// </summary>
        [Display(Name = "Actualizado")]
        [Column("EMP_UPDATEDATE")]
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// Usuario que crea
        /// </summary>
        [Display(Name = "Creado por")]
        [Column("EMP_CREATEUSER")]
        public int CreateUser { get; set; }

        /// <summary>
        /// Usuario que actualiza
        /// </summary>
        [Display(Name = "Actualizado por")]
        [Column("EMP_UPDATEUSER")]
        public int UpdateUser { get; set; }

        /// <summary>
        /// Usuario que crea
        /// </summary>
        [Display(Name = "Creado por")]
        [Column("EMP_CREATEUSER")]
        public string CreateUserName { get 
            {
                return GetUserName(CreateUser);
                    
            }

            set
            {
                this.CreateUserName = value;
            } }

        /// <summary>
        /// Usuario que actualiza
        /// </summary>
        [Display(Name = "Actualizado por")]
        [Column("EMP_UPDATEUSER")]
        public string UpdateUserName { get
            {
                return GetUserName(UpdateUser);
            }
            set { this.UpdateUserName = value; } }



        public string GetUserName(int userCode)
        {
            Usuario usuario = null;
            if (userCode != 0)
            {
                try
                {
                    var jsonUsuario = api.Get(Convert.ToInt32(userCode), ApiUrl + "Usuarios/");
                    usuario = JsonConvert.DeserializeObject<Usuario>(jsonUsuario);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                return usuario.Nombre;
            }
            else
            {
                return "Ninguno";
            }
            
        }
    }
}