﻿using Business;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using WebSite.Models;

namespace WebSite.App_Start
{
    public class AccessCheck : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpSessionStateBase session = filterContext.HttpContext.Session;

            var api = new Api();
            var ApiUrl = ConfigurationManager.AppSettings["ApiUrl"].ToString();

            var jsonControladores = api.Get(ApiUrl + "Controladores");
            var controladores = JsonConvert.DeserializeObject<List<Controlador>>(jsonControladores);

            var jsonAcciones = api.Get(ApiUrl + "acciones");
            var acciones = JsonConvert.DeserializeObject<List<Accion>>(jsonAcciones);

            var jsonPermisosRoles = api.Get(ApiUrl + "PermisosRoles");
            var permisosRoles = JsonConvert.DeserializeObject<List<RolPermiso>>(jsonPermisosRoles);

            var routeValues = HttpContext.Current.Request.RequestContext.RouteData.Values;

            var controlador = controladores.Where(p => p.Nombre == routeValues["controller"].ToString()).FirstOrDefault();
            var accion = acciones.Where(p => p.Nombre == routeValues["action"].ToString() && p.CodigoControlador == controlador.Codigo).FirstOrDefault();

            var permisoRol = permisosRoles.Where(
                p => p.RolCode == Convert.ToInt16(session["RolID"]) && p.ControladorCode == controlador.Codigo && p.AccionCode == accion.Codigo)
                .FirstOrDefault();

            if (permisoRol == null)
            {
                //filterContext.Result = new RedirectToRouteResult(
                //    new RouteValueDictionary {
                //                { "Controller", "Sesion" },
                //                { "Error403", "" }
                //                });

                filterContext.Result = new RedirectToRouteResult(
                   new RouteValueDictionary(
                       new
                       {
                           action = "Error403",
                           Controller = "Sesion",
                           area = ""
                       }));
            }
        }
    }
}