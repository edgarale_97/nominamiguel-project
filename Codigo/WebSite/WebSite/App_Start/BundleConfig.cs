﻿using System.Web;
using System.Web.Optimization;

namespace WebSite
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/corejs").Include(
                "~/Content/theme/assets/global/plugins/js.cookie.min.js",
                "~/Content/theme/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js",
                "~/Content/theme/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js",
                "~/Content/theme/assets/global/plugins/jquery.blockui.min.js",
                "~/Content/theme/assets/global/plugins/uniform/jquery.uniform.min.js",

                "~/Content/theme/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js",

                "~/Content/theme/assets/global/scripts/app.min.js",
                "~/Content/theme/assets/layouts/layout/scripts/layout.min.js",
                "~/Content/theme/assets/layouts/global/scripts/quick-sidebar.min.js",

                "~/Content/theme/assets/global/plugins/bootstrap-toastr/toastr.min.js",

                "~/Scripts/layout.js"
            ));

            bundles.Add(new StyleBundle("~/Content/corestyle").Include(
                //"http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all",
                "~/Content/theme/assets/global/plugins/font-awesome/css/font-awesome.css",
                "~/Content/theme/assets/global/plugins/simple-line-icons/simple-line-icons.min.css",
                "~/Content/theme/assets/global/plugins/uniform/css/uniform.default.css",
                "~/Content/theme/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css",

                "~/Content/theme/assets/global/css/components.min.css",
                "~/Content/theme/assets/global/css/plugins.min.css",

                "~/Content/theme/assets/layouts/layout/css/layout.min.css",
                "~/Content/theme/assets/layouts/layout/css/themes/light2.min.css",
                "~/Content/theme/assets/layouts/layout/css/custom.min.css",

                "~/Content/theme/assets/global/plugins/bootstrap-toastr/toastr.min.css",

                "~/Content/theme/assets/global/css/validation.css"
            ));

            bundles.Add(new ScriptBundle("~/bundles/gridMvc").Include(
               "~/Scripts/gridmvc.js",
               "~/Scripts/gridmvc.lang.es.js",
               "~/Scripts/gridmvc-ext.js",
               "~/Scripts/URI.js",
               "~/Scripts/gridmvc-columnas-grid.js"));

            bundles.Add(new StyleBundle("~/Content/cssGridMvc").Include(
                "~/Content/gridmvc.datepicker.css",
                "~/Content/Gridmvc.css"));

            bundles.Add(new ScriptBundle("~/bundles/amcharts").Include(
                "~/Content/theme/assets/global/plugins/amcharts/amcharts/amcharts.js",
                "~/Content/theme/assets/global/plugins/amcharts/amcharts/pie.js",
                "~/Content/theme/assets/global/plugins/amcharts/amcharts/serial.js",
                "~/Content/theme/assets/global/plugins/amcharts/amcharts/themes/light.js",
                "~/Content/theme/assets/global/plugins/amcharts/amcharts/lang.es.js"));


        }
    }
}
