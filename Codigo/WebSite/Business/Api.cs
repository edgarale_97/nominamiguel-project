﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class Api
    {
        public string Get(string url)
        {
            var message = "";

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                var responseTask = client.GetAsync("");
                responseTask.Wait();

                var result = responseTask.Result;

                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();

                    message = readTask.Result;
                }
                return message;
            }
        }

        public string Get(int? id, string url)
        {
            var json = "";

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                var responseTask = client.GetAsync("" + id);
                responseTask.Wait();

                var result = responseTask.Result;

                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();

                    json = readTask.Result;
                }
                return json;
            }
        }

        public bool Post(string jsonObj, string url)
        {
            using (var client = new HttpClient())
            {
                var content = new StringContent(jsonObj.ToString(), Encoding.UTF8, "application/json");

                var uri = new Uri(url);

                var responseTask = client.PostAsync(uri, content);

                responseTask.Wait();
                var result = responseTask.Result;

                return result.IsSuccessStatusCode;
            }
        }

        public string PostReturnInfo(string jsonObj, string url)
        {
            var info = "";

            using (var client = new HttpClient())
            {
                var content = new StringContent(jsonObj.ToString(), Encoding.UTF8, "application/json");

                var uri = new Uri(url);

                var responseTask = client.PostAsync(uri, content);

                responseTask.Wait();

                var result = responseTask.Result;

                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();

                    info = readTask.Result;
                }

            }

            return info;
        }

        public bool Post(string url, Dictionary<string, string> parametros)
        {
            var client = new HttpClient();
            var req = new HttpRequestMessage(HttpMethod.Post, url) { Content = new FormUrlEncodedContent(parametros) };
            var responseTask = client.SendAsync(req);
            var result = responseTask.Result;
            return result.IsSuccessStatusCode;
        }

        public bool Post(string url)
        {
            using (var client = new HttpClient())
            {
                var uri = new Uri(url);

                var responseTask = client.PostAsync(uri, null);

                responseTask.Wait();
                var result = responseTask.Result;

                return result.IsSuccessStatusCode;
            }
        }

        public string PostResponseString(string jsonObj, string url)
        {
            using (var client = new HttpClient())
            {
                var content = new StringContent(jsonObj.ToString(), Encoding.UTF8, "application/json");

                var uri = new Uri(url);

                var responseTask = client.PostAsync(uri, content);

                responseTask.Wait();
                var result = responseTask.Result;
                var mensajeAsync = result.Content.ReadAsStringAsync();
                var mensaje = mensajeAsync.Result.ToString();
                return mensaje;
            }
        }


        public bool Put(string jsonObj, string url)
        {
            using (var client = new HttpClient())
            {
                var content = new StringContent(jsonObj.ToString(), Encoding.UTF8, "application/json");

                var uri = new Uri(url);

                var responseTask = client.PutAsync(uri, content);

                responseTask.Wait();
                var result = responseTask.Result;

                return result.IsSuccessStatusCode;
            }
        }

        public bool Delete(int id, string url)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                var responseTask = client.DeleteAsync("" + id);

                responseTask.Wait();
                var result = responseTask.Result;

                return result.IsSuccessStatusCode;
            }
        }

        public string GetForWebClient(string url, string id)
        {
            using (var client = new WebClient())
            {
                client.Encoding = Encoding.UTF8;

                Uri uriConsultarRespuesta = new Uri(url);

                var result = client.DownloadString(uriConsultarRespuesta + id);
                return result;

                //return JsonConvert.DeserializeObject<EntitiesMH.Mensajes.Consulta>(result);
            }
        }
    }
}
