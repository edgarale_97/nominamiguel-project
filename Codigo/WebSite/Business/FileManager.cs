﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Business
{
    /// <summary>
    /// Nombre de la clase: FileManager
    /// </summary>
    public class FileManager
    {
        /// <summary>
        /// Método que sirve para convertir de HttpPostedFile a byte array
        /// </summary>
        /// <param name="image">Recibe por parametro un archivo de imagen</param>
        /// <returns>Retorna un array de bytes</returns>
        public byte[] FileToByteArray(HttpPostedFileBase file)
        {
            var content = new byte[file.ContentLength];
            file.InputStream.Read(content, 0, file.ContentLength);
            return content;
        }
    }
}
