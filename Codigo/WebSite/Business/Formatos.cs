﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public static class Formatos
    {
        public static int? GetInt32(string number)
        {
            if (String.IsNullOrEmpty(number))
            {
                return null;
            }

            return Convert.ToInt32(number);
        }

        public static decimal? GetDecimal(string number)
        {
            if (String.IsNullOrEmpty(number))
            {
                return null;
            }

            return Convert.ToDecimal(number);
        }

        public static DateTime? GetDateTime(string datetime)
        {
            if (String.IsNullOrEmpty(datetime) || datetime == "undefined")
            {
                return null;
            }

            return Convert.ToDateTime(datetime);
        }

        public static DateTime? GetTime(string pTime)
        {
            if (String.IsNullOrEmpty(pTime) || pTime == "undefined")
            {
                return null;
            }

            var timeSplit = pTime.Split(':');

            if(timeSplit.Count() != 2)
            {
                return null;
            }

            var hora = timeSplit[0].ToString();
            var minuto = timeSplit[1].ToString();

            var hoy = DateTime.Now;
            var timeFormat = new DateTime(hoy.Year, hoy.Month, hoy.Day, Convert.ToInt32(hora), Convert.ToInt32(minuto), 0);

            return timeFormat;
        }
    }
}
