﻿using Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Data
{
    /// <summary>
    /// Nombre de la clase: Context
    /// </summary>
    public class Context : DbContext
    {
        /// <summary>
        /// Mapeo de el objeto Provincias con la db
        /// </summary>
        public DbSet<Pais> Paises { get; set; }

        /// <summary>
        /// Mapeo de el objeto Provincias con la db
        /// </summary>
        public DbSet<Provincia> Provincias { get; set; }

        /// <summary>
        /// Mapeo de el objeto Cantones con la db
        /// </summary>
        public DbSet<Canton> Cantones { get; set; }

        /// <summary>
        /// Mapeo de el objeto Distritos con la db
        /// </summary>
        public DbSet<Distrito> Distritos { get; set; }

        /// <summary>
        /// Mapeo de el objeto Controladores con la db
        /// </summary>
        public DbSet<Controlador> Controladores { get; set; }

        /// <summary>
        /// Mapeo de el objeto Acciones con la db
        /// </summary>
        public DbSet<Accion> Acciones { get; set; }

        /// <summary>
        /// Mapeo de el objeto Roles con la db
        /// </summary>
        public DbSet<Rol> Roles { get; set; }

        /// <summary>
        /// Mapeo de el objeto Empresa con la db
        /// </summary>
        public DbSet<Empresa> Empresas { get; set; }

        /// <summary>
        /// Mapeo de el objeto Usuario con la db
        /// </summary>
        public DbSet<Usuario> Usuarios { get; set; }

        /// <summary>
        /// Mapeo de el objeto RolPermiso con la db
        /// </summary>
        public DbSet<RolPermiso> RolesPermisos { get; set; }

        /// <summary>
        /// Mapeo de el objeto Cliente con la db
        /// </summary>
        public DbSet<Cliente> Clientes { get; set; }

        /// <summary>
        /// Mapeo de el objeto FichaTecnica con la db
        /// </summary>
        public DbSet<FichaTecnica> FichasTecnicas { get; set; }

        /// <summary>
        /// Mapeo de el objeto OrdenAnodizado con la db
        /// </summary>
        public DbSet<OrdenAnodizado> OrdenesAnodizado { get; set; }

        /// <summary>
        /// Mapeo de el objeto FotoRecibo con la db
        /// </summary>
        public DbSet<FotoRecibo> FotosRecibo { get; set; }

        /// <summary>
        /// Mapeo de el objeto FotoRaqueo con la db
        /// </summary>
        public DbSet<FotoRaqueo> FotosRaqueo { get; set; }

        /// <summary>
        /// Mapeo de el objeto FotoEmpaque con la db
        /// </summary>
        public DbSet<FotoEmpaque> FotosEmpaque { get; set; }

        /// <summary>
        /// Mapeo de el objeto FotoSalida con la db
        /// </summary>
        public DbSet<FotoSalida> FotosSalida { get; set; }


        /// <summary>
        /// Mapeo de el objeto EstatusOrdenAnodizado con la db
        /// </summary>
        public DbSet<EstatusOrdenAnodizado> EstatusOrdenAnodizado { get; set; }

        /// <summary>
        /// Mapeo de el objeto AccionSalida con la db
        /// </summary>
        public DbSet<AccionSalida> AccionesSalida { get; set; }

        /// <summary>
        /// Mapeo de el objeto TipoTratamiento con la db
        /// </summary>
        public DbSet<TipoTratamiento> TiposTratamiento { get; set; }

        /// <summary>
        /// Mapeo de el objeto TipoMaterial con la db
        /// </summary>
        public DbSet<TipoMaterial> TiposMaterial { get; set; }

        /// <summary>
        /// Mapeo de el objeto TipoPrioridad con la db
        /// </summary>
        public DbSet<TipoPrioridad> TiposPrioridad { get; set; }

        /// <summary>
        /// Mapeo de el objeto ProductosQuimicos con la db
        /// </summary>
        public DbSet<Productos> ProductosQuimicos { get; set; }

        /// <summary>
        /// Metodo que sirve para crear los modelos
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Canton>()
                .HasKey(c => new { c.Code, c.ProvinciaCode });

            modelBuilder.Entity<Distrito>()
               .HasKey(c => new { c.ProvinciaCode, c.CantonCode, c.DistritoCode });
        }

        /// <summary>
        /// Metodo que sirve para conectar la base de datos con el proyecto
        /// </summary>
        /// <param name="optionsBuilder">Recibe un objeto DbContextOptionsBuilder</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //se realizan un objeto de configuracion mediante el directorio asignado
            var configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())

            //se agrega el archivo appsettings.json
            .AddJsonFile("appsettings.json")
            .Build();

            //se le pasa el connectionstring y los parametros para usar sql server
            optionsBuilder.UseSqlServer(configuration["ConnectionStrings:DefaultConnection"]);
        }

        /// <summary>
        /// Metodo que sirve para obtener un archivo de configuraciones
        /// </summary>
        /// <param name="parameter">Recibe el nombre del parametro de la configuracion</param>
        /// <returns>Retorna el valor correspondiente al parametro de configuracion</returns>
        public string GetConfiguration(string parameter)
        {
            //se realizan un objeto de configuracion mediante el directorio asignado
            var configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())

            //se agrega el archivo configurations.json
            .AddJsonFile("configurations.json")
            .Build();

            //se carga la configuracion en base al parametro
            var configuracion = configuration[parameter];

            //retorna la configuracion
            return configuracion;
        }
    }
}
