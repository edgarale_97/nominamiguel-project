﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities
{
    /// <summary>
    /// Nombre de la clase: Accion
    /// </summary>
    [Table("ACC_ACCIONES")]
    public class Accion
    {
        /// <summary>
        /// Codigo
        /// </summary>
        [Key]
        [Column("ACC_CODE")]
        public int Codigo { get; set; }

        /// <summary>
        /// Codigo del controlador
        /// </summary>
        [Column("CON_CODE")]
        [Display(Name = "Controlador")]
        public int CodigoControlador { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>
        [Column("ACC_NOMBRE")]
        public string Nombre { get; set; }

        /// <summary>
        /// Visible
        /// </summary>
        [Column("ACC_VISIBLE")]
        public bool Visible { get; set; }

        /// <summary>
        /// Fecha de creacion
        /// </summary>
        [Column("ACC_CREATEDATE")]
        [Display(Name = "Creado")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Fecha de actualizacion
        /// </summary>
        [Column("ACC_UPDATEDATE")]
        [Display(Name = "Acualizado")]
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// Usuario que crea
        /// </summary>
        [Column("ACC_CREATEUSER")]
        [Display(Name = "Creado Por")]
        public string CreateUser { get; set; }

        /// <summary>
        /// Usuario que actualiza
        /// </summary>
        [Display(Name = "Actualizado Por")]
        [Column("ACC_UPDATEUSER")]
        public string UpdateUser { get; set; }
    }
}
