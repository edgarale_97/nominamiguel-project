﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities
{
    /// <summary>
    /// Nombre de la clase: Provincia
    /// </summary>
    [Table("PROV_PROVINCIAS")]
    public class Provincia
    {
        /// <summary>
        /// Codigo
        /// </summary>
        [Key]
        [Column("PROV_CODE")]
        public short Code { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>
        [Required]
        [StringLength(30)]
        [Column("PROV_NAME")]
        public string Nombre { get; set; }

        /// <summary>
        /// Descripcion
        /// </summary>
        [StringLength(100)]
        [Column("PROV_DESCRIPCION")]
        public string Descripcion { get; set; }

        /// <summary>
        /// Visible
        /// </summary>
        [Column("PROV_VISIBLE")]
        public bool Visible { get; set; }

        /// <summary>
        /// Fecha de creacion
        /// </summary>
        [Required]
        [Column("PROV_CREATEDATE")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Usuario que crea
        /// </summary>
        [Required]
        [StringLength(20)]
        [Column("PROV_CREATEUSER")]
        public string CreateUser { get; set; }

        /// <summary>
        /// Fecha de actualizacion
        /// </summary>
        [Column("PROV_UPDATEDATE")]
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// Usuario que actualiza
        /// </summary>
        [StringLength(20)]
        [Column("PROV_UPDATEUSER")]
        public string UpdateUser { get; set; }
    }
}
