﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities
{
    /// <summary>
    /// Nombre de la clase: Distrito
    /// </summary>
    [Table("DIS_DISTRITOS")]
    public class Distrito
    {
        /// <summary>
        /// Codigo de la provincia
        /// </summary>
        [Key, Column("PROV_CODE", Order = 0)]
        public short ProvinciaCode { get; set; }

        /// <summary>
        /// Codigo del canton
        /// </summary>
        [Key, Column("CAN_CODE", Order = 1)]
        public short CantonCode { get; set; }

        /// <summary>
        /// Codigo del distrito
        /// </summary>
        [Key, Column("DIS_CODE", Order = 2)]
        public short DistritoCode { get; set; }

        /// <summary>
        /// Nombre 
        /// </summary>
        [Required]
        [StringLength(30)]
        [Column("DIS_NAME")]
        public string Nombre { get; set; }

        /// <summary>
        /// Descripcion 
        /// </summary>
        [StringLength(100)]
        [Column("DIS_DESCRIPCION")]
        public string Descripcion { get; set; }

        /// <summary>
        /// Visible
        /// </summary>
        [Column("DIS_VISIBLE")]
        public bool Visible { get; set; }

        /// <summary>
        /// Fecha de creacion
        /// </summary>
        [Required]
        [Column("DIS_CREATEDATE")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Usuario que crea
        /// </summary>
        [Required]
        [StringLength(20)]
        [Column("DIS_CREATEUSER")]
        public string CreateUser { get; set; }

        /// <summary>
        /// Fecha de actualizacion
        /// </summary>
        [Column("DIS_UPDATEDATE")]
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// Usuario que actualiza
        /// </summary>
        [StringLength(20)]
        [Column("DIS_UPDATEUSER")]
        public string UpdateUser { get; set; }
    }
}
