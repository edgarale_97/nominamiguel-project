﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities
{
    /// <summary>
    /// Nombre de la clase: FotoEmpaque
    /// </summary>
    [Table("FOEM_FOTOS_EMPAQUE_FICHA")]
    public class FotoEmpaque
    {
        /// <summary>
        /// Copdigo
        /// </summary>
        [Key]
        [Column("FOEM_CODE")]
        public int Codigo { get; set; }

        /// <summary>
        /// Codigo de la ficha tecnica
        /// </summary>
        [Column("FITA_CODE")]
        public int FichaCodigo { get; set; }

        /// <summary>
        /// Archivo en base 64
        /// </summary>
        [Column("FOEM_ARCHIVO_BASE64")]
        public string ArchivoBase64 { get; set; }

        /// <summary>
        /// Nombre del archivo
        /// </summary>
        [Column("FOEM_ARCHIVO_NOMBRE")]
        public string ArchivoNombre { get; set; }

        /// <summary>
        /// Tipo de archivo
        /// </summary>
        [Column("FOEM_ARCHIVO_TYPE")]
        public string ArchivoType { get; set; }

        /// <summary>
        /// Fecha de creacion
        /// </summary>
        [Column("FOEM_CREATEDATE")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Usuario que crea
        /// </summary>
        [Column("FOEM_CREATEUSER")]
        public string CreateUser { get; set; }

        /// <summary>
        /// Fecha en que actualizan
        /// </summary>
        [Column("FOEM_UPDATEDATE")]
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// Usuario que actualiza
        /// </summary>
        [Column("FOEM_UPDATEUSER")]
        public string UpdateUser { get; set; }
    }
}
