﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities
{
    /// <summary>
    /// Nombre de la clase: FotoSalida
    /// </summary>
    [Table("FOSA_FOTOS_SALIDA_FICHA")]
    public class FotoSalida
    {
        /// <summary>
        /// Copdigo
        /// </summary>
        [Key]
        [Column("FOSA_CODE")]
        public int Codigo { get; set; }

        /// <summary>
        /// Codigo de la ficha tecnica
        /// </summary>
        [Column("FITA_CODE")]
        public int FichaCodigo { get; set; }

        /// <summary>
        /// Archivo en base 64
        /// </summary>
        [Column("FOSA_ARCHIVO_BASE64")]
        public string ArchivoBase64 { get; set; }

        /// <summary>
        /// Nombre del archivo
        /// </summary>
        [Column("FOSA_ARCHIVO_NOMBRE")]
        public string ArchivoNombre { get; set; }

        /// <summary>
        /// Tipo de archivo
        /// </summary>
        [Column("FOSA_ARCHIVO_TYPE")]
        public string ArchivoType { get; set; }

        /// <summary>
        /// Fecha de creacion
        /// </summary>
        [Column("FOSA_CREATEDATE")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Usuario que crea
        /// </summary>
        [Column("FOSA_CREATEUSER")]
        public string CreateUser { get; set; }

        /// <summary>
        /// Fecha en que actualizan
        /// </summary>
        [Column("FOSA_UPDATEDATE")]
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// Usuario que actualiza
        /// </summary>
        [Column("FOSA_UPDATEUSER")]
        public string UpdateUser { get; set; }
    }
}
