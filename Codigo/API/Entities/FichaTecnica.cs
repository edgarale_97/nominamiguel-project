﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities
{
    /// <summary>
    /// Nombre de la clase: FichaTecnica
    /// </summary>
    [Table("FITA_FICHAS_TECNICAS_ANODIZADOS")]
    public class FichaTecnica
    {
        /// <summary>
        /// Codigo
        /// </summary>
        [Key]
        [Column("FITA_CODE")]
        public int Code { get; set; }

        /// <summary>
        /// Cliente
        /// </summary>
        [Required]
        [Column("CLI_CODE")]
        public int ClienteCode { get; set; }

        /// <summary>
        /// Codigo que define el usuario
        /// </summary>
        [Column("FITA_CODIGO")]
        public string Codigo { get; set; }

        /// <summary>
        /// Codigo del tipo de tratamiento
        /// </summary>
        [Column("TITRA_CODE")]
        public int TipoTratamientoCode { get; set; }

        /// <summary>
        /// Codigo que define el usuario
        /// </summary>
        [Column("FITA_REVISIONPLANO")]
        public string RevisionPlano { get; set; }

        /// <summary>
        /// Codigo del tipo de material
        /// </summary>
        [Column("TIMA_CODE")]
        public int TipoMaterialCode { get; set; }

        /// <summary>
        /// Medidas criticas de la inspeccion de recibido
        /// </summary>
        [Column("FITA_RECIBO_MEDIDAS_CRITICAS")]
        public string ReciboMedidaCriticas { get; set; }

        /// <summary>
        /// Recomendacion de raqueo
        /// </summary>
        [Column("FITA_RAQUEO_RECOMENDACION")]
        public string RaqueoRecomendacion { get; set; }

        /// <summary>
        /// Tiempo de desengrase
        /// </summary>
        [Column("FITA_TIEMPO_DESENGRASE")]
        public decimal TiempoDesengrase { get; set; }

        /// <summary>
        /// Tiempo de inmersion
        /// </summary>
        [Column("FITA_TIEMPO_ETCHING")]
        public decimal TiempoEtching { get; set; }

        /// <summary>
        /// Tiempo de amperaje o voltaje
        /// </summary>
        [Column("FITA_AMPERAJE_VOLTAJE")]
        public decimal AmperajeVoltaje { get; set; }

        /// <summary>
        /// Tiempo de anodizado
        /// </summary>
        [Column("FITA_TIEMPO_ANODIZADO")]
        public decimal TiempoAnodizado { get; set; }

        /// <summary>
        /// Tiempo de inmersion tinta
        /// </summary>
        [Column("FITA_TIEMPO_TINTA")]
        public decimal TiempoTinta { get; set; }

        /// <summary>
        /// Tiempo de inmersion sello
        /// </summary>
        [Column("FITA_TIEMPO_SELLO")]
        public decimal TiempoSello { get; set; }

        /// <summary>
        /// Cuidados especiales
        /// </summary>
        [Column("FITA_CUIDADOS_ESPECIALES")]
        public string CuidadosEspeciales { get; set; }

        /// <summary>
        /// Otros
        /// </summary>
        [Column("FITA_OTROS")]
        public string Otros { get; set; }

        /// <summary>
        /// criterios de aceptacion de la salida
        /// </summary>
        [Column("FITA_SALIDA_CRITERIOS_ACEPTACION")]
        public string SalidaCriteriosAceptacion { get; set; }

        /// <summary>
        /// Recomendacion de empaque
        /// </summary>
        [Column("FITA_EMPAQUE_RECOMENDACION")]
        public string EmpaqueRecomendacion { get; set; }

        /// <summary>
        /// Fecha de creacion
        /// </summary>
        [Required]
        [Column("FITA_CREATEDATE")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Usuario que crea
        /// </summary>
        [Required]
        [Column("FITA_CREATEUSER")]
        public string CreateUser { get; set; }

        /// <summary>
        /// Fecha de actualizacion
        /// </summary>
        [Column("FITA_UPDATEDATE")]
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// Usuario que actualiza
        /// </summary>
        [Column("FITA_UPDATEUSER")]
        public string UpdateUser { get; set; }

        /// <summary>
        /// Listado de fotos de recibo
        /// </summary>
        [ForeignKey("FichaCodigo")]
        public virtual ICollection<FotoRecibo> FotosRecibo { get; set; }

        /// <summary>
        /// Listado de fotos de raqueo
        /// </summary>
        [ForeignKey("FichaCodigo")]
        public ICollection<FotoRaqueo> FotosRaqueo { get; set; }

        /// <summary>
        /// Listado de fotos de empaque
        /// </summary>
        [ForeignKey("FichaCodigo")]
        public ICollection<FotoEmpaque> FotosEmpaque { get; set; }

        /// <summary>
        /// Listado de fotos de salida
        /// </summary>
        [ForeignKey("FichaCodigo")]
        public ICollection<FotoSalida> FotosSalida { get; set; }

    }
}
