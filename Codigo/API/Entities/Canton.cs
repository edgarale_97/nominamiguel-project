﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities
{
    /// <summary>
    /// Nombre de la clase: Canton
    /// </summary>
    [Table("CAN_CANTONES")]
    public class Canton
    {
        /// <summary>
        /// Codigo del canton
        /// </summary>
        [Key, Column("CAN_CODE", Order = 0)]
        public short Code { get; set; }

        /// <summary>
        /// Codigo de la provincia
        /// </summary>
        [Required]
        [Key, Column("PROV_CODE", Order = 1)]
        public short ProvinciaCode { get; set; }

        /// <summary>
        /// Nombre del canton
        /// </summary>
        [StringLength(30)]
        [Required]
        [Column("CAN_NAME")]
        public string Nombre { get; set; }

        /// <summary>
        /// Descripcion del canton
        /// </summary>
        [StringLength(100)]
        [Column("CAN_DESCRIPCION")]
        public string Descripcion { get; set; }

        /// <summary>
        /// Visibilidad del canton
        /// </summary>
        [Column("CAN_VISIBLE")]
        public bool Visible { get; set; }

        /// <summary>
        /// Fecha de creacion del registro
        /// </summary>
        [Required]
        [Column("CAN_CREATEDATE")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Usuario que crea
        /// </summary>
        [Required]
        [StringLength(20)]
        [Column("CAN_CREATEUSER")]
        public string CreateUser { get; set; }

        /// <summary>
        /// Fecha de actualizacion del registro
        /// </summary>
        [Column("CAN_UPDATEDATE")]
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// Usuario que actualiza
        /// </summary>
        [StringLength(20)]
        [Column("CAN_UPDATEUSER")]
        public string UpdateUser { get; set; }
    }
}
