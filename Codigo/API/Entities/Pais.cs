﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities
{
    /// <summary>
    /// Nombre de la clase: Pais
    /// </summary>
    [Table("PAI_PAIS")]
    public class Pais
    {
        [Key]
        [Column("PAI_CODE")]
        public short Code { get; set; }

        [Column("PAI_CODE_ISO")]
        public short CodeIso { get; set; }

        [Required]
        [StringLength(30)]
        [Column("PAI_NAME")]
        public string Nombre { get; set; }

        [StringLength(100)]
        [Column("PAI_DESCRIPCION")]
        public string Descripcion { get; set; }

        [Column("PAI_VISIBLE")]
        public bool Visible { get; set; }

        [Required]
        [Column("PAI_CREATEDATE")]
        public DateTime CreateDate { get; set; }

        [Required]
        [StringLength(20)]
        [Column("PAI_CREATEUSER")]
        public string CreateUser { get; set; }

        [Column("PAI_UPDATEDATE")]
        public DateTime? UpdateDate { get; set; }

        [StringLength(20)]
        [Column("PAI_UPDATEUSER")]
        public string UpdateUser { get; set; }
    }
}
