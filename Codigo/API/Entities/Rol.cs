﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities
{
    /// <summary>
    /// Nombre de la clase: ROL_ROLES
    /// </summary>
    [Table("ROL_ROLES")]
    public class Rol
    {
        /// <summary>
        /// Codigo 
        /// </summary>
        [Key]
        [Column("ROL_CODE")]
        public short Code { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>
        [Column("ROL_NOMBRE")]
        public string Nombre { get; set; }

        /// <summary>
        /// Visibilidad
        /// </summary>
        [Column("ROL_VISIBLE")]
        public bool Visible { get; set; }

        /// <summary>
        /// Fecha de creacion
        /// </summary>
        [Column("ROL_CREATEDATE")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Fecha de actualizacion
        /// </summary>
        [Column("ROL_UPDATEDATE")]
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// Usuario que crea
        /// </summary>
        [Column("ROL_CREATEUSER")]
        public string CreateUser { get; set; }

        /// <summary>
        /// Usuario que actualiza
        /// </summary>
        [Column("ROL_UPDATEUSER")]
        public string UpdateUser { get; set; }
    }
}
