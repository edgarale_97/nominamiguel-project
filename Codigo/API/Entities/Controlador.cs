﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities
{
    /// <summary>
    /// Nombre de la clase: Controlador
    /// </summary>
    [Table("CON_CONTROLADOR")]
    public class Controlador
    {
        /// <summary>
        /// Codigo
        /// </summary>
        [Key]
        [Column("CON_CODE")]
        public int Codigo { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>
        [Column("CON_NOMBRE")]
        public string Nombre { get; set; }

        /// <summary>
        /// Visibilidad
        /// </summary>
        [Column("CON_VISIBLE")]
        public bool Visibilidad { get; set; }

        /// <summary>
        /// Fecha de creacion
        /// </summary>
        [Column("CON_CREATEDATE")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Fecha de actualizacion
        /// </summary>
        [Column("CON_UPDATEDATE")]
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// Usuario que crea
        /// </summary>
        [Column("CON_CREATEUSER")]
        public string CreateUser { get; set; }

        /// <summary>
        /// Usuario que actualiza
        /// </summary>
        [Column("CON_UPDATEUSER")]
        public string UpdateUser { get; set; }
    }
}
