﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities
{
    /// <summary>
    /// Nombre de la clase: TipoTratamiento
    /// </summary>
    [Table("TITRA_TIPOSTRATAMIENTO")]
    public class TipoTratamiento
    {
        /// <summary>
        /// Codigo autoincremental
        /// </summary>
        [Display(Name = "Código")]
        [Key]
        [Column("TITRA_CODE")]
        public int Code { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>
        [Display(Name = "Nombre")]
        [Column("TITRA_NOMBRE")]
        public string Nombre { get; set; }

        /// <summary>
        /// Descripcion
        /// </summary>
        [Display(Name = "Descripción")]
        [Column("TITRA_DESCRIPCION")]
        public string Descripcion { get; set; }

        /// <summary>
        /// Indica si es visible
        /// </summary>
        [Display(Name = "Visible")]
        [Column("TITRA_VISIBLE")]
        public bool Visible { get; set; }

        /// <summary>
        /// Fecha de creacion
        /// </summary>
        [Display(Name = "Creación")]
        [Column("TITRA_CREATEDATE")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Usuario que crea
        /// </summary>
        [Display(Name = "Creado por")]
        [Column("TITRA_CREATEUSER")]
        public string CreateUser { get; set; }

        /// <summary>
        /// Fecha de actualizacion
        /// </summary>
        [Display(Name = "Actualización")]
        [Column("TITRA_UPDATEDATE")]
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// Usuario que actualiza
        /// </summary>
        [Display(Name = "Actualizado por")]
        [Column("TITRA_UPDATEUSER")]
        public string UpdateUser { get; set; }
    }
}
