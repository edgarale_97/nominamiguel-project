﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities
{
    /// <summary>
    /// Nombre de la clase: Empresa
    /// </summary>
    [Table("EMP_EMPRESA")]
    public class Empresa
    {
        /// <summary>
        /// Codigo
        /// </summary>
        [Key]
        [Column("EMP_CODE")]
        public short Code { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>
        [Column("EMP_NAME")]
        public string Nombre { get; set; }

        /// <summary>
        /// Telefono
        /// </summary>
        [Column("EMP_TELEFONO")]
        public string Telefono { get; set; }

        /// <summary>
        /// Fax
        /// </summary>
        [Column("EMP_FAX")]
        public string Fax { get; set; }

        /// <summary>
        /// Correo electronico
        /// </summary>
        [Column("EMP_MAIL")]
        public string Correo { get; set; }

        /// <summary>
        /// Url website
        /// </summary>
        [Column("EMP_URL")]
        public string Url { get; set; }

        /// <summary>
        /// Direccion
        /// </summary>
        [Column("EMP_DIRECCION")]
        public string Direccion { get; set; }

        /// <summary>
        /// Cedula juridica
        /// </summary>
        [Column("EMP_CEDULAJURIDICA")]
        public string CedulaJuridica { get; set; }

        /// <summary>
        /// Descripcion
        /// </summary>
        [Column("EMP_DESCRIPCION")]
        public string Descripcion { get; set; }

        /// <summary>
        /// Logotipo
        /// </summary>
        [Column("EMP_LOGO")]
        public string Logo { get; set; }

        /// <summary>
        /// Visible
        /// </summary>
        [Column("EMP_VISIBLE")]
        public bool Visible { get; set; }

        /// <summary>
        /// Fecha de creacion
        /// </summary>
        [Column("EMP_CREATEDATE")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Fecha de actualizacion
        /// </summary>
        [Column("EMP_UPDATEDATE")]
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// Usuario que crea
        /// </summary>
        [Column("EMP_CREATEUSER")]
        public int CreateUser { get; set; }

        /// <summary>
        /// Usuario que actualiza
        /// </summary>
        [Column("EMP_UPDATEUSER")]
        public int UpdateUser { get; set; }
    }
}
