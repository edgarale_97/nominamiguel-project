﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
    public enum SalidaAccionEnum
    {
        Reproceso = 1,
        Cuarentena = 2,
        Scrap = 3
    }
}
