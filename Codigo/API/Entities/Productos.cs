﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities
{
    /// <summary>
    /// Nombre de la clase: Productos
    /// </summary>
    [Table("PROD_PRODUCTOQUIMICO")]
    public class Productos
    {
        /// <summary>
        /// Codigo
        /// </summary>
        [Key]
        [Column("PROD_CODE")]
        public short Code { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>
        [Column("PROD_NAME")]
        public string Nombre { get; set; }

        /// <summary>
        /// Fabricante
        /// </summary>
        [Column("PROD_FABRICANTE")]
        public string Fabricante { get; set; }

        /// <summary>
        /// Proveedor
        /// </summary>
        [Column("PROD_PROVEEDOR")]
        public string Proveedor { get; set; }

        /// <summary>
        /// Codigo Interno del producto
        /// </summary>
        [Column("PROD_CODINTPRODUCTO")]
        public string CodIntProducto { get; set; }

        /// <summary>
        /// Nro. de registro químico
        /// </summary>
        [Column("PROD_NROREGQUIMICO")]
        public string NroRegQuimico { get; set; }

        /// <summary>
        /// Precursor
        /// </summary>
        [Column("PROD_PRECURSOR")]
        public bool Precursor { get; set; }

        /// <summary>
        /// Hojas de seguridad
        /// </summary>
        [Column("PROD_HOJASEGURIDAD")]
        public string HojaSeguridad { get; set; }

        /// <summary>
        /// Peligrosidad
        /// </summary>
        [Column("PROD_PELIGROSIDAD")]
        public string Peligrosidad { get; set; }

        /// <summary>
        /// Permiso de venta
        /// </summary>
        [Column("PROD_PERMISOVENTA")]
        public string PermisoVenta { get; set; }

        /// <summary>
        /// Permiso de consumo
        /// </summary>
        [Column("PROD_PERMISOCONSUMO")]
        public string PermisoConsumo { get; set; }

        /// <summary>
        /// Material
        /// </summary>
        [Column("PROD_MATERIAL")]
        public string Material { get; set; }

        /// <summary>
        /// Volumen
        /// </summary>
        [Column("PROD_VOLUMEN")]
        public string Volumen { get; set; }

        /// <summary>
        /// Densidad
        /// </summary>
        [Column("PROD_DENSIDAD")]
        public string Densidad { get; set; }

        /// <summary>
        /// Tipo de residuo
        /// </summary>
        [Column("PROD_TIPORESIDUO")]
        public string TipoResiduo { get; set; }

        /// <summary>
        /// Gestor autorizado
        /// </summary>
        [Column("PROD_GESTORAUTH")]
        public string Gestor { get; set; }

        /// <summary>
        /// Fecha de creacion
        /// </summary>
        [Column("PROD_CREATEDATE")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Fecha de actualizacion
        /// </summary>
        [Column("PROD_UPDATEDATE")]
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// Usuario que crea
        /// </summary>
        [Column("PROD_CREATEUSER")]
        public int CreateUser { get; set; }

        /// <summary>
        /// Usuario que actualiza
        /// </summary>
        [Column("PROD_UPDATEUSER")]
        public int UpdateUser { get; set; }
    }
}
