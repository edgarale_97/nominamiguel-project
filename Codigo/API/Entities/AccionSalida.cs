﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities
{
    /// <summary>
    /// Nombre de la clase: AccionSalida
    /// </summary>
    [Table("ACCSA_ACCIONESSALIDA")]
    public class AccionSalida
    {
        /// <summary>
        /// Codigo autoincremental
        /// </summary>
        [Display(Name = "Código")]
        [Key]
        [Column("ACCSA_CODE")]
        public int Code { get; set; }

        /// <summary>
        /// Nombre
        /// </summary>
        [Display(Name = "Nombre")]
        [Column("ACCSA_NOMBRE")]
        public string Nombre { get; set; }

        /// <summary>
        /// Indica si es visible
        /// </summary>
        [Display(Name = "Visible")]
        [Column("ACCSA_VISIBLE")]
        public bool Visible { get; set; }

        /// <summary>
        /// Fecha de creacion
        /// </summary>
        [Display(Name = "Creación")]
        [Column("ACCSA_CREATEDATE")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Usuario que crea
        /// </summary>
        [Display(Name = "Creado por")]
        [Column("ACCSA_CREATEUSER")]
        public string CreateUser { get; set; }

        /// <summary>
        /// Fecha de actualizacion
        /// </summary>
        [Display(Name = "Actualización")]
        [Column("ACCSA_UPDATEDATE")]
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// Usuario que actualiza
        /// </summary>
        [Display(Name = "Actualizado por")]
        [Column("ACCSA_UPDATEUSER")]
        public string UpdateUser { get; set; }
    }
}
