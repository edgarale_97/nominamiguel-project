﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities
{
    /// <summary>
    /// Nombre de la clase: RolPermiso
    /// </summary>
    [Table("ROLPER_ROLES_PERMISOS")]
    public class RolPermiso
    {
        /// <summary>
        /// Codigo 
        /// </summary>
        [Key]
        [Column("ROLPER_CODE")]
        public string Code { get; set; }

        /// <summary>
        /// Codigo del rol 
        /// </summary>
        [Column("ROL_CODE")]
        public short RolCode { get; set; }

        /// <summary>
        /// Codigo del controlador
        /// </summary>
        [Column("CON_CODE")]
        public short ControladorCode { get; set; }

        /// <summary>
        /// Accion del controlador
        /// </summary>
        [Column("ACC_CODE")]
        public short AccionCode { get; set; }

        /// <summary>
        /// Visibilidad
        /// </summary>
        [Column("ROLPER_VISIBLE")]
        public bool Visibilidad { get; set; }

        /// <summary>
        /// Fecha de creacion
        /// </summary>
        [Column("ROLPER_CREATEDATE")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Fecha de actualizacion
        /// </summary>
        [Column("ROLPER_UPDATEDATE")]
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// Usuario que crea
        /// </summary>
        [Column("ROLPER_CREATEUSER")]
        public string CreateUser { get; set; }

        /// <summary>
        /// Usuario que actualiza
        /// </summary>
        [Column("ROLPER_UPDATEUSER")]
        public string UpdateUser { get; set; }
    }
}
