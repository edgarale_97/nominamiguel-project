﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business;
using Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    /// <summary>
    /// Nombre de la clase: OrdenesAnodizadoController
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class OrdenesAnodizadoController : ControllerBase
    {
        /// <summary>
        /// Se declara un objeto OrdenAnodizadoBus
        /// </summary>
        OrdenAnodizadoBus _OrdenAnodizadoBus = new OrdenAnodizadoBus();

        /// <summary>
        /// Metodo que consulta el listado de registros
        /// </summary>
        /// <returns>Se retorna el listado</returns>
        [HttpGet]
        public ActionResult Get()
        {
            try
            {
                var listado = _OrdenAnodizadoBus.Listado();

                return Ok(listado);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }


        /// <summary>
        /// Metodo que sirve para retornar un listado
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="Desde">desde</param>
        /// <param name="Hasta">hasta</param>
        /// <param name="EstadoRecepcion">estado recepcion</param>
        /// <param name="Cedula">cedula</param>
        /// <param name="RazonSocial">razon social</param>
        /// <returns>Retorna un listado</returns>
        [Produces("application/json")]
        [HttpGet("Buscar/{id}")]
        public ActionResult Buscar(int id, DateTime Desde, DateTime Hasta)
        {
            try
            {
                var listado = _OrdenAnodizadoBus.Buscar(Desde, Hasta);

                return Ok(listado);
            }
            catch (Exception ex)
            {
                return StatusCode(500);
            }
        }



        /// <summary>
        /// Metodo que consulta el listado de registros
        /// </summary>
        /// <returns>Se retorna el listado</returns>
        [HttpGet("ListadoReproceso")]
        public ActionResult ListadoReproceso()
        {
            try
            {
                var listado = _OrdenAnodizadoBus.ListadoReproceso();

                return Ok(listado);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Metodo que consulta un registro mediante su id
        /// </summary>
        /// <param name="id">Recibe el id</param>
        /// <returns>Retorna el registro consultado</returns>
        [HttpGet("{id}")]
        public ActionResult Get(short? id)
        {
            try
            {
                if (id == null)
                {
                    return StatusCode(400);
                }

                var registro = _OrdenAnodizadoBus.Buscar(id);

                if (registro == null)
                {
                    return StatusCode(400);
                }

                return StatusCode(200, registro);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }

        /// <summary>
        /// Metodo que crea un registro 
        /// </summary>
        /// <param name="ordenAnodizado">Recibe un objeto</param>
        /// <returns>Retorna el resultado del proceso</returns>
        [HttpPost]
        public ActionResult Post([FromBody]OrdenAnodizado ordenAnodizado)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    var secuencial = _OrdenAnodizadoBus.Crear(ordenAnodizado);
                    return StatusCode(200, secuencial);
                }

                return StatusCode(400);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }

        /// <summary>
        /// Metodo que crea un registro 
        /// </summary>
        /// <param name="ordenAnodizado">Recibe un objeto</param>
        /// <returns>Retorna el resultado del proceso</returns>
        [HttpPost("GuardarEncabezado")]
        public ActionResult GuardarEncabezado([FromBody]OrdenAnodizado ordenAnodizado)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _OrdenAnodizadoBus.GuardarEncabezado(ordenAnodizado);
                    return StatusCode(200);
                }

                return StatusCode(400);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Metodo que crea un registro 
        /// </summary>
        /// <param name="ordenAnodizado">Recibe un objeto</param>
        /// <returns>Retorna el resultado del proceso</returns>
        [HttpPost("GuardarInspeccionRecibo")]
        public ActionResult GuardarInspeccionRecibo([FromBody]OrdenAnodizado ordenAnodizado)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _OrdenAnodizadoBus.GuardarInspeccionRecibo(ordenAnodizado);
                    return StatusCode(200);
                }

                return StatusCode(400);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }

        /// <summary>
        /// Metodo que crea un registro 
        /// </summary>
        /// <param name="ordenAnodizado">Recibe un objeto</param>
        /// <returns>Retorna el resultado del proceso</returns>
        [HttpPost("GuardarRaqueo")]
        public ActionResult GuardarRaqueo([FromBody]OrdenAnodizado ordenAnodizado)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _OrdenAnodizadoBus.GuardarRaqueo(ordenAnodizado);
                    return StatusCode(200);
                }

                return StatusCode(400);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }


        /// <summary>
        /// Metodo que crea un registro 
        /// </summary>
        /// <param name="ordenAnodizado">Recibe un objeto</param>
        /// <returns>Retorna el resultado del proceso</returns>
        [HttpPost("GuardarProceso")]
        public ActionResult GuardarProceso([FromBody]OrdenAnodizado ordenAnodizado)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _OrdenAnodizadoBus.GuardarProceso(ordenAnodizado);
                    return StatusCode(200);
                }

                return StatusCode(400);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }


        /// <summary>
        /// Metodo que crea un registro 
        /// </summary>
        /// <param name="ordenAnodizado">Recibe un objeto</param>
        /// <returns>Retorna el resultado del proceso</returns>
        [HttpPost("GuardarSalida")]
        public ActionResult GuardarSalida([FromBody]OrdenAnodizado ordenAnodizado)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _OrdenAnodizadoBus.GuardarSalida(ordenAnodizado);
                    return StatusCode(200);
                }

                return StatusCode(400);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }

        /// <summary>
        /// Metodo que crea un registro 
        /// </summary>
        /// <param name="ordenAnodizado">Recibe un objeto</param>
        /// <returns>Retorna el resultado del proceso</returns>
        [HttpPost("GuardarEmpaque")]
        public ActionResult GuardarEmpaque([FromBody]OrdenAnodizado ordenAnodizado)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _OrdenAnodizadoBus.GuardarEmpaque(ordenAnodizado);
                    return StatusCode(200);
                }

                return StatusCode(400);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }

        /// <summary>
        /// Metodo que crea un registro 
        /// </summary>
        /// <param name="ordenAnodizado">Recibe un objeto</param>
        /// <returns>Retorna el resultado del proceso</returns>
        [HttpPost("GuardarDespacho")]
        public ActionResult GuardarDespacho([FromBody]OrdenAnodizado ordenAnodizado)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _OrdenAnodizadoBus.GuardarDespacho(ordenAnodizado);
                    return StatusCode(200);
                }

                return StatusCode(400);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }

        /// <summary>
        /// Metodo que sirve para editar el registro
        /// </summary>
        /// <param name="controlador">Recibe un objeto</param>
        /// <returns>Retorna el resultado del proceso</returns>
        [HttpPut]
        public ActionResult Put([FromBody]OrdenAnodizado ordenAnodizado)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    _OrdenAnodizadoBus.Editar(ordenAnodizado);
                    return StatusCode(200);
                }
                return StatusCode(400);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }


        /// <summary>
        /// Metodo que sirve para borrar un registro
        /// </summary>
        /// <param name="id">Recibe el id del registro</param>
        /// <returns>Retorna el resultado del proceso</returns>
        [HttpDelete("{id}")]
        public ActionResult Delete(short? id)
        {

            try
            {
                if (id == null)
                {
                    return StatusCode(400);
                }

                var registro = _OrdenAnodizadoBus.Buscar(id);

                if (registro == null)
                {
                    return StatusCode(400);
                }

                _OrdenAnodizadoBus.Borrar(registro);

                return StatusCode(200);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }
    }
}