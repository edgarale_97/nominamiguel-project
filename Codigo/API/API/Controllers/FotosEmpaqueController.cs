﻿using Business;
using Entities;
using Microsoft.AspNetCore.Mvc;
using System;

namespace API.Controllers
{
    /// <summary>
    /// Nombre de la clase: FotosEmpaqueController
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class FotosEmpaqueController : ControllerBase
    {
        // <summary>
        /// Se declara un objeto FotoEmpaqueBus
        /// </summary>
        FotoEmpaqueBus _FotoEmpaqueBus = new FotoEmpaqueBus();

        /// <summary>
        /// Metodo que consulta el listado de registros
        /// </summary>
        /// <returns>Se retorna el listado</returns>
        [HttpGet]
        public ActionResult Get()
        {
            try
            {
                var listado = _FotoEmpaqueBus.Listado();

                return Ok(listado);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Obtiene el listado de fotos respectivo a la ficha
        /// </summary>
        /// <param name="fichaCode">Code de la ficha</param>
        /// <returns>Retorna el listado</returns>
        [HttpGet("FichaTecnica/{id}")]
        public ActionResult Get(int id)
        {
            try
            {
                var listado = _FotoEmpaqueBus.ListadoFicha(id);

                return Ok(listado);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Metodo que consulta un registro mediante su id
        /// </summary>
        /// <param name="id">Recibe el id</param>
        /// <returns>Retorna el registro consultado</returns>
        [HttpGet("{id}")]
        public ActionResult Get(short? id)
        {
            try
            {
                if (id == null)
                {
                    return StatusCode(400);
                }

                var registro = _FotoEmpaqueBus.Buscar(id);

                if (registro == null)
                {
                    return StatusCode(400);
                }

                return StatusCode(200, registro);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }

        /// <summary>
        /// Metodo que crea un registro 
        /// </summary>
        /// <param name="fotoEmpaque">Recibe un objeto</param>
        /// <returns>Retorna el resultado del proceso</returns>
        [HttpPost]
        public ActionResult Post([FromBody]FotoEmpaque fotoEmpaque)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    _FotoEmpaqueBus.Crear(fotoEmpaque);
                    return StatusCode(200);
                }

                return StatusCode(400);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }

        /// <summary>
        /// Metodo que sirve para editar el registro
        /// </summary>
        /// <param name="controlador">Recibe un objeto</param>
        /// <returns>Retorna el resultado del proceso</returns>
        [HttpPut]
        public ActionResult Put([FromBody]FotoEmpaque fotoEmpaque)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    _FotoEmpaqueBus.Editar(fotoEmpaque);
                    return StatusCode(200);
                }
                return StatusCode(400);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }


        /// <summary>
        /// Metodo que sirve para borrar un registro
        /// </summary>
        /// <param name="id">Recibe el id del registro</param>
        /// <returns>Retorna el resultado del proceso</returns>
        [HttpDelete("{id}")]
        public ActionResult Delete(short? id)
        {

            try
            {
                if (id == null)
                {
                    return StatusCode(400);
                }

                var registro = _FotoEmpaqueBus.Buscar(id);

                if (registro == null)
                {
                    return StatusCode(400);
                }

                _FotoEmpaqueBus.Borrar(registro);

                return StatusCode(200);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }
    }
}