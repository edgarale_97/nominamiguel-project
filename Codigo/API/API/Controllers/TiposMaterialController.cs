﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business;
using Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    /// <summary>
    /// Nombre de la clase: TiposMaterialController
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class TiposMaterialController : ControllerBase
    {
        /// <summary>
        /// Se declara un objeto TipoMaterialBus
        /// </summary>
        TipoMaterialBus _TipoMaterialBus = new TipoMaterialBus();

        /// <summary>
        /// Metodo que consulta el listado de registros
        /// </summary>
        /// <returns>Se retorna el listado</returns>
        [HttpGet]
        public ActionResult Get()
        {
            try
            {
                var listado = _TipoMaterialBus.Listado();

                return Ok(listado);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Metodo que consulta un registro mediante su id
        /// </summary>
        /// <param name="id">Recibe el id</param>
        /// <returns>Retorna el registro consultado</returns>
        [HttpGet("{id}")]
        public ActionResult Get(int? id)
        {
            try
            {
                if (id == null)
                {
                    return StatusCode(400);
                }

                var registro = _TipoMaterialBus.Buscar(id);

                if (registro == null)
                {
                    return StatusCode(400);
                }

                return StatusCode(200, registro);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }

        /// <summary>
        /// Metodo que crea un registro 
        /// </summary>
        /// <param name="tipoMaterial">Recibe un objeto</param>
        /// <returns>Retorna el resultado del proceso</returns>
        [HttpPost]
        public ActionResult Post([FromBody]TipoMaterial tipoMaterial)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _TipoMaterialBus.Crear(tipoMaterial);
                    return StatusCode(200);
                }

                return StatusCode(400);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }

        /// <summary>
        /// Metodo que sirve para editar el registro
        /// </summary>
        /// <param name="tipoMaterial">Recibe un objeto</param>
        /// <returns>Retorna el resultado del proceso</returns>
        [HttpPut]
        public ActionResult Put([FromBody]TipoMaterial tipoMaterial)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _TipoMaterialBus.Editar(tipoMaterial);
                    return StatusCode(200);
                }
                return StatusCode(400);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }


        /// <summary>
        /// Metodo que sirve para borrar un registro
        /// </summary>
        /// <param name="id">Recibe el id del registro</param>
        /// <returns>Retorna el resultado del proceso</returns>
        [HttpDelete("{id}")]
        public ActionResult Delete(int? id)
        {

            try
            {
                if (id == null)
                {
                    return StatusCode(400);
                }

                var registro = _TipoMaterialBus.Buscar(id);

                if (registro == null)
                {
                    return StatusCode(400);
                }

                _TipoMaterialBus.Borrar(registro);

                return StatusCode(200);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }
    }
}