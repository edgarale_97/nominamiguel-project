﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business;
using Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    /// <summary>
    /// Nombre de la clase: UsuariosController
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class UsuariosController : ControllerBase
    {
        /// <summary>
        /// Se declara un objeto UsuarioBus
        /// </summary>
        UsuarioBus _UsuarioBus = new UsuarioBus();

        /// <summary>
        /// Metodo que consulta el listado de registros
        /// </summary>
        /// <returns>Se retorna el listado</returns>
        [HttpGet]
        public ActionResult Get()
        {
            try
            {
                var listado = _UsuarioBus.Listado();

                return Ok(listado);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Metodo que consulta un registro mediante su id
        /// </summary>
        /// <param name="id">Recibe el id</param>
        /// <returns>Retorna el registro consultado</returns>
        [HttpGet("{id}")]
        public ActionResult Get(short? id)
        {
            try
            {
                if (id == null)
                {
                    return StatusCode(400);
                }

                var registro = _UsuarioBus.Buscar(id);

                if (registro == null)
                {
                    return StatusCode(400);
                }

                return StatusCode(200, registro);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }

        /// <summary>
        /// Metodo que crea un registro 
        /// </summary>
        /// <param name="usuario">Recibe un objeto</param>
        /// <returns>Retorna el resultado del proceso</returns>
        [HttpPost]
        public ActionResult Post([FromBody]Usuario usuario)
        {
            try
            {
                usuario.Visible = true;
                usuario.CreateDate = DateTime.Now;
                usuario.CreateUser = "SYSTEM";
                usuario.UpdateUser = null;
                usuario.UpdateDate = null;

                if (ModelState.IsValid)
                {
                    _UsuarioBus.Crear(usuario);
                    return StatusCode(200);
                }

                return StatusCode(400);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }

        /// <summary>
        /// Metodo que sirve para editar el registro
        /// </summary>
        /// <param name="usuario">Recibe un objeto</param>
        /// <returns>Retorna el resultado del proceso</returns>
        [HttpPut]
        public ActionResult Put([FromBody]Usuario usuario)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _UsuarioBus.Editar(usuario);
                    return StatusCode(200);
                }
                return StatusCode(400);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }


        /// <summary>
        /// Metodo que sirve para borrar un registro
        /// </summary>
        /// <param name="id">Recibe el id del registro</param>
        /// <returns>Retorna el resultado del proceso</returns>
        [HttpDelete("{id}")]
        public ActionResult Delete(short? id)
        {

            try
            {
                if (id == null)
                {
                    return StatusCode(400);
                }

                var registro = _UsuarioBus.Buscar(id);

                if (registro == null)
                {
                    return StatusCode(400);
                }

                _UsuarioBus.Borrar(registro);

                return StatusCode(200);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}