﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business;
using Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    /// <summary>
    /// Nombre la clase: AccionesController
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class AccionesController : ControllerBase
    {
        /// <summary>
        /// Se declara un objeto AccionBus
        /// </summary>
        AccionBus _AccionBus = new AccionBus();


        /// <summary>
        /// Metodo que consulta el listado de registros
        /// </summary>
        /// <returns>Se retorna el listado</returns>
        [HttpGet]
        public ActionResult Get()
        {
            try
            {
                var listado = _AccionBus.Listado();

                return Ok(listado);
            }
            catch(Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Metodo que consulta un registro mediante su id
        /// </summary>
        /// <param name="id">Recibe el id</param>
        /// <returns>Retorna el registro consultado</returns>
        [HttpGet("{id}")]
        public ActionResult Get(short? id)
        {
            try
            {
                if (id == null)
                {
                    return StatusCode(400);
                }

                var registro = _AccionBus.Buscar(id);

                if (registro == null)
                {
                    return StatusCode(400);
                }

                return StatusCode(200, registro);
            }
            catch(Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
          
        }

        /// <summary>
        /// Metodo que crea un registro 
        /// </summary>
        /// <param name="accion">Recibe un objeto</param>
        /// <returns>Retorna el resultado del proceso</returns>
        [HttpPost]
        public ActionResult Post([FromBody]Accion accion)
        {
            try
            {
                accion.Visible = true;
                accion.CreateDate = DateTime.Now;
                accion.CreateUser = "SYSTEM";
                accion.UpdateUser = "";

                if (ModelState.IsValid)
                {
                    _AccionBus.Crear(accion);
                    return StatusCode(200);
                }

                return StatusCode(400);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }

        /// <summary>
        /// Metodo que sirve para editar el registro
        /// </summary>
        /// <param name="accion">Recibe un objeto</param>
        /// <returns>Retorna el resultado del proceso</returns>
        [HttpPut]
        public ActionResult Put([FromBody]Accion accion)
        {
            try
            {
                accion.UpdateUser = "SYSTEM";
                accion.UpdateDate = DateTime.Now;

                if (ModelState.IsValid)
                {
                    _AccionBus.Editar(accion);
                    return StatusCode(200);
                }
                return StatusCode(400);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }


        /// <summary>
        /// Metodo que sirve para borrar un registro
        /// </summary>
        /// <param name="id">Recibe el id del registro</param>
        /// <returns>Retorna el resultado del proceso</returns>
        [HttpDelete("{id}")]
        public ActionResult Delete(short? id)
        {
    
            try
            {
                if (id == null)
                {
                    return StatusCode(400);
                }

                var registro = _AccionBus.Buscar(id);

                if (registro == null)
                {
                    return StatusCode(400);
                }

                _AccionBus.Borrar(registro);

                return StatusCode(200);
            }
            catch(Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }
    }
}