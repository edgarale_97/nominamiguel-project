﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business;
using Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RolesController : ControllerBase
    {
        /// <summary>
        /// Se declara un objeto RolBus
        /// </summary>
        RolBus _RolBus = new RolBus();

        /// <summary>
        /// Metodo que consulta el listado de registros
        /// </summary>
        /// <returns>Se retorna el listado</returns>
        [HttpGet]
        public ActionResult Get()
        {
            try
            {
                var listado = _RolBus.Listado();

                return Ok(listado);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Metodo que consulta un registro mediante su id
        /// </summary>
        /// <param name="id">Recibe el id</param>
        /// <returns>Retorna el registro consultado</returns>
        [HttpGet("{id}")]
        public ActionResult Get(short? id)
        {
            try
            {
                if (id == null)
                {
                    return StatusCode(400);
                }

                var registro = _RolBus.Buscar(id);

                if (registro == null)
                {
                    return StatusCode(400);
                }

                return StatusCode(200, registro);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }

        /// <summary>
        /// Metodo que crea un registro 
        /// </summary>
        /// <param name="rol">Recibe un objeto</param>
        /// <returns>Retorna el resultado del proceso</returns>
        [HttpPost]
        public ActionResult Post([FromBody]Rol rol)
        {
            try
            {
                rol.Visible = true;
                rol.CreateDate = DateTime.Now;
                rol.CreateUser = "SYSTEM";
                rol.UpdateUser = "";

                if (ModelState.IsValid)
                {
                    _RolBus.Crear(rol);
                    return StatusCode(200);
                }

                return StatusCode(400);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }

        /// <summary>
        /// Metodo que sirve para editar el registro
        /// </summary>
        /// <param name="rol">Recibe un objeto</param>
        /// <returns>Retorna el resultado del proceso</returns>
        [HttpPut]
        public ActionResult Put([FromBody]Rol rol)
        {
            try
            {
                rol.UpdateUser = "SYSTEM";
                rol.UpdateDate = DateTime.Now;

                if (ModelState.IsValid)
                {
                    _RolBus.Editar(rol);
                    return StatusCode(200);
                }
                return StatusCode(400);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }


        /// <summary>
        /// Metodo que sirve para borrar un registro
        /// </summary>
        /// <param name="id">Recibe el id del registro</param>
        /// <returns>Retorna el resultado del proceso</returns>
        [HttpDelete("{id}")]
        public ActionResult Delete(short? id)
        {

            try
            {
                if (id == null)
                {
                    return StatusCode(400);
                }

                var registro = _RolBus.Buscar(id);

                if (registro == null)
                {
                    return StatusCode(400);
                }

                _RolBus.Borrar(registro);

                return StatusCode(200);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}