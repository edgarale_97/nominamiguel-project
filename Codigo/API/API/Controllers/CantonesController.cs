﻿using Business;
using Entities;
using Microsoft.AspNetCore.Mvc;
using System;

namespace API.Controllers
{
    /// <summary>
    /// Nombre de la clase: CantonesController
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class CantonesController : ControllerBase
    {
        /// <summary>
        /// Se declara un objeto _CantonBus
        /// </summary>
        CantonBus _CantonBus = new CantonBus();

        /// <summary>
        /// Metodo que consulta el listado de registros
        /// </summary>
        /// <returns>Se retorna el listado</returns>
        [HttpGet]
        public ActionResult Get()
        {
            try
            {
                var listado = _CantonBus.Listado();

                return Ok(listado);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Metodo que consulta un registro mediante su id
        /// </summary>
        /// <param name="id">Recibe el id</param>
        /// <returns>Retorna el registro consultado</returns>
        [HttpGet("Buscar")]
        public ActionResult Get(short? provinciaCode, short? cantonCode)
        {
            try
            {
                if (provinciaCode == null || cantonCode == null)
                {
                    return StatusCode(400);
                }

                var registro = _CantonBus.Buscar(provinciaCode, cantonCode);

                if (registro == null)
                {
                    return StatusCode(400);
                }

                return StatusCode(200, registro);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }

        /// <summary>
        /// Metodo que crea un registro 
        /// </summary>
        /// <param name="provincia">Recibe un objeto</param>
        /// <returns>Retorna el resultado del proceso</returns>
        [HttpPost]
        public ActionResult Post([FromBody]Canton canton)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _CantonBus.Crear(canton);
                    return StatusCode(200);
                }

                return StatusCode(400);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }

        /// <summary>
        /// Metodo que sirve para editar el registro
        /// </summary>
        /// <param name="canton">Recibe un objeto</param>
        /// <returns>Retorna el resultado del proceso</returns>
        [HttpPut]
        public ActionResult Put([FromBody]Canton canton)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _CantonBus.Editar(canton);
                    return StatusCode(200);
                }
                return StatusCode(400);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }


        /// <summary>
        /// Metodo que sirve para borrar un registro
        /// </summary>
        /// <param name="id">Recibe el id del registro</param>
        /// <returns>Retorna el resultado del proceso</returns>
        [HttpDelete("{id}")]
        public ActionResult Delete(short? provinciaCode, short? cantonCode)
        {

            try
            {
                if (provinciaCode == null || cantonCode == null)
                {
                    return StatusCode(400);
                }

                var registro = _CantonBus.Buscar(provinciaCode, cantonCode);

                if (registro == null)
                {
                    return StatusCode(400);
                }

                _CantonBus.Borrar(registro);

                return StatusCode(200);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }
    }
}