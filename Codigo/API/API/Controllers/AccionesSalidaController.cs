﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business;
using Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    /// <summary>
    /// Nombre de la clase: AccionesSalidaController
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class AccionesSalidaController : ControllerBase
    {
        /// <summary>
        /// Se declara un objeto _AccionSalidaBus
        /// </summary>
        AccionSalidaBus _AccionSalidaBus = new AccionSalidaBus();

        /// <summary>
        /// Metodo que consulta el listado de registros
        /// </summary>
        /// <returns>Se retorna el listado</returns>
        [HttpGet]
        public ActionResult Get()
        {
            try
            {
                var listado = _AccionSalidaBus.Listado();

                return Ok(listado);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Metodo que consulta un registro mediante su id
        /// </summary>
        /// <param name="id">Recibe el id</param>
        /// <returns>Retorna el registro consultado</returns>
        [HttpGet("{id}")]
        public ActionResult Get(short? id)
        {
            try
            {
                if (id == null)
                {
                    return StatusCode(400);
                }

                var registro = _AccionSalidaBus.Buscar(id);

                if (registro == null)
                {
                    return StatusCode(400);
                }

                return StatusCode(200, registro);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }

        /// <summary>
        /// Metodo que crea un registro 
        /// </summary>
        /// <param name="accionSalida">Recibe un objeto</param>
        /// <returns>Retorna el resultado del proceso</returns>
        [HttpPost]
        public ActionResult Post([FromBody]AccionSalida accionSalida)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _AccionSalidaBus.Crear(accionSalida);
                    return StatusCode(200);
                }

                return StatusCode(400);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }

        /// <summary>
        /// Metodo que sirve para editar el registro
        /// </summary>
        /// <param name="accionSalida">Recibe un objeto</param>
        /// <returns>Retorna el resultado del proceso</returns>
        [HttpPut]
        public ActionResult Put([FromBody]AccionSalida accionSalida)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _AccionSalidaBus.Editar(accionSalida);
                    return StatusCode(200);
                }
                return StatusCode(400);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }


        /// <summary>
        /// Metodo que sirve para borrar un registro
        /// </summary>
        /// <param name="id">Recibe el id del registro</param>
        /// <returns>Retorna el resultado del proceso</returns>
        [HttpDelete("{id}")]
        public ActionResult Delete(short? id)
        {

            try
            {
                if (id == null)
                {
                    return StatusCode(400);
                }

                var registro = _AccionSalidaBus.Buscar(id);

                if (registro == null)
                {
                    return StatusCode(400);
                }

                _AccionSalidaBus.Borrar(registro);

                return StatusCode(200);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }
    }
}