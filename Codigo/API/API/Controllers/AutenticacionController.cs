﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AutenticacionController : ControllerBase
    {
        private UsuarioBus usuarioBus = new UsuarioBus();

        [HttpGet("Login")]
        public ActionResult Get(string nombreusuario, string password)
        {
            try
            {
                var usuario = usuarioBus.Listado().Where(p => p.NombreUsuario == nombreusuario && p.Contrasenia == password).FirstOrDefault();

                if (usuario != null)
                {
                    General.currentUser = usuario;
                    return Ok(usuario);
                }
                return StatusCode(401);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return StatusCode(500);
            }
        }
    }
}