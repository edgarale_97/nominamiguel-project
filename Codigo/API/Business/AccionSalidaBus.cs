﻿using Data;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    /// <summary>
    /// Nombre de la clase: AccionSalidaBus
    /// </summary>
    public class AccionSalidaBus
    {
        /// <summary>
        /// Se declara un objeto Context
        /// </summary>
        private Context db;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public AccionSalidaBus()
        {
            db = new Context();
        }

        /// <summary>
        /// Metodo que sirve para consultar el listado
        /// </summary>
        /// <returns>Retorna el listado</returns>
        public List<AccionSalida> Listado()
        {
            return db.AccionesSalida.ToList();
        }

        /// <summary>
        /// Metodo que sirve para consultar un registro mediante su id
        /// </summary>
        /// <param name="id">Recibe el id del registro</param>
        /// <returns>Retorna el registro consultado</returns>
        public AccionSalida Buscar(short? id)
        {
            return db.AccionesSalida.Find(id);
        }

        /// <summary>
        /// Metodo que sirve para agregar un registro
        /// </summary>
        /// <param name="accionSalida">Recibe un objeto</param>
        public void Crear(AccionSalida accionSalida)
        {
            db.AccionesSalida.Add(accionSalida);
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para actualizar un registro
        /// </summary>
        /// <param name="accionSalida">Recibe un objeto</param>
        public void Editar(AccionSalida accionSalida)
        {
            db.Entry(accionSalida).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para borrar un registro
        /// </summary>
        /// <param name="accionSalida">Recibe un objeto</param>
        public void Borrar(AccionSalida accionSalida)
        {
            db.AccionesSalida.Remove(accionSalida);
            db.SaveChanges();
        }
    }
}
