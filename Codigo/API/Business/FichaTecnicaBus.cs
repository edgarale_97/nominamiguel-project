﻿using Data;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    /// <summary>
    /// Nombre  de la clase: FichaTecnicaBus
    /// </summary>
    public class FichaTecnicaBus
    {
        /// <summary>
        /// Se declara un objeto Context
        /// </summary>
        private Context db;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public FichaTecnicaBus()
        {
            db = new Context();
        }

        /// <summary>
        /// Metodo que sirve para consultar el listado
        /// </summary>
        /// <returns>Retorna el listado</returns>
        public List<FichaTecnica> Listado()
        {
            return db.FichasTecnicas.ToList();
        }


        /// <summary>
        /// Metodo que sirve para consultar el listado
        /// </summary>
        /// <returns>Retorna el listado</returns>
        public List<FichaTecnica> ListadoDelCliente(int clienteCode)
        {
            return db.FichasTecnicas.Where(p => p.ClienteCode == clienteCode).ToList();
        }

        /// <summary>
        /// Metodo que sirve para consultar un registro mediante su id
        /// </summary>
        /// <param name="id">Recibe el id del registro</param>
        /// <returns>Retorna el registro consultado</returns>
        public FichaTecnica Buscar(int? id)
        {
            var fichaTecnica = db.FichasTecnicas.Find(id);

            //Se declara instancia los objetos que manejan los fotos de la ficha
            var fotoReciboBus = new FotoReciboBus();
            var fotoRaqueoBus = new FotoRaqueoBus();
            var fotoEmpaqueBus = new FotoEmpaqueBus();
            var fotoSalidaBus = new FotoSalidaBus();

            //se cargan las fotos de la ficha
            fichaTecnica.FotosRecibo = fotoReciboBus.ListadoFicha(fichaTecnica.Code);
            fichaTecnica.FotosRaqueo = fotoRaqueoBus.ListadoFicha(fichaTecnica.Code);
            fichaTecnica.FotosEmpaque = fotoEmpaqueBus.ListadoFicha(fichaTecnica.Code);
            fichaTecnica.FotosSalida = fotoSalidaBus.ListadoFicha(fichaTecnica.Code);

            return fichaTecnica;
        }

        /// <summary>
        /// Metodo que sirve para agregar un registro
        /// </summary>
        /// <param name="fichatecnica">Recibe un objeto</param>
        public int Crear(FichaTecnica fichatecnica)
        {
            db.FichasTecnicas.Add(fichatecnica);
            db.SaveChanges();

            return fichatecnica.Code;
        }

        /// <summary>
        /// Metodo que sirve para actualizar un registro
        /// </summary>
        /// <param name="fichaTecnica">Recibe un objeto</param>
        public void Editar(FichaTecnica fichaTecnica)
        {
            db.Entry(fichaTecnica).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            db.SaveChanges();

            //Se declara instancia los objetos que manejan los fotos de la ficha
            var fotoReciboBus = new FotoReciboBus();
            var fotoRaqueoBus = new FotoRaqueoBus();
            var fotoEmpaqueBus = new FotoEmpaqueBus();
            var fotoSalidaBus = new FotoSalidaBus();

            //se cargan las fotos de la ficha
            if(fichaTecnica.FotosRecibo != null)
            {
                fotoReciboBus.EditarPorFicha(fichaTecnica.Code, fichaTecnica.FotosRecibo.ToList());
            }

            if (fichaTecnica.FotosRaqueo != null)
            {
                fotoRaqueoBus.EditarPorFicha(fichaTecnica.Code, fichaTecnica.FotosRaqueo.ToList());
            }

            if (fichaTecnica.FotosEmpaque != null)
            {
                fotoEmpaqueBus.EditarPorFicha(fichaTecnica.Code, fichaTecnica.FotosEmpaque.ToList());
            }

            if (fichaTecnica.FotosSalida != null)
            {
                fotoSalidaBus.EditarPorFicha(fichaTecnica.Code, fichaTecnica.FotosSalida.ToList());
            }

        }

        /// <summary>
        /// Metodo que sirve para borrar un registro
        /// </summary>
        /// <param name="fichaTecnica">Recibe un objeto</param>
        public void Borrar(FichaTecnica fichaTecnica)
        {
            db.FichasTecnicas.Remove(fichaTecnica);
            db.SaveChanges();
        }
    }
}
