﻿using Data;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    /// <summary>
    /// Nombre de la clase: TipoMaterialBus
    /// </summary>
    public class TipoMaterialBus
    {
        /// <summary>
        /// Se declara un objeto Context
        /// </summary>
        private Context db;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public TipoMaterialBus()
        {
            db = new Context();
        }

        /// <summary>
        /// Metodo que sirve para consultar el listado
        /// </summary>
        /// <returns>Retorna el listado</returns>
        public List<TipoMaterial> Listado()
        {
            return db.TiposMaterial.ToList();
        }

        /// <summary>
        /// Metodo que sirve para consultar un registro mediante su id
        /// </summary>
        /// <param name="id">Recibe el id del registro</param>
        /// <returns>Retorna el registro consultado</returns>
        public TipoMaterial Buscar(int? id)
        {
            return db.TiposMaterial.Find(id);
        }

        /// <summary>
        /// Metodo que sirve para agregar un registro
        /// </summary>
        /// <param name="tipoMaterial">Recibe un objeto</param>
        public void Crear(TipoMaterial tipoMaterial)
        {
            db.TiposMaterial.Add(tipoMaterial);
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para actualizar un registro
        /// </summary>
        /// <param name="tipoMaterial">Recibe un objeto</param>
        public void Editar(TipoMaterial tipoMaterial)
        {
            db.Entry(tipoMaterial).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para borrar un registro
        /// </summary>
        /// <param name="tipoMaterial">Recibe un objeto</param>
        public void Borrar(TipoMaterial tipoMaterial)
        {
            db.TiposMaterial.Remove(tipoMaterial);
            db.SaveChanges();
        }
    }
}
