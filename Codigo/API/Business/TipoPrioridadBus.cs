﻿using Data;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    /// <summary>
    /// Nombre de la clase: TipoPrioridadBus
    /// </summary>
    public class TipoPrioridadBus
    {
        /// <summary>
        /// Se declara un objeto Context
        /// </summary>
        private Context db;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public TipoPrioridadBus()
        {
            db = new Context();
        }

        /// <summary>
        /// Metodo que sirve para consultar el listado
        /// </summary>
        /// <returns>Retorna el listado</returns>
        public List<TipoPrioridad> Listado()
        {
            return db.TiposPrioridad.ToList();
        }

        /// <summary>
        /// Metodo que sirve para consultar un registro mediante su id
        /// </summary>
        /// <param name="id">Recibe el id del registro</param>
        /// <returns>Retorna el registro consultado</returns>
        public TipoPrioridad Buscar(int? id)
        {
            return db.TiposPrioridad.Find(id);
        }

        /// <summary>
        /// Metodo que sirve para agregar un registro
        /// </summary>
        /// <param name="tipoPrioridad">Recibe un objeto</param>
        public void Crear(TipoPrioridad tipoPrioridad)
        {
            db.TiposPrioridad.Add(tipoPrioridad);
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para actualizar un registro
        /// </summary>
        /// <param name="tipoPrioridad">Recibe un objeto</param>
        public void Editar(TipoPrioridad tipoPrioridad)
        {
            db.Entry(tipoPrioridad).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para borrar un registro
        /// </summary>
        /// <param name="tipoPrioridad">Recibe un objeto</param>
        public void Borrar(TipoPrioridad tipoPrioridad)
        {
            db.TiposPrioridad.Remove(tipoPrioridad);
            db.SaveChanges();
        }
    }
}
