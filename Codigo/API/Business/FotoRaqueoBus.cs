﻿using Data;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    /// <summary>
    /// Nombre de la clase: FotoRaqueoBus
    /// </summary>
    public class FotoRaqueoBus
    {
        /// <summary>
        /// Se declara un objeto Context
        /// </summary>
        private Context db;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public FotoRaqueoBus()
        {
            db = new Context();
        }

        /// <summary>
        /// Metodo que sirve para consultar el listado
        /// </summary>
        /// <returns>Retorna el listado</returns>
        public List<FotoRaqueo> Listado()
        {
            return db.FotosRaqueo.ToList();
        }

        /// <summary>
        /// Metodo que sirve para consultar el listado respectivo a la ficha
        /// </summary>
        /// <returns>Retorna el listado</returns>
        public List<FotoRaqueo> ListadoFicha(int fichaCodigo)
        {
            var listado = db.FotosRaqueo.Where(p => p.FichaCodigo == fichaCodigo).ToList();

            return listado;
        }

        /// <summary>
        /// Metodo que sirve para consultar un registro mediante su id
        /// </summary>
        /// <param name="id">Recibe el id del registro</param>
        /// <returns>Retorna el registro consultado</returns>
        public FotoRaqueo Buscar(short? id)
        {
            return db.FotosRaqueo.Find(id);
        }

        /// <summary>
        /// Metodo que sirve para agregar un registro
        /// </summary>
        /// <param name="fotoRaqueo">Recibe un objeto</param>
        public void Crear(FotoRaqueo fotoRaqueo)
        {
            db.FotosRaqueo.Add(fotoRaqueo);
            db.SaveChanges();
        }


        /// <summary>
        /// Metodo que sirve para actualizar un registro
        /// </summary>
        /// <param name="fotoRaqueo">Recibe un objeto</param>
        public void Editar(FotoRaqueo fotoRaqueo)
        {
            db.Entry(fotoRaqueo).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para actualizar las fotos de recibo de la ficha
        /// </summary>
        /// <param name="codigoFicha">Recibe el codigo de la ficha</param>
        /// <param name="fotosNuevas">Recibe el listado de las fotos a actualizar</param>
        public void EditarPorFicha(int codigoFicha, List<FotoRaqueo> fotosNuevas)
        {
            var fotosViejas = ListadoFicha(codigoFicha);

            foreach (var foto in fotosViejas)
            {
                Borrar(foto);
            }

            foreach (var foto in fotosNuevas)
            {
                foto.Codigo = 0;
                foto.FichaCodigo = codigoFicha;
                Crear(foto);
            }
        }

        /// <summary>
        /// Metodo que sirve para borrar un registro
        /// </summary>
        /// <param name="fotoRecibo">Recibe un objeto</param>
        public void Borrar(FotoRaqueo fotoRaqueo)
        {
            db.FotosRaqueo.Remove(fotoRaqueo);
            db.SaveChanges();
        }
    }
}
