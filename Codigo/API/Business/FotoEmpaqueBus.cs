﻿using Data;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    /// <summary>
    /// Nombre de la clase: FotoEmpaqueBus
    /// </summary>
    public class FotoEmpaqueBus
    {
        /// <summary>
        /// Se declara un objeto Context
        /// </summary>
        private Context db;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public FotoEmpaqueBus()
        {
            db = new Context();
        }

        /// <summary>
        /// Metodo que sirve para consultar el listado
        /// </summary>
        /// <returns>Retorna el listado</returns>
        public List<FotoEmpaque> Listado()
        {
            return db.FotosEmpaque.ToList();
        }

        /// <summary>
        /// Metodo que sirve para consultar el listado respectivo a la ficha
        /// </summary>
        /// <returns>Retorna el listado</returns>
        public List<FotoEmpaque> ListadoFicha(int fichaCodigo)
        {
            var listado = db.FotosEmpaque.Where(p => p.FichaCodigo == fichaCodigo).ToList();

            return listado;
        }

        /// <summary>
        /// Metodo que sirve para consultar un registro mediante su id
        /// </summary>
        /// <param name="id">Recibe el id del registro</param>
        /// <returns>Retorna el registro consultado</returns>
        public FotoEmpaque Buscar(short? id)
        {
            return db.FotosEmpaque.Find(id);
        }

        /// <summary>
        /// Metodo que sirve para agregar un registro
        /// </summary>
        /// <param name="fotoEmpaque">Recibe un objeto</param>
        public void Crear(FotoEmpaque fotoEmpaque)
        {
            db.FotosEmpaque.Add(fotoEmpaque);
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para actualizar un registro
        /// </summary>
        /// <param name="fotoEmpaque">Recibe un objeto</param>
        public void Editar(FotoEmpaque fotoEmpaque)
        {
            db.Entry(fotoEmpaque).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para actualizar las fotos de recibo de la ficha
        /// </summary>
        /// <param name="codigoFicha">Recibe el codigo de la ficha</param>
        /// <param name="fotosNuevas">Recibe el listado de las fotos a actualizar</param>
        public void EditarPorFicha(int codigoFicha, List<FotoEmpaque> fotosNuevas)
        {
            var fotosViejas = ListadoFicha(codigoFicha);

            foreach (var foto in fotosViejas)
            {
                Borrar(foto);
            }

            foreach (var foto in fotosNuevas)
            {
                foto.Codigo = 0;
                foto.FichaCodigo = codigoFicha;
                Crear(foto);
            }
        }

        /// <summary>
        /// Metodo que sirve para borrar un registro
        /// </summary>
        /// <param name="fotoEmpaque">Recibe un objeto</param>
        public void Borrar(FotoEmpaque fotoEmpaque)
        {
            db.FotosEmpaque.Remove(fotoEmpaque);
            db.SaveChanges();
        }
    }
}
