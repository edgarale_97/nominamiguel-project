﻿using Data;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    /// <summary>
    /// Nombre de la clase: ProvinciaBus
    /// </summary>
    public class ProvinciaBus
    {
        /// <summary>
        /// Se declara un objeto Context
        /// </summary>
        private Context db;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public ProvinciaBus()
        {
            db = new Context();
        }

        /// <summary>
        /// Metodo que sirve para consultar el listado
        /// </summary>
        /// <returns>Retorna el listado</returns>
        public List<Provincia> Listado()
        {
            return db.Provincias.ToList();
        }

        /// <summary>
        /// Metodo que sirve para consultar un registro mediante su id
        /// </summary>
        /// <param name="id">Recibe el id del registro</param>
        /// <returns>Retorna el registro consultado</returns>
        public Provincia Buscar(short? id)
        {
            return db.Provincias.Find(id);
        }

        /// <summary>
        /// Metodo que sirve para agregar un registro
        /// </summary>
        /// <param name="provincia">Recibe un objeto</param>
        public void Crear(Provincia provincia)
        {
            db.Provincias.Add(provincia);
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para actualizar un registro
        /// </summary>
        /// <param name="provincia">Recibe un objeto</param>
        public void Editar(Provincia provincia)
        {
            db.Entry(provincia).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para borrar un registro
        /// </summary>
        /// <param name="provincia">Recibe un objeto</param>
        public void Borrar(Provincia provincia)
        {
            db.Provincias.Remove(provincia);
            db.SaveChanges();
        }

    }
}
