﻿using Data;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    /// <summary>
    /// Nombre de la clase: OrdenAnodizadoBus
    /// </summary>
    public class OrdenAnodizadoBus
    {
        /// <summary>
        /// Se declara un objeto Context
        /// </summary>
        private Context db;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public OrdenAnodizadoBus()
        {
            db = new Context();
        }

        /// <summary>
        /// Metodo que sirve para consultar el listado
        /// </summary>
        /// <returns>Retorna el listado</returns>
        public List<OrdenAnodizado> Listado()
        {
            return db.OrdenesAnodizado.Where(p => p.Tipo == TipoOrdenEnum.Normal && p.Visible == true).ToList();
        }

        /// <summary>
        /// Metodo que sirve para consultar el listado
        /// </summary>
        /// <returns>Retorna el listado</returns>
        public List<OrdenAnodizado> ListadoReproceso()
        {
            return db.OrdenesAnodizado.Where(p => p.Tipo == TipoOrdenEnum.Reproceso && p.Visible == true).ToList();
        }


        /// <summary>
        /// Metodo que sirve para consultar el listado
        /// </summary>
        /// <returns>Retorna el listado</returns>
        public List<OrdenAnodizado> Buscar(DateTime desde, DateTime hasta)
        {
            var listado = db.OrdenesAnodizado.Where(p => p.Tipo == TipoOrdenEnum.Normal && p.Visible == true).ToList();
            listado = listado.Where(p => p.FechaCreacion >= desde && p.FechaCreacion <= hasta).ToList();

            return listado;
        }

        /// <summary>.tolis
        /// Metodo que sirve para consultar el listado
        /// </summary>
        /// <returns>Retorna el listado</returns>
        public int GetSecuencialNuevo()
        {
            var listado = db.OrdenesAnodizado.ToList();
            var secuencial = listado.Count() + 1;
            return secuencial;
        }

        /// <summary>
        /// Metodo que sirve para consultar un registro mediante su id
        /// </summary>
        /// <param name="id">Recibe el id del registro</param>
        /// <returns>Retorna el registro consultado</returns>
        public OrdenAnodizado Buscar(int? id)
        {
            return db.OrdenesAnodizado.Find(id);
        }

        /// <summary>
        /// Metodo que sirve para agregar un registro
        /// </summary>
        /// <param name="ordenAnodizado">Recibe un objeto</param>
        public int Crear(OrdenAnodizado ordenAnodizado)
        {

            var cantidadPorLote = ordenAnodizado.CantidadTotal / ordenAnodizado.CantidadTotalLotes;

            var fichaTecnicaBus = new FichaTecnicaBus();
            var fichaTecnica = fichaTecnicaBus.Buscar(ordenAnodizado.FichaCode);

            var code = 0;
            var codigo = "";

            for (int i = 0; i < ordenAnodizado.CantidadTotalLotes; i++)
            {
                ordenAnodizado.Code = 0;

                ordenAnodizado.Tipo = TipoOrdenEnum.Normal;
                ordenAnodizado.Visible = true;

                ordenAnodizado.RecibidoMedidasCriticas = fichaTecnica.ReciboMedidaCriticas;
                ordenAnodizado.RaqueoRecomendacion = fichaTecnica.RaqueoRecomendacion;
                ordenAnodizado.ProcesoDesengraseMinutos = fichaTecnica.TiempoDesengrase;
                ordenAnodizado.ProcesoEtching = fichaTecnica.TiempoEtching;
                ordenAnodizado.ProcesoAmperaje = fichaTecnica.AmperajeVoltaje;
                ordenAnodizado.ProcesoAnodizadoMinutos = fichaTecnica.TiempoAnodizado;
                ordenAnodizado.ProcesoTintaMinutos = fichaTecnica.TiempoTinta;
                ordenAnodizado.ProcesoSelloMinutos = fichaTecnica.TiempoSello;
                ordenAnodizado.ProcesoOtros = fichaTecnica.Otros;
                ordenAnodizado.ProcesoCiudadosEspeciales = fichaTecnica.CuidadosEspeciales;
                ordenAnodizado.SalidaCriteriosAceptacion = fichaTecnica.SalidaCriteriosAceptacion;
                ordenAnodizado.EmpaqueRecomendacion = fichaTecnica.EmpaqueRecomendacion;

                ordenAnodizado.RecibidoCantidad = cantidadPorLote;
                ordenAnodizado.RaqueoCantidad = cantidadPorLote;

                ordenAnodizado.Estado = 2;
                ordenAnodizado.RecibidoActivo = true;

                var dbs = new Context();
                dbs.OrdenesAnodizado.Add(ordenAnodizado);
                dbs.SaveChanges();

                if(i == 0)
                {
                    code = ordenAnodizado.Code;
                    codigo = ordenAnodizado.Code + "-" + DateTime.Now.Year;
                }

                ordenAnodizado.Codigo = codigo;
                ordenAnodizado.RecibidoCodigo = codigo;
                ordenAnodizado.NumeroLote = i + 1;

                dbs = new Context();
                ordenAnodizado.Codigo = codigo;
                ordenAnodizado.RecibidoCodigo = codigo;
                dbs.Entry(ordenAnodizado).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                dbs.SaveChanges();
            }

            return code;
        }

        /// <summary>
        /// Metodo que sirve para actualizar el encabezado 
        /// </summary>
        /// <param name="ordenAnodizado">Recibe un objeto</param>
        public void GuardarEncabezado(OrdenAnodizado ordenAnodizado)
        {
            var ordenAntigua = Buscar(ordenAnodizado.Code);

            ordenAnodizado.Tipo = TipoOrdenEnum.Normal;
            ordenAnodizado.Visible = true;

            var fichaTecnicaBus = new FichaTecnicaBus();
            var fichaTecnica = fichaTecnicaBus.Buscar(ordenAnodizado.FichaCode);

            ordenAnodizado.RecibidoMedidasCriticas = fichaTecnica.ReciboMedidaCriticas;
            ordenAnodizado.RaqueoRecomendacion = fichaTecnica.RaqueoRecomendacion;
            ordenAnodizado.ProcesoDesengraseMinutos = fichaTecnica.TiempoDesengrase;
            ordenAnodizado.ProcesoEtching = fichaTecnica.TiempoEtching;
            ordenAnodizado.ProcesoAmperaje = fichaTecnica.AmperajeVoltaje;
            ordenAnodizado.ProcesoAnodizadoMinutos = fichaTecnica.TiempoAnodizado;
            ordenAnodizado.ProcesoTintaMinutos = fichaTecnica.TiempoTinta;
            ordenAnodizado.ProcesoSelloMinutos = fichaTecnica.TiempoSello;
            ordenAnodizado.ProcesoOtros = fichaTecnica.Otros;
            ordenAnodizado.ProcesoCiudadosEspeciales = fichaTecnica.CuidadosEspeciales;
            ordenAnodizado.SalidaCriteriosAceptacion = fichaTecnica.SalidaCriteriosAceptacion;
            ordenAnodizado.EmpaqueRecomendacion = fichaTecnica.EmpaqueRecomendacion;

            ordenAnodizado.Estado = 2;
            ordenAnodizado.RecibidoActivo = true;

            ordenAnodizado.FechaCreacion = ordenAntigua.FechaCreacion;
            ordenAnodizado.CreadoPor = ordenAntigua.CreadoPor;
            ordenAnodizado.Visible = true;

            var dbs = new Context();
            dbs.Entry(ordenAnodizado).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            dbs.SaveChanges();

        }

        /// <summary>
        /// Metodo que sirve para actualizar la inspeccion de recibo
        /// </summary>
        /// <param name="ordenAnodizado">Recibe un objeto</param>
        public void GuardarInspeccionRecibo(OrdenAnodizado ordenAnodizado)
        {
            var orden = db.OrdenesAnodizado.Find(ordenAnodizado.Code);

            var fichaTecnicaBus = new FichaTecnicaBus();
            var fichaTecnica = fichaTecnicaBus.Buscar(orden.FichaCode);

            orden.RecibidoMedidasCriticas = fichaTecnica.ReciboMedidaCriticas;
            orden.RaqueoRecomendacion = fichaTecnica.RaqueoRecomendacion;
            orden.ProcesoDesengraseMinutos = fichaTecnica.TiempoDesengrase;
            orden.ProcesoEtching = fichaTecnica.TiempoEtching;
            orden.ProcesoAmperaje = fichaTecnica.AmperajeVoltaje;
            orden.ProcesoAnodizadoMinutos = fichaTecnica.TiempoAnodizado;
            orden.ProcesoTintaMinutos = fichaTecnica.TiempoTinta;
            orden.ProcesoSelloMinutos = fichaTecnica.TiempoSello;
            orden.ProcesoOtros = fichaTecnica.Otros;
            orden.ProcesoCiudadosEspeciales = fichaTecnica.CuidadosEspeciales;
            ordenAnodizado.SalidaCriteriosAceptacion = fichaTecnica.SalidaCriteriosAceptacion;

            orden.EmpaqueRecomendacion = fichaTecnica.EmpaqueRecomendacion;

            orden.RecibidoCodigo = ordenAnodizado.RecibidoCodigo;
            orden.RecibidoCantidad = ordenAnodizado.RecibidoCantidad;
            orden.RecibidoMedidasCriticas = ordenAnodizado.RecibidoMedidasCriticas;
            orden.RecibidoResponsable = ordenAnodizado.RecibidoResponsable;
            orden.RecibidoFecha = ordenAnodizado.RecibidoFecha;

            orden.FechaActualizacion = ordenAnodizado.FechaActualizacion;
            orden.ActualizadoPor = ordenAnodizado.ActualizadoPor;

            orden.RaqueoActivo = ordenAnodizado.RaqueoActivo;

            if (orden.Estado < 3)
            {
                orden.Estado = 3;
            }

            db.Entry(orden).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            db.SaveChanges();

        }

        /// <summary>
        /// Metodo que sirve para actualizar la inspeccion de recibo
        /// </summary>
        /// <param name="ordenAnodizado">Recibe un objeto</param>
        public void GuardarRaqueo(OrdenAnodizado ordenAnodizado)
        {
            var orden = db.OrdenesAnodizado.Find(ordenAnodizado.Code);

            orden.RaqueoRecomendacion = ordenAnodizado.RaqueoRecomendacion;
            orden.RaqueoCantidad = ordenAnodizado.RaqueoCantidad;
            orden.RaqueoResponsable = ordenAnodizado.RaqueoResponsable;
            orden.RaqueoFecha = ordenAnodizado.RaqueoFecha;

            orden.FechaActualizacion = ordenAnodizado.FechaActualizacion;
            orden.ActualizadoPor = ordenAnodizado.ActualizadoPor;

            orden.ProcesoActivo = ordenAnodizado.ProcesoActivo;

            if (orden.Estado < 4)
            {
                orden.Estado = 4;
            }

            db.Entry(orden).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para actualizar el proceso de anodizado
        /// </summary>
        /// <param name="ordenAnodizado">Recibe un objeto</param>
        public void GuardarProceso(OrdenAnodizado ordenAnodizado)
        {
            var orden = db.OrdenesAnodizado.Find(ordenAnodizado.Code);

            orden.ProcesoDesengraseMinutos = ordenAnodizado.ProcesoDesengraseMinutos;
            orden.ProcesoDesengraseTanque = ordenAnodizado.ProcesoDesengraseTanque;
            orden.ProcesoDesengraseHoraInicio = ordenAnodizado.ProcesoDesengraseHoraInicio;
            orden.ProcesoDesengraseHoraFinal = ordenAnodizado.ProcesoDesengraseHoraFinal;
            orden.ProcesoEtching = ordenAnodizado.ProcesoEtching;
            orden.ProcesoEtchingTanque = ordenAnodizado.ProcesoEtchingTanque;
            orden.ProcesoEtchingHoraInicial = ordenAnodizado.ProcesoEtchingHoraInicial;
            orden.ProcesoEtchingHoraFinal = ordenAnodizado.ProcesoEtchingHoraFinal;
            orden.ProcesoAmperaje = ordenAnodizado.ProcesoAmperaje;
            orden.ProcesoAmperajeTanque = ordenAnodizado.ProcesoAmperajeTanque;
            orden.ProcesoAmperajeHoraInicio = ordenAnodizado.ProcesoAmperajeHoraInicio;
            orden.ProcesoAmperajeHoraFinal = ordenAnodizado.ProcesoAmperajeHoraFinal;
            orden.ProcesoAnodizadoMinutos = ordenAnodizado.ProcesoAnodizadoMinutos;
            orden.ProcesoAnodizadoTanque = ordenAnodizado.ProcesoAnodizadoTanque;
            orden.ProcesoAnodizadoHoraInicio = ordenAnodizado.ProcesoAnodizadoHoraInicio;
            orden.ProcesoAnodizadoHoraFinal = ordenAnodizado.ProcesoAnodizadoHoraFinal;
            orden.ProcesoTintaMinutos = ordenAnodizado.ProcesoTintaMinutos;
            orden.ProcesoTintaTanque = ordenAnodizado.ProcesoTintaTanque;
            orden.ProcesoTintaHoraInicio = ordenAnodizado.ProcesoTintaHoraInicio;
            orden.ProcesoTintaHoraFinal = ordenAnodizado.ProcesoTintaHoraFinal;
            orden.ProcesoSelloMinutos = ordenAnodizado.ProcesoSelloMinutos;
            orden.ProcesoSelloTanque = ordenAnodizado.ProcesoSelloTanque;
            orden.ProcesoSelloHoraInicial = ordenAnodizado.ProcesoSelloHoraInicial;
            orden.ProcesoSelloHoraFinal = ordenAnodizado.ProcesoSelloHoraFinal;
            orden.ProcesoOtros = ordenAnodizado.ProcesoOtros;
            orden.ProcesoOtrosMinutos = ordenAnodizado.ProcesoOtrosMinutos;
            orden.ProcesoOtrosTanque = ordenAnodizado.ProcesoOtrosTanque;
            orden.ProcesoOtrosHoraInicial = ordenAnodizado.ProcesoOtrosHoraInicial;
            orden.ProcesoOtrosHoraFinal = ordenAnodizado.ProcesoOtrosHoraFinal;
            orden.ProcesoCiudadosEspeciales = ordenAnodizado.ProcesoCiudadosEspeciales;
            orden.ProcesoResponsable = ordenAnodizado.ProcesoResponsable;

            orden.FechaActualizacion = ordenAnodizado.FechaActualizacion;
            orden.ActualizadoPor = ordenAnodizado.ActualizadoPor;

            orden.SalidaActivo = ordenAnodizado.SalidaActivo;

            if (orden.Estado < 5)
            {
                orden.Estado = 5;
            }

            db.Entry(orden).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            db.SaveChanges();
        }


        /// <summary>
        /// Metodo que sirve para actualizar la salida
        /// </summary>
        /// <param name="ordenAnodizado">Recibe un objeto</param>
        public void GuardarSalida(OrdenAnodizado ordenAnodizado)
        {
            var orden = db.OrdenesAnodizado.Find(ordenAnodizado.Code);

            orden.SalidaCriteriosAceptacion = ordenAnodizado.SalidaCriteriosAceptacion;
            orden.SalidaCantiAprobada = ordenAnodizado.SalidaCantiAprobada;
            orden.SalidaCantiRechazada = ordenAnodizado.SalidaCantiRechazada;
            orden.SalidaCausas = ordenAnodizado.SalidaCausas;
            orden.SalidaAccion = ordenAnodizado.SalidaAccion;
            orden.SalidaAprobadas = ordenAnodizado.SalidaAprobadas;
            orden.SalidaFecha = ordenAnodizado.SalidaFecha;
            orden.SalidaResponsable = ordenAnodizado.SalidaResponsable;
            
            orden.FechaActualizacion = ordenAnodizado.FechaActualizacion;
            orden.ActualizadoPor = ordenAnodizado.ActualizadoPor;

            orden.EmpaqueActivo = ordenAnodizado.EmpaqueActivo;

            if (orden.Estado < 6)
            {
                orden.Estado = 6;
            }

            db.Entry(orden).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            db.SaveChanges();

            //se valida si se va a crear una nueva orden de reproceso
            if (orden.SalidaAccion == Convert.ToInt32(SalidaAccionEnum.Reproceso))
            {
                var ordenReproceso = new OrdenAnodizado();
                ordenReproceso.Tipo = TipoOrdenEnum.Reproceso;
                ordenReproceso.Code = 0;
                ordenReproceso.Codigo = orden.Codigo;
                ordenReproceso.ClienteCode = orden.ClienteCode;
                ordenReproceso.FichaCode = orden.FichaCode;
                ordenReproceso.CantidadTotal = orden.CantidadTotal;
                ordenReproceso.CantidadTotalLotes = orden.CantidadTotalLotes;
                ordenReproceso.FechaVencimiento = orden.FechaVencimiento;
                ordenReproceso.NumeroLote = orden.NumeroLote;
                ordenReproceso.RevisionPlano = orden.RevisionPlano;
                ordenReproceso.TipoTratamiento = orden.TipoTratamiento;
                ordenReproceso.FechaVencimiento = orden.FechaVencimiento;
                ordenReproceso.FechaCreacion = orden.FechaActualizacion;
                ordenReproceso.CreadoPor = orden.ActualizadoPor;
                ordenReproceso.RecibidoActivo = orden.RecibidoActivo;
                ordenReproceso.RecibidoCodigo = orden.Codigo;
                ordenReproceso.Visible = true;

                db.OrdenesAnodizado.Add(ordenReproceso);
                db.SaveChanges();

            }

        }

        /// <summary>
        /// Metodo que sirve para actualizar la informacion de empaque
        /// </summary>
        /// <param name="ordenAnodizado">Recibe un objeto</param>
        public void GuardarEmpaque(OrdenAnodizado ordenAnodizado)
        {
            var orden = db.OrdenesAnodizado.Find(ordenAnodizado.Code);

            orden.EmpaqueRecomendacion = ordenAnodizado.EmpaqueRecomendacion;
            orden.EmpaqueCantidad = ordenAnodizado.EmpaqueCantidad;
            orden.EmpaqueFecha = ordenAnodizado.EmpaqueFecha;
            orden.EmpaqueResponsable = ordenAnodizado.EmpaqueResponsable;

            orden.FechaActualizacion = ordenAnodizado.FechaActualizacion;
            orden.ActualizadoPor = ordenAnodizado.ActualizadoPor;

            orden.DespachoActivo = ordenAnodizado.DespachoActivo;

            if (orden.Estado < 7)
            {
                orden.Estado = 7;
            }

            db.Entry(orden).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para guardar la orden de despacho
        /// </summary>
        /// <param name="ordenAnodizado">Recibe un objeto</param>
        public void GuardarDespacho(OrdenAnodizado ordenAnodizado)
        {
            var orden = db.OrdenesAnodizado.Find(ordenAnodizado.Code);

            orden.DespachoCantidad = ordenAnodizado.DespachoCantidad;
            orden.DespachoCertificado = ordenAnodizado.DespachoCertificado;
            orden.DespachoFactura = ordenAnodizado.DespachoFactura;
            orden.DespachoFecha = ordenAnodizado.DespachoFecha;
            orden.DespachoResponsable = ordenAnodizado.DespachoResponsable;

            orden.FechaActualizacion = ordenAnodizado.FechaActualizacion;
            orden.ActualizadoPor = ordenAnodizado.ActualizadoPor;

            db.Entry(orden).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para validar las fechas
        /// </summary>
        /// <param name="ordenAnodizado">Recibe un objeto OrdenAnodizado</param>
        /// <returns>Retorna el objeto OrdenAnodizado con las fechas formateadas</returns>
        public OrdenAnodizado ValTodasFechas(OrdenAnodizado ordenAnodizado)
        {
            ordenAnodizado.RecibidoFecha = ValFecha(ordenAnodizado.RecibidoFecha);
            ordenAnodizado.RaqueoFecha = ValFecha(ordenAnodizado.RaqueoFecha);
            ordenAnodizado.RaqueoFecha = ValFecha(ordenAnodizado.ProcesoDesengraseHoraInicio);
            ordenAnodizado.RaqueoFecha = ValFecha(ordenAnodizado.ProcesoDesengraseHoraFinal);
            ordenAnodizado.ProcesoEtchingHoraInicial = ValFecha(ordenAnodizado.ProcesoEtchingHoraInicial);
            ordenAnodizado.ProcesoEtchingHoraFinal = ValFecha(ordenAnodizado.ProcesoEtchingHoraFinal);
            ordenAnodizado.ProcesoAmperajeHoraInicio = ValFecha(ordenAnodizado.ProcesoAmperajeHoraInicio);
            ordenAnodizado.ProcesoAmperajeHoraFinal = ValFecha(ordenAnodizado.ProcesoAmperajeHoraFinal);
            ordenAnodizado.ProcesoAnodizadoHoraInicio = ValFecha(ordenAnodizado.ProcesoAnodizadoHoraInicio);
            ordenAnodizado.ProcesoAnodizadoHoraFinal = ValFecha(ordenAnodizado.ProcesoAnodizadoHoraFinal);
            ordenAnodizado.ProcesoTintaHoraInicio = ValFecha(ordenAnodizado.ProcesoTintaHoraInicio);
            ordenAnodizado.ProcesoDesengraseHoraInicio = ValFecha(ordenAnodizado.ProcesoDesengraseHoraInicio);
            ordenAnodizado.ProcesoDesengraseHoraFinal = ValFecha(ordenAnodizado.ProcesoDesengraseHoraFinal);
            ordenAnodizado.ProcesoTintaHoraFinal = ValFecha(ordenAnodizado.ProcesoTintaHoraFinal);
            ordenAnodizado.ProcesoSelloHoraInicial = ValFecha(ordenAnodizado.ProcesoSelloHoraInicial);
            ordenAnodizado.ProcesoSelloHoraFinal = ValFecha(ordenAnodizado.ProcesoSelloHoraFinal);
            ordenAnodizado.SalidaFecha = ValFecha(ordenAnodizado.SalidaFecha);
            ordenAnodizado.EmpaqueFecha = ValFecha(ordenAnodizado.EmpaqueFecha);
            ordenAnodizado.DespachoFecha = ValFecha(ordenAnodizado.DespachoFecha);
            ordenAnodizado.FechaActualizacion = ValFecha(ordenAnodizado.FechaActualizacion);

            return ordenAnodizado;
        }

        public DateTime? ValFecha(DateTime? dateTime)
        {
            if (dateTime == new DateTime() || dateTime == null)
            {
                return null;
            }

            return dateTime;
        }

        /// <summary>
        /// Metodo que sirve para actualizar un registro
        /// </summary>
        /// <param name="ordenAnodizado">Recibe un objeto</param>
        public void Editar(OrdenAnodizado ordenAnodizado)
        {
            db.Entry(ordenAnodizado).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para borrar un registro
        /// </summary>
        /// <param name="ordenAnodizado">Recibe un objeto</param>
        public void Borrar(OrdenAnodizado ordenAnodizado)
        {
            ordenAnodizado.Visible = false;
            Editar(ordenAnodizado);
        }
    }
}
