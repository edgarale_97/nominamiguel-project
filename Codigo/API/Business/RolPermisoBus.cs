﻿using Data;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    /// <summary>
    /// Nombre de la clase: RolPermisoBus
    /// </summary>
    public class RolPermisoBus
    {
        /// <summary>
        /// Se declara un objeto Context
        /// </summary>
        private Context db;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public RolPermisoBus()
        {
            db = new Context();
        }

        /// <summary>
        /// Metodo que sirve para consultar el listado
        /// </summary>
        /// <returns>Retorna el listado</returns>
        public List<RolPermiso> Listado()
        {
            return db.RolesPermisos.ToList();
        }

        /// <summary>
        /// Metodo que sirve para consultar un registro mediante su id
        /// </summary>
        /// <param name="id">Recibe el id del registro</param>
        /// <returns>Retorna el registro consultado</returns>
        public RolPermiso Buscar(short? id)
        {
            return db.RolesPermisos.Find(id);
        }

        /// <summary>
        /// Metodo que sirve para agregar un registro
        /// </summary>
        /// <param name="rolPermiso">Recibe un objeto</param>
        public void Crear(RolPermiso rolPermiso)
        {
            db.RolesPermisos.Add(rolPermiso);
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para actualizar un registro
        /// </summary>
        /// <param name="rolPermiso">Recibe un objeto</param>
        public void Editar(RolPermiso rolPermiso)
        {
            db.Entry(rolPermiso).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para borrar un registro
        /// </summary>
        /// <param name="rolPermiso">Recibe un objeto</param>
        public void Borrar(RolPermiso rolPermiso)
        {
            db.RolesPermisos.Remove(rolPermiso);
            db.SaveChanges();
        }
    }
}
