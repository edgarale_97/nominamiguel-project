﻿using Data;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    /// <summary>
    /// Nombre de la clase: FotoSalidaBus
    /// </summary>
    public class FotoSalidaBus
    {
        /// <summary>
        /// Se declara un objeto Context
        /// </summary>
        private Context db;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public FotoSalidaBus()
        {
            db = new Context();
        }

        /// <summary>
        /// Metodo que sirve para consultar el listado
        /// </summary>
        /// <returns>Retorna el listado</returns>
        public List<FotoSalida> Listado()
        {
            return db.FotosSalida.ToList();
        }

        /// <summary>
        /// Metodo que sirve para consultar el listado respectivo a la ficha
        /// </summary>
        /// <returns>Retorna el listado</returns>
        public List<FotoSalida> ListadoFicha(int fichaCodigo)
        {
            var listado = db.FotosSalida.Where(p => p.FichaCodigo == fichaCodigo).ToList();

            return listado;
        }

        /// <summary>
        /// Metodo que sirve para consultar un registro mediante su id
        /// </summary>
        /// <param name="id">Recibe el id del registro</param>
        /// <returns>Retorna el registro consultado</returns>
        public FotoSalida Buscar(short? id)
        {
            return db.FotosSalida.Find(id);
        }

        /// <summary>
        /// Metodo que sirve para agregar un registro
        /// </summary>
        /// <param name="fotoSalida">Recibe un objeto</param>
        public void Crear(FotoSalida fotoSalida)
        {
            db.FotosSalida.Add(fotoSalida);
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para actualizar un registro
        /// </summary>
        /// <param name="fotoSalida">Recibe un objeto</param>
        public void Editar(FotoSalida fotoSalida)
        {
            db.Entry(fotoSalida).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para actualizar las fotos de recibo de la ficha
        /// </summary>
        /// <param name="codigoFicha">Recibe el codigo de la ficha</param>
        /// <param name="fotosNuevas">Recibe el listado de las fotos a actualizar</param>
        public void EditarPorFicha(int codigoFicha, List<FotoSalida> fotosNuevas)
        {
            var fotosViejas = ListadoFicha(codigoFicha);

            foreach (var foto in fotosViejas)
            {
                Borrar(foto);
            }

            foreach (var foto in fotosNuevas)
            {
                foto.Codigo = 0;
                foto.FichaCodigo = codigoFicha;
                Crear(foto);
            }
        }

        /// <summary>
        /// Metodo que sirve para borrar un registro
        /// </summary>
        /// <param name="fotoSalida">Recibe un objeto</param>
        public void Borrar(FotoSalida fotoSalida)
        {
            db.FotosSalida.Remove(fotoSalida);
            db.SaveChanges();
        }
    }
}
