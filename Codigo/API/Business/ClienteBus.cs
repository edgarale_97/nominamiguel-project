﻿using Data;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    /// <summary>
    /// Nombre de clase: ClienteBus
    /// </summary>
    public class ClienteBus
    {
        /// <summary>
        /// Se declara un objeto Context
        /// </summary>
        private Context db;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public ClienteBus()
        {
            db = new Context();
        }

        /// <summary>
        /// Metodo que sirve para consultar el listado
        /// </summary>
        /// <returns>Retorna el listado</returns>
        public List<Cliente> Listado()
        {
            return db.Clientes.ToList();
        }

        /// <summary>
        /// Metodo que sirve para consultar un registro mediante su id
        /// </summary>
        /// <param name="id">Recibe el id del registro</param>
        /// <returns>Retorna el registro consultado</returns>
        public Cliente Buscar(int? id)
        {
            return db.Clientes.Find(id);
        }

        /// <summary>
        /// Metodo que sirve para agregar un registro
        /// </summary>
        /// <param name="cliente">Recibe un objeto</param>
        public void Crear(Cliente cliente)
        {
            db.Clientes.Add(cliente);
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para actualizar un registro
        /// </summary>
        /// <param name="cliente">Recibe un objeto</param>
        public void Editar(Cliente cliente)
        {
            db.Entry(cliente).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para borrar un registro
        /// </summary>
        /// <param name="cliente">Recibe un objeto</param>
        public void Borrar(Cliente cliente)
        {
            db.Clientes.Remove(cliente);
            db.SaveChanges();
        }
    }
}
