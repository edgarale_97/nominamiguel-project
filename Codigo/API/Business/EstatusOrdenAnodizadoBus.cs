﻿using Data;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    /// <summary>
    /// Nombre de la clase: EstatusOrdenAnodizadoBus
    /// </summary>
    public class EstatusOrdenAnodizadoBus
    {
        /// <summary>
        /// Se declara un objeto Context
        /// </summary>
        private Context db;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public EstatusOrdenAnodizadoBus()
        {
            db = new Context();
        }

        /// <summary>
        /// Metodo que sirve para consultar el listado
        /// </summary>
        /// <returns>Retorna el listado</returns>
        public List<EstatusOrdenAnodizado> Listado()
        {
            return db.EstatusOrdenAnodizado.ToList();
        }

        /// <summary>
        /// Metodo que sirve para consultar un registro mediante su id
        /// </summary>
        /// <param name="id">Recibe el id del registro</param>
        /// <returns>Retorna el registro consultado</returns>
        public EstatusOrdenAnodizado Buscar(int? id)
        {
            return db.EstatusOrdenAnodizado.Find(id);
        }

        /// <summary>
        /// Metodo que sirve para agregar un registro
        /// </summary>
        /// <param name="EstatusOrdenAnodizado">Recibe un objeto</param>
        public void Crear(EstatusOrdenAnodizado EstatusOrdenAnodizado)
        {
            db.EstatusOrdenAnodizado.Add(EstatusOrdenAnodizado);
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para actualizar un registro
        /// </summary>
        /// <param name="EstatusOrdenAnodizado">Recibe un objeto</param>
        public void Editar(EstatusOrdenAnodizado EstatusOrdenAnodizado)
        {
            db.Entry(EstatusOrdenAnodizado).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para borrar un registro
        /// </summary>
        /// <param name="EstatusOrdenAnodizado">Recibe un objeto</param>
        public void Borrar(EstatusOrdenAnodizado EstatusOrdenAnodizado)
        {
            db.EstatusOrdenAnodizado.Remove(EstatusOrdenAnodizado);
            db.SaveChanges();
        }
    }
}
