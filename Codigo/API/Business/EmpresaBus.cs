﻿using Data;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    /// <summary>
    /// Nombre de la clase: EmpresaBus
    /// </summary>
    public class EmpresaBus
    {
        /// <summary>
        /// Se declara un objeto Context
        /// </summary>
        private Context db;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public EmpresaBus()
        {
            db = new Context();
        }

        /// <summary>
        /// Metodo que sirve para consultar el listado
        /// </summary>
        /// <returns>Retorna el listado</returns>
        public List<Empresa> Listado()
        {
            return db.Empresas.ToList();
        }

        /// <summary>
        /// Metodo que sirve para consultar un registro mediante su id
        /// </summary>
        /// <param name="id">Recibe el id del registro</param>
        /// <returns>Retorna el registro consultado</returns>
        public Empresa Buscar(short? id)
        {
            return db.Empresas.Find(id);
        }

        /// <summary>
        /// Metodo que sirve para agregar un registro
        /// </summary>
        /// <param name="empresa">Recibe un objeto</param>
        public void Crear(Empresa empresa)
        {
            db.Empresas.Add(empresa);
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para actualizar un registro
        /// </summary>
        /// <param name="empresa">Recibe un objeto</param>
        public void Editar(Empresa empresa)
        {
            db.Entry(empresa).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para borrar un registro
        /// </summary>
        /// <param name="empresa">Recibe un objeto</param>
        public void Borrar(Empresa empresa)
        {
            db.Empresas.Remove(empresa);
            db.SaveChanges();
        }
    }
}
