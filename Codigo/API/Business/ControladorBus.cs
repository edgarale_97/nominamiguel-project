﻿using Data;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    /// <summary>
    /// Nombre de la clase: ControladorBus
    /// </summary>
    public class ControladorBus
    {
        /// <summary>
        /// Se declara un objeto Context
        /// </summary>
        private Context db;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public ControladorBus()
        {
            db = new Context();
        }

        /// <summary>
        /// Metodo que sirve para consultar el listado
        /// </summary>
        /// <returns>Retorna el listado</returns>
        public List<Controlador> Listado()
        {
            return db.Controladores.ToList();
        }

        /// <summary>
        /// Metodo que sirve para consultar un registro mediante su id
        /// </summary>
        /// <param name="id">Recibe el id del registro</param>
        /// <returns>Retorna el registro consultado</returns>
        public Controlador Buscar(int? id)
        {
            return db.Controladores.Find(id);
        }

        /// <summary>
        /// Metodo que sirve para agregar un registro
        /// </summary>
        /// <param name="controlador">Recibe un objeto</param>
        public void Crear(Controlador controlador)
        {
            db.Controladores.Add(controlador);
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para actualizar un registro
        /// </summary>
        /// <param name="controlador">Recibe un objeto</param>
        public void Editar(Controlador controlador)
        {
            db.Entry(controlador).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para borrar un registro
        /// </summary>
        /// <param name="controlador">Recibe un objeto</param>
        public void Borrar(Controlador controlador)
        {
            db.Controladores.Remove(controlador);
            db.SaveChanges();
        }
    }
}
