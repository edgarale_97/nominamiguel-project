﻿using Data;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    /// <summary>
    /// Nombre de la clase: TipoTratamientoBus
    /// </summary>
    public class TipoTratamientoBus
    {
        /// <summary>
        /// Se declara un objeto Context
        /// </summary>
        private Context db;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public TipoTratamientoBus()
        {
            db = new Context();
        }

        /// <summary>
        /// Metodo que sirve para consultar el listado
        /// </summary>
        /// <returns>Retorna el listado</returns>
        public List<TipoTratamiento> Listado()
        {
            return db.TiposTratamiento.ToList();
        }

        /// <summary>
        /// Metodo que sirve para consultar un registro mediante su id
        /// </summary>
        /// <param name="id">Recibe el id del registro</param>
        /// <returns>Retorna el registro consultado</returns>
        public TipoTratamiento Buscar(int? id)
        {
            return db.TiposTratamiento.Find(id);
        }

        /// <summary>
        /// Metodo que sirve para agregar un registro
        /// </summary>
        /// <param name="tipoTratamiento">Recibe un objeto</param>
        public void Crear(TipoTratamiento tipoTratamiento)
        {
            db.TiposTratamiento.Add(tipoTratamiento);
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para actualizar un registro
        /// </summary>
        /// <param name="tiposTratamiento">Recibe un objeto</param>
        public void Editar(TipoTratamiento tiposTratamiento)
        {
            db.Entry(tiposTratamiento).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para borrar un registro
        /// </summary>
        /// <param name="tiposTratamiento">Recibe un objeto</param>
        public void Borrar(TipoTratamiento tiposTratamiento)
        {
            db.TiposTratamiento.Remove(tiposTratamiento);
            db.SaveChanges();
        }
    }
}
