﻿using Data;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    /// <summary>
    /// Nombre de la clase: UsuarioBus
    /// </summary>
    public class UsuarioBus
    {
        #region Declaración de objetos
        /// <summary>
        /// Se declara un objeto Context
        /// </summary>
        private Context db;
        #endregion

        #region Constructor de clase UsuarioBus
        /// <summary>
        /// Constructor de la clase UsuarioBus
        /// </summary>
        public UsuarioBus()
        {
            //Se inicializa la instancia del objeto context
            db = new Context();
        }
        #endregion

        #region Validaciones
        /// <summary>
        /// Verifica si el nombre de usuario y contraseña ingresados estan registrados.
        /// </summary>
        /// <param name="username">Recibe el nombre de usuario</param>
        /// <param name="password">Recibe la contrasenia</param>
        /// <returns>Retorna un objeto si los parametros no estan null, de lo contrario retorna null.</returns>
        public Usuario Validacion(string username, string password)
        {
            //Verifica que los parametros no se encuentren nulos
            if (!String.IsNullOrEmpty(username) && !String.IsNullOrEmpty(password))
            {
                var usuario = db.Usuarios.Where(x => x.NombreUsuario == username && x.Contrasenia == password).FirstOrDefault();

                return usuario;
            }
            return null;
        }

        /// <summary>
        /// Verifica que el correo este registrado.
        /// </summary>
        /// <param name="email"></param>
        /// <returns>Retorna un objeto si los parametros no estan null, de lo contrario retorna null./returns>
        public Usuario ValidacionCorreo(string email)
        {
            //Verifica que los parametros no se encuentren nulos
            if (email != null)
            {
                return db.Usuarios.Where(x => x.Correo == email).FirstOrDefault();
            }
            return null;
        }
        #endregion

        #region Listados
        /// <summary>
        /// Metodo que sirve para consultar el listado de usuarios
        /// </summary>
        /// <returns>Retorna el listado de usuarios</returns>
        public List<Usuario> Listado()
        {
            return db.Usuarios.ToList();
        }
        #endregion

        #region CRUD
        /// <summary>
        /// Metodo que sirve para consultar un registro mediante su id
        /// </summary>
        /// <param name="id">Recibe el id del registro</param>
        /// <returns>Retorna el registro consultado</returns>
        public Usuario Buscar(short? id)
        {
            return db.Usuarios.Find(id);
        }

        /// <summary>
        /// Metodo que sirve para agregar un registro
        /// </summary>
        /// <param name="usuario">Recibe un objeto</param>
        public void Crear(Usuario usuario)
        {
            db.Usuarios.Add(usuario);
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para actualizar un registro
        /// </summary>
        /// <param name="usuario">Recibe un objeto</param>
        public void Editar(Usuario usuario)
        {
            db.Update(usuario);
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para borrar un registro
        /// </summary>
        /// <param name="usuario">Recibe un objeto</param>
        public void Borrar(Usuario usuario)
        {
            db.Usuarios.Remove(usuario);
            db.SaveChanges();
        }
        #endregion
    }
}
