﻿using Data;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    /// <summary>
    /// Nombre de la clase: Canton
    /// </summary>
    public class CantonBus
    {
        /// <summary>
        /// Se declara un objeto Context
        /// </summary>
        private Context db;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public CantonBus()
        {
            db = new Context();
        }

        /// <summary>
        /// Metodo que sirve para consultar el listado
        /// </summary>
        /// <returns>Retorna el listado</returns>
        public List<Canton> Listado()
        {
            return db.Cantones.ToList();
        }

        /// <summary>
        /// Metodo que sirve para consultar un registro mediante su id
        /// </summary>
        /// <param name="id">Recibe el id del registro</param>
        /// <returns>Retorna el registro consultado</returns>
        public Canton Buscar(short? provinciacode, short? cantonCode)
        {
            return db.Cantones.Where(p => p.ProvinciaCode == provinciacode && p.Code == cantonCode).FirstOrDefault();
        }

        /// <summary>
        /// Metodo que sirve para agregar un registro
        /// </summary>
        /// <param name="canton">Recibe un objeto</param>
        public void Crear(Canton canton)
        {
            db.Cantones.Add(canton);
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para actualizar un registro
        /// </summary>
        /// <param name="canton">Recibe un objeto</param>
        public void Editar(Canton canton)
        {
            db.Entry(canton).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para borrar un registro
        /// </summary>
        /// <param name="canton">Recibe un objeto</param>
        public void Borrar(Canton canton)
        {
            db.Cantones.Remove(canton);
            db.SaveChanges();
        }
    }
}
