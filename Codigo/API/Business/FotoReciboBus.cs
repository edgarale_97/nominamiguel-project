﻿using Data;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    /// <summary>
    /// Nombre de la clase: FotoRecibo
    /// </summary>
    public class FotoReciboBus
    {
        /// <summary>
        /// Se declara un objeto Context
        /// </summary>
        private Context db;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public FotoReciboBus()
        {
            db = new Context();
        }

        /// <summary>
        /// Metodo que sirve para consultar el listado
        /// </summary>
        /// <returns>Retorna el listado</returns>
        public List<FotoRecibo> Listado()
        {
            return db.FotosRecibo.ToList();
        }

        /// <summary>
        /// Metodo que sirve para consultar el listado respectivo a la ficha
        /// </summary>
        /// <returns>Retorna el listado</returns>
        public List<FotoRecibo> ListadoFicha(int fichaCodigo)
        {
            var listado = db.FotosRecibo.Where(p => p.FichaCodigo == fichaCodigo).ToList();

            return listado;
        }

        /// <summary>
        /// Metodo que sirve para consultar un registro mediante su id
        /// </summary>
        /// <param name="id">Recibe el id del registro</param>
        /// <returns>Retorna el registro consultado</returns>
        public FotoRecibo Buscar(short? id)
        {
            return db.FotosRecibo.Find(id);
        }

        /// <summary>
        /// Metodo que sirve para agregar un registro
        /// </summary>
        /// <param name="fotoRecibo">Recibe un objeto</param>
        public void Crear(FotoRecibo fotoRecibo)
        {
            db.FotosRecibo.Add(fotoRecibo);
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para actualizar un registro
        /// </summary>
        /// <param name="fotoRecibo">Recibe un objeto</param>
        public void Editar(FotoRecibo fotoRecibo)
        {
            db.Entry(fotoRecibo).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para borrar un registro
        /// </summary>
        /// <param name="fotoRecibo">Recibe un objeto</param>
        public void Borrar(FotoRecibo fotoRecibo)
        {
            db.FotosRecibo.Remove(fotoRecibo);
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para actualizar las fotos de recibo de la ficha
        /// </summary>
        /// <param name="codigoFicha">Recibe el codigo de la ficha</param>
        /// <param name="fotosNuevas">Recibe el listado de las fotos a actualizar</param>
        public void EditarPorFicha(int codigoFicha, List<FotoRecibo> fotosNuevas)
        {
            var fotosViejas = ListadoFicha(codigoFicha);

            foreach (var foto in fotosViejas)
            {
                Borrar(foto);
            }

            foreach (var foto in fotosNuevas)
            {
                foto.Codigo = 0;
                foto.FichaCodigo = codigoFicha;
                Crear(foto);
            }
        }


        public FotoRecibo CambiarFoto(FotoRecibo fotoVieja, FotoRecibo fotoNueva)
        {
            return null;
        }

    }
}
