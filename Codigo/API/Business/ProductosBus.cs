﻿using Data;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    /// <summary>
    /// Nombre de la clase: EmpresaBus
    /// </summary>
    public class ProductosBus
    {
        /// <summary>
        /// Se declara un objeto Context
        /// </summary>
        private Context db;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public ProductosBus()
        {
            db = new Context();
        }

        /// <summary>
        /// Metodo que sirve para consultar el listado
        /// </summary>
        /// <returns>Retorna el listado</returns>
        public List<Productos> Listado()
        {
            return db.ProductosQuimicos.ToList();
        }

        /// <summary>
        /// Metodo que sirve para consultar un registro mediante su id
        /// </summary>
        /// <param name="id">Recibe el id del registro</param>
        /// <returns>Retorna el registro consultado</returns>
        public Productos Buscar(short? id)
        {
            return db.ProductosQuimicos.Find(id);
        }

        /// <summary>
        /// Metodo que sirve para agregar un registro
        /// </summary>
        /// <param name="producto">Recibe un objeto</param>
        public void Crear(Productos producto)
        {
            db.ProductosQuimicos.Add(producto);
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para actualizar un registro
        /// </summary>
        /// <param name="producto">Recibe un objeto</param>
        public void Editar(Productos producto)
        {
            db.Entry(producto).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para borrar un registro
        /// </summary>
        /// <param name="producto">Recibe un objeto</param>
        public void Borrar(Productos producto)
        {
            db.ProductosQuimicos.Remove(producto);
            db.SaveChanges();
        }
    }
}
