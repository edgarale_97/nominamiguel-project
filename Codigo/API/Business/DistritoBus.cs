﻿using Data;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    /// <summary>
    /// Nombre de la clase: DistritoBus
    /// </summary>
    public class DistritoBus
    {
        /// <summary>
        /// Se declara un objeto Context
        /// </summary>
        private Context db;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public DistritoBus()
        {
            db = new Context();
        }

        /// <summary>
        /// Metodo que sirve para consultar el listado
        /// </summary>
        /// <returns>Retorna el listado</returns>
        public List<Distrito> Listado()
        {
            return db.Distritos.ToList();
        }

        /// <summary>
        /// Metodo que sirve para consultar un registro mediante su id
        /// </summary>
        /// <param name="id">Recibe el id del registro</param>
        /// <returns>Retorna el registro consultado</returns>
        public Distrito Buscar(short? id)
        {
            return db.Distritos.Find(id);
        }

        /// <summary>
        /// Metodo que sirve para agregar un registro
        /// </summary>
        /// <param name="distrito">Recibe un objeto</param>
        public void Crear(Distrito distrito)
        {
            db.Distritos.Add(distrito);
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para actualizar un registro
        /// </summary>
        /// <param name="distrito">Recibe un objeto</param>
        public void Editar(Distrito distrito)
        {
            db.Entry(distrito).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            db.SaveChanges();
        }

        /// <summary>
        /// Metodo que sirve para borrar un registro
        /// </summary>
        /// <param name="distrito">Recibe un objeto</param>
        public void Borrar(Distrito distrito)
        {
            db.Distritos.Remove(distrito);
            db.SaveChanges();
        }
    }
}
